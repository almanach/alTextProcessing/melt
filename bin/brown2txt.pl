#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";

while (1) {
  $_=shift;
  if (/^$/) {last;}
  elsif (/^-S$/) {$sxpipecomments = 1}
  else {die "Unrecognized option: $_";}
}

if ($sxpipecomments) {
  while (<>) {
    chomp;
    s/\/[^\/ }]+([ }]|$)/\1/g;
    print "$_\n";
  }
} else {
  while (<>) {
    chomp;
    s/\/[^\/ ]+( |$)/\1/g;
    print "$_\n";
  }
}
