#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$max_code = 0;
$n = shift || 5;
if ($n < 0) {$n = -$n}

while (<>) {
  chomp;
  next if /^\s*$/;
  /^([^\t]+)\t([^\t]+)(?:\t([^\t]+))?$/ || die "Error in lexicon file: <$_>";
  $form = $1;
  $tag = $2;
  $lemma = $3;
  $form =~ s/ /_/g;
  $tag =~ s/^V.*/V/g;
#  $tag =~ s/^(...).*/\1/g;
  $tag_code{$tag} = ++$max_code unless (defined($tag_code{$tag}));
  $form_tag{$form}{$tag}++;
####
  $features = "";
  $orig_form = $form;
  $form = ("#" x $n).$form;
  $form =~ s/^.*?((?:.){$n})$/$1/;
  while ($form =~ s/^(.)(.*)$/$2/) {
    $features .= " $1$2";
  }
  $features =~ s/^ //;
  print $tag."\t".$features."\t$orig_form\t$tag\n";
####
}

# for $form (keys %form_tag) {
#   $features = "";
#   $orig_form = $form;
#   $form = ("#" x $n).$form;
#   $form =~ s/^.*?((?:.){$n})$/$1/;
#   while ($form =~ s/^(.)(.*)$/$2/) {
#     $features .= " $1$2";
#   }
#   $features =~ s/^ //;
#   for $tag (keys %{$form_tag{$orig_form}}) {
# #    print $tag_code{$tag}."\t".$features."\t$orig_form\t$tag\n";
#     print $tag."\t".$features."\t$orig_form\t$tag\n";
#   }
# }
