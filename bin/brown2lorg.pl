#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

print STDERR "  BROWN2LORG: Converting...\n";

while (<>) {
  chomp;
  s/^ */ /;
  s/ *$/ /;
  s/\(/-LRB-/g;
  s/\)/-RRB-/g;
  s/\[/-LSB-/g;
  s/\]/-RSB-/g;
  s/\{/-LCB-/g;
  s/\}/-RCB-/g;
  s/(?<= )([^ \/]+)(?= )/$1 ( )/g;
  s/(?<= )([^ ]+)\/([^ \/]+)(?= )/$1 ( $2 )/g;
#  s/\//^^/g;
  s/\^\^/\//g;
  print "$_\n";
}
print "\n";
print STDERR "  BROWN2LORG: Converting: done\n";
