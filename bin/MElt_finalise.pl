#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

while (<>) {
  chomp;
#  s/_acc_o/{/gi;
#  s/_acc_f/}/gi;
  s/_underscore/_/gi;
  print "$_\n";
}
