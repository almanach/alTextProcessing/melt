#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$just_printed_newline = 1;
while (<>) {
  chomp;
  s/[\l\r]//g;
  if (/^\t*$/) {
    print "\n";
    $just_printed_newline = 1;
  } elsif (/^#[^\t]*$/ || /^# sent_id/) {
    next;
  } else {
    /^[\d-]+\t([^\t]+)\t[^\t]+\t[^\t]+\t([^\t]+)(\t|$)/ || die "\n<$_>";
    print " " unless $just_printed_newline;
    print $1."/".$2;
    $just_printed_newline = 0;
  }
}
print "\n" unless $just_printed_newline;
