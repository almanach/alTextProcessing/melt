#!/usr/bin/perl

$model = shift || die "Please provide a model file name";
open MODEL, "<$model" || die $!;
while (<MODEL>) {
  chomp;
  if (s/^\*\*\*NAMEDLABELSIDS\*\*\*\t//) {
    $n = -1;
    while (s/^(.*?)\t// || s/^(.+)$//) {
      $labels[++$n] = $1;
    }
  } elsif (s/^\*\*BIAS\*\*\t//) {
    $n = -1;
    while (s/^(.*?) // || s/^(.+)$//) {
      $bias[++$n] = $1;
    }
    die if ($n ne $#labels);
  } else {
    s/^(.*?)\t//;
    $feat = $1;
    $n = -1;
    while (s/^(.*?) // || s/^(.+)$//) {
      $model{$feat}->[++$n] = $1;
    }
    die if ($n ne $#bias);
  }
}

while (<>) {
  chomp;
  /^([^\t]*)\t([^\t]*)(\t.*)$/ || die;
  $gold = $1;
  $feats = $2;
  $other = $3;
  $other =~ s/ /_/g;
  @res = ();
  for $n (0..$#bias) {
    $res[$n] = $bias[$n];
  }
  for $feat (split / +/, $feats) {
    $model_feat = $model{$feat};
    for $n (1..$#bias) {
      $res[$n] += $model_feat->[$n];
    }
  }
  $best_score = -1000000;
  $res = "";
  for (0..$#res) {
    if ($res[$_] > $best_score) {
      $best_score = $res[$_];
    }
  }
  for (0..$#res) {
    if ($res[$_] > $best_score - 1) {
      if ($res ne "") {$res .= "|"}
      $res .= $labels[$_];
    }
  }
  print "$res\t$gold\t$feats$other\n";
}
