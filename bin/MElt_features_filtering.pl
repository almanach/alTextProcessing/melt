#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$filename = shift || die;
$thrd = shift || die;

open FILE, "<$filename" || die;
binmode FILE, ":utf8";
while (<FILE>) {
  chomp;
  push @lines, $_;
  unless ($_ eq "DEV") {
    s/^.*?\t//;
    for (split / /,$_) {
      $feat2occ{$_}++;
    }
  }
}
close FILE;



open FILE, ">$filename" || die;
binmode FILE, ":utf8";
for (@lines) {
  if ($_ eq "DEV") {
    print FILE "DEV\n";
  } else {
    s/^(.*?)\t// || die;
    print FILE "$1\t";
    while (s/^([^ ]+)( ?)//) {
      if ($feat2occ{$1} < $thrd) {
	$filtered_features{$1} = 1;
      } else {
	print FILE $1.$2;
      }
    }
    print FILE "\n";
  }
}
close FILE;
print STDERR " Keeping ".((scalar keys %feat2occ) - (scalar keys %filtered_features))." distinct features (filtered out ".(scalar keys %filtered_features)." distinct features)";
