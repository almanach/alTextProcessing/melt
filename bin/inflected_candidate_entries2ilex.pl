#!/usr/bin/perl
# cat emea-fr-train.tcs.MElt.lemmatized | perl ~/Documents/alexina/lefff/trunk/MElt2ilex_candidate_entries.pl /Users/sagot/Documents/alexina/lefff/trunk/lefff.edylex_data | perl ~/Documents/alexina/lefff/trunk/morpho.fr.dir.pl -nosep > emea-fr-train.tcs.MElt.lemmatized.inflected_candidate_entries


binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

while (<>) {
  chomp;
  next if (/<error\[/);
  @line = split (/\t/, $_);
  $qm = quotemeta($line[1]);
  if ($line[6] =~ /(^| _ )${qm}:(\d+)/) {
    $entry_form{$line[4]."\t".$line[0]."\t".$line[3]}{$line[1]} = $2;
    $entry_form{$line[4]."\t".$line[0]."\t".$line[3]}{__ALL__} += $2;
  } else {
    $entry_form{$line[4]."\t".$line[0]."\t".$line[3]}{$line[1]} = 0;
  }
  $form_entry{$line[1]}{$line[4]."\t".$line[0]."\t".$line[3]} = 1;
}

while (1) {
  $entry = 0;
  $best_cov = -1;
  for (keys %entry_form) {
    if ($entry_form{$_}{__ALL__} > $best_cov && !defined($seen{$_})) {
      $best_cov = $entry_form{$_}{__ALL__};
      $entry = $_;
    }
  }
  last if ($best_cov <= 0);
  $seen{$entry} = 1;
  print STDERR (' ' x 200)."\r";
  print STDERR (scalar keys %seen)."/".(scalar keys %entry_form)."\t$entry\r";
  for $form (keys %{$entry_form{$entry}}) {
    for $otherentry (keys %{$form_entry{$form}}) {
      next if ($otherentry eq $entry);
      $entry_form{$otherentry}{__ALL__} -= $entry_form{$otherentry}{$form};
      $entry_form{$otherentry}{$form} = 0;	
    }
  }
}
print STDERR (' ' x 200)."\r";

for $entry (sort {$entry_form{$b}{__ALL__} <=> $entry_form{$a}{__ALL__}} keys %entry_form) {
  last if $entry_form{$entry}{__ALL__} <= 1;
  $entry =~ /^(.+?)\t(.*?\t.*)$/ || die $entry;
  print $2."\t100;Lemma;$1;;cat=$1;\%default\t".$entry_form{$entry}{__ALL__}."\t".join(" _ ",map {"$_:$entry_form{$entry}{$_}"} sort {$a cmp $b} keys %{$entry_form{$entry}})."\n";
}
