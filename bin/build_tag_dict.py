#!/usr/bin/env python

import os
import sys
import optparse
import re
from collections import defaultdict
import codecs
try:
    from cjson import dumps, loads
except ImportError:
    try:
        from simplejson import dumps, loads
    except ImportError:
        from json import dumps, loads


usage = "usage: %prog [options] <input_file>"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-p", "--path", action="store", help="path for dump", default="./tag_dict.json")
(options, args) = parser.parse_args()

WD_TAG_RE = re.compile(r'^(.+)/([^\/]+)$')
CAPONLYLINE_RE = re.compile(r'^([^a-z]+)$')

class BrownReader():

    """
    Data reader for corpus in the Brown format:

    Le/DET prix/NC de/P vente/NC du/P+D journal/NC n'/ADV a/V pas/ADV <E9>t<E9>/VPP divulgu<E9>/VPP ./PONCT 
    Le/DET cabinet/NC Arthur/NPP Andersen/NPP ,/PONCT administrateur/NC judiciaire/ADJ du/P+D titre/NC depuis/P l'/DET effondrement/NC de/P l'/DET empire/NC Maxwell/NPP en/P d<E9>cembre/NC 1991/NC ,/PO
    NCT a/V indiqu<E9>/VPP que/CS les/DET nouveaux/ADJ propri<E9>taires/NC allaient/V poursuivre/VINF la/DET publication/NC ./PONCT 

    """

    def __init__(self,infile, encoding='utf-8'):
        self.stream = codecs.open(infile, 'r', encoding)
        return

    def __iter__(self):
        return self

    def next(self,lowerCaseCapOnly=0):
        line = self.stream.readline()
        if (line == ''):
            self.stream.seek(0)
            raise StopIteration
        line = line.strip(' \n')
        token_list = []
        if line == 'DEV':
            token_list.append((line,"__SPECIAL__"))
        else:
            wasCapOnly = 0
            if (lowerCaseCapOnly == 1 and len(line) > 10):
                wasCapOnly = CAPONLYLINE_RE.match(line)
            for item in line.split(' '):
                if item == '':
                    continue
                wdtag = WD_TAG_RE.match(item)
                if (wdtag):
                    wd,tag = wdtag.groups()
                    if (wasCapOnly):
                        token_list.append( (wd.lower(),tag) )
                    else:
                        token_list.append( (wd,tag) )
                else:
                    print >> sys.stderr, "Warning: Incorrect token/tag pair: \""+(item.encode('utf8'))+"\""+" --- line: "+line
        return token_list


def tag_dict(file_path):
    tag_dict = defaultdict(dict)
    for s in BrownReader(file_path,encoding="utf-8"):
        for wd,tag in s:
            tag_dict[wd][tag] = 1
    return tag_dict


def serialize(datastruct, filepath, encoding="utf-8"):
    _file = codecs.open( filepath, 'w', encoding=encoding )
    _file.write( dumps( datastruct ) )
    _file.close()
    return 




tdict = tag_dict( args[0] ) # e.g., ftb_1.pos

serialize(tdict,options.path)

print >> sys.stderr, "Tag dictionary dumped at: %s (%s entries)" %(options.path,len(tdict))

