#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

$inputfile = shift || die "Please provide an input file (in Brown format)";

open IN, "<$inputfile" || die $!;
binmode IN, ":utf8";
open TOK, ">$inputfile.tok" || die $!;
binmode TOK, ":utf8";
open TAG, ">$inputfile.tag" || die $!;
binmode TAG, ":utf8";
$l = 0;
while (<IN>) {
  $l++;
  chomp;
  s/^ +//;
  s/ +$//;
  s/ +/ /g;
  next if $_ eq "";
  if ($_ eq "DEV") {
    print TOK "DEV\n";
    print TAG "__SPECIAL__\n";
  } else {
    unless (/^[^ ]+\/[^ \/]+(?: [^ ]+\/[^ \/]+)*$/) {
      die "Incorrect line $l:\n$_";
    }
    s/_-_/-/g;
    while (s/^([^ ]+)\/([^ \/]+)( |$)//) {
      print TOK $1.$3;
      print TAG $2.$3;
    }
    print TOK "\n";
    print TAG "\n";
  }
}
