#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

while (1) {
  $_=shift;
  if (/^$/) {last;}
  elsif (/^-p$/) {$prediction = shift}
  elsif (/^-[gr]$/) {$reference = shift}
  elsif (/^-np$/) {$no_ponct = 1}
  elsif (/^-v$/) {$verbose = 1}
  elsif (/^-c$/) {$output_corpus_for_comparison = 1}
  elsif (/^-t$/) {$training_data_for_listing_known_words = shift}
  else {die "Unrecognized option: $_";}
}

die "Please provide a prediction file using option -p" if ($prediction eq "");
die "Please provide a reference file using option -r" if ($reference eq "");

if ($training_data_for_listing_known_words ne "") {
  open TDFLKW, "<$training_data_for_listing_known_words" || die $!;
  binmode TDFLKW, ":utf8";
  while (<TDFLKW>) {
    chomp;
    while (s/^\s*([^ ]+)\/([^ \/]+)//) {
      $known_words{$1} = 1;
      $known_word_tag_pairs{$1."#_#_#".$2} = 1;
    }
  }
}

print STDERR "Loaded ".(scalar keys %known_words)." know words and ".(scalar keys %known_word_tag_pairs)." known word/tag pairs in the training corpus.\n";

open PRED, "<$prediction" || die "Could not open $prediction: $!";
binmode PRED, ":utf8";
open REF, "<$reference" || die "Could not open $reference: $!";
binmode REF, ":utf8";

my ($r, $p);

while ($r=<REF>) {
  next if $r =~ /^\s*$/;
  last if $r =~ /^===+$/;
  $r =~ s/ \/\/SPECIAL//g;
  while ($p=<PRED>) {
    last unless $p =~ /^\s*$/;
  }
  chomp ($r);
  chomp ($p);
  $r =~ s/^ +//;   $r =~ s/ +$//; 
  $p =~ s/^ +//;   $p =~ s/ +$//; 
  @r = split (/ +/, $r);
  @p = split (/ +/, $p);
  if ($#r != $#p) {die "Error: different number of tokens in the following sentence:\nREF = $r\nPRED = $p\n";}
  for $i (0..$#r) {
    $rt = $r[$i];
    $pt = $p[$i];
    $rt =~ s/^(.*?)\/([^\/]+)$/\2/;
    next if ($no_ponct && ($rt eq "PONCT" || $rt eq "PUNCT"));
    $w = $1;
    $pt =~ s/^.*?\/([^\/]+)$/\1/;
    $qm_rt = quotemeta($rt);
    if ($pt =~ /(^|\|)[\?\!]?$qm_rt(\||$)/) {
      $acc++;
      if (defined($known_words{$w})) {
	$acck++;
      } else {
	$accuk++;
      }
      if (defined($known_word_tag_pairs{$w."#_#_#".$rt})) {
	$acckp++;
      } else {
	$accukp++;
      }
      if ($output_corpus_for_comparison) {
	if ($i > 0) {
	  print " ";
	}
	print $w."/".$rt;
      }
    } else {
      if ($output_corpus_for_comparison) {
	if ($i > 0) {
	  print " ";
	}
	print $w."/".$rt."/!".$pt;
      }
      if ($verbose) {
	if (defined($known_words{$w})) {
	  print STDERR "KW ";
	}
	print STDERR "$w --- ref: $rt --- pred: $pt\n";
      }
    }
    $tot++;
    if (defined($known_words{$w})) {
      $totk++;
    } else {
      $totuk++;
    }
    if (defined($known_word_tag_pairs{$w."#_#_#".$rt})) {
      $totkp++;
    } else {
      $totukp++;
    }
  }
  if ($output_corpus_for_comparison) {
    print "\n";
  }
}
print "All\t".(100*$acc/$tot)." ($acc/$tot)\n";
print "InTrainWords\t".(100*$acck/$totk)." ($acck/$totk)\n" unless $totk == 0;
print "NotInTrainWords\t".(100*$accuk/$totuk)." ($accuk/$totuk)\n" unless $totuk == 0;
print "InTrainWordTagPairs\t".(100*$acckp/$totkp)." ($acckp/$totkp)\n" unless $totkp == 0;
print "NotInTrainWordTagPairs\t".(100*$accukp/$totukp)." ($accukp/$totukp)\n" unless $totukp == 0;
