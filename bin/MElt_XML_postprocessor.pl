#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$| = 1;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-lemma_only$/) {$lemma_only = 1}
}

%lemmas_weight = (
		  "cln" => 3,
		  "cla" => 3,
		  "cld" => 2,
		  "clg" => 2,
		  "cour" => 2,
		  "falloir" => 2,
		  "mois" => 2,
		  "fils" => 2,
		  "jour" => 2,
		  "fois" => 2,
		  "ouvrir" => 2,
		  "feu" => 2,
		  "politique" => 2,
		  "ouvré" => 2,
		 );

while (<>) {
  chomp;
  s/^\s*/ /;
  s/\s*$/ /;
  s/<\//◁/g;
  s/\/>/▷/g;
  if ($lemma_only) {
    s/(?<= )[^\/ \n]+\/[^ \/]+\/\*?([^\/ \n]+)(?= )/chose_one_lemma($1)/ge;
    for my $i (1..4) {
      s/(?<= )[^\/ \n]+(?:\/[^ \/]+){$i}\/[^ \/]+\/\*?([^\/ \n]+(?:\/[^ \/]+){$i})(?= )/chose_one_lemma($1)/ge;
    }
  }
  s/◁/<\//g;
  s/▷/\/>/g;
  s/{<([^> {}]+)>[^{} ]+<\/\1>}\s*([^ \n]+)/<\1>\2<\/\1>/g;
  s/^\s+//;
  s/\s+$//;
  if (s/^<inserted_newline_before\/>/ /) {
    $print_newline = 0;
  }
  print "\n" if $print_newline;
  if (s/<inserted_newline_after\/>$/ /) {
    $print_newline = 0;
  } else {
    $print_newline = 1;
  }
  s/  +/ /g;
  print $_;
}
print "\n" if $print_newline;

sub chose_one_lemma {
  my $s = shift;
  return (sort {$lemmas_weight{$b} <=> $lemmas_weight{$a} || length($b) <=> length($a)} split(/\|/,$s))[0];
}
