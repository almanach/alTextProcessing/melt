#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

print STDERR "  BROWN2MORFETTE: Converting...\n";

while (<>) {
  chomp;
  s/^ */ /;
  s/ *$/ /;
  s/\(/-LRB-/g;
  s/\)/-RRB-/g;
  s/\[/-LSB-/g;
  s/\]/-RSB-/g;
  s/\{/-LCB-/g;
  s/\}/-RCB-/g;
  s/(?<= )([^ \/]+)(?= )/"$1\t".lc($1)."\t0\n"/ge;
  s/(?<= )([^ ]+)\/([^ \/]+)(?= )/"$1\t".lc($1)."\t$2\n"/ge;
  s/\^\^/\//g;
#  s/\//^^/g;
  print "$_\n";
}
print "\n";
print STDERR "  BROWN2MORFETTE: Converting: done\n";
