#!/usr/bin/env python

import sys

tags = {}
for l in open(sys.argv[1]):
    tokens = l.strip().split()
    for tok in tokens:
        try:
            _,tag = tok.split("/")
            tags[tag] = 1
        except ValueError:
            pass

tagset = tags.keys()
print "\n".join( sorted(tagset) )
