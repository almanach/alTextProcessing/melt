#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

print STDERR "  BROWN2WORDFREAK: Converting...\n";

while (<>) {
  chomp;
  if (/^\s*(<\/?(?:[A-Za-z][A-Za-z0-9_-]*)(?: [^>]+)?\/?>)\s*/) {
    $tag = $1;
    $tag =~ s/ /_/g;
    print "(SENT (META $tag) )\n";
    next;
  } else {
    s/^ */ /;
    s/ *$/ /;
    s/\(/-LRB-/g;
    s/\)/-RRB-/g;
    s/\[/-LSB-/g;
    s/\]/-RSB-/g;
    s/\{/-LCB-/g;
    s/\}/-RCB-/g;
    s/(?<= )([^ \/]+)(?= )/(0 $1)/g;
    s/(?<= )([^ ]+)\/([^ \/]+)(?= )/($2 $1)/g;
    #  s/\//^^/g;
    s/\^\^/\//g;
    s/^ /(SENT /;
    s/ $/ )/;
    print "$_\n";
  }
}
print "\n";
print STDERR "  BROWN2WORDFREAK: Converting: done\n";
