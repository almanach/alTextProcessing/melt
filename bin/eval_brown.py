#!/usr/bin/env python

import sys
import codecs
import re
import optparse
from collections import defaultdict

try:
    from cjson import dumps, loads
except ImportError:
    try:
        from simplejson import dumps, loads
    except ImportError:
        from json import dumps, loads

################## I/O ################################################
usage = "usage: %prog [options] <input_file>"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-g", "--gold", action="store", help="gold file")
parser.add_option("-p", "--predictions", action="store", help="predictions file")
parser.add_option("-d", "--tag_dictionary", action="store", help="tag dictionary from training data (in json format)")
parser.add_option("-l", "--lexicon", action="store", help="external lexicon (in json format)")
parser.add_option("-e", "--errors", action="store_true", help="show errors",default=False)
(options, args) = parser.parse_args()

if not (options.gold and options.predictions):
    sys.exit("Please provide reference file (-g) and predictions file (-p)")




WD_TAG_RE = re.compile(r'^(.+)/([^\/]+)$')
CAPONLYLINE_RE = re.compile(r'^([^a-z]+)$')

class BrownReader():

    """
    Data reader for corpus in the Brown format:

    Le/DET prix/NC de/P vente/NC du/P+D journal/NC n'/ADV a/V pas/ADV <E9>t<E9>/VPP divulgu<E9>/VPP ./PONCT 
    Le/DET cabinet/NC Arthur/NPP Andersen/NPP ,/PONCT administrateur/NC judiciaire/ADJ du/P+D titre/NC depuis/P l'/DET effondrement/NC de/P l'/DET empire/NC Maxwell/NPP en/P d<E9>cembre/NC 1991/NC ,/PO
    NCT a/V indiqu<E9>/VPP que/CS les/DET nouveaux/ADJ propri<E9>taires/NC allaient/V poursuivre/VINF la/DET publication/NC ./PONCT 

    """

    def __init__(self,infile, encoding='utf-8'):
        self.stream = codecs.open(infile, 'r', encoding)
        return

    def __iter__(self):
        return self

    def next(self,lowerCaseCapOnly=0):
        line = self.stream.readline()
        if (line == ''):
            self.stream.seek(0)
            raise StopIteration
        line = line.strip(' \n')
        wasCapOnly = 0
        if (lowerCaseCapOnly == 1 and len(line) > 10):
            wasCapOnly = CAPONLYLINE_RE.match(line)
        token_list = []
        for item in line.split(' '):
            wdtag = WD_TAG_RE.match(item)
            if (wdtag):
                wd,tag = wdtag.groups()
                if (wasCapOnly):
                    token_list.append( (wd.lower(),tag) )
                else:
                    token_list.append( (wd,tag) )
            else:
                print >> sys.stderr, "Warning: Incorrect token/tag pair: \""+item+"\""
        return token_list



############################ result_sink.py ############################


class ResultSink:
    pass


class AccuracySink(ResultSink):
    """Result sink that collects accuracy statistics"""

    def __init__(self):
        self.total = 0
        self.total_knowns = 0
        self.total_unknowns = 0
        self.correct = 0
        self.correct_knowns = 0
        self.correct_unknowns = 0
        self.correct_by_tag = {}
        self.pred_by_tag = {}
        self.total_by_tag = {}
        self.matrix = {}
        self.tags = {}
	self.model_predictions = []
        return

    def update(self,wd,gold_cl,pred_cl,know_words):
        self.total += 1
        self.total_by_tag[gold_cl] = self.total_by_tag.get(gold_cl,0) + 1
        if wd in know_words:# or wd.lower() in  know_words:
            self.total_knowns += 1
        else:
            self.total_unknowns += 1
        self.pred_by_tag[pred_cl] = self.pred_by_tag.get(pred_cl,0) + 1
        if (gold_cl == pred_cl):
            self.correct += 1
            self.correct_by_tag[pred_cl] = self.correct_by_tag.get(pred_cl,0) + 1
            if wd in know_words:# or wd.lower() in  know_words:
                self.correct_knowns += 1
            else:
                self.correct_unknowns += 1
        self.tags[gold_cl] = 1
        self.matrix[(gold_cl,pred_cl)] = self.matrix.get((gold_cl,pred_cl),0) + 1
	self.model_predictions.append((gold_cl,pred_cl))
        return


    def score(self):
        return self._score(self.correct, total=self.total)

    def score_knowns(self):
        return self._score(self.correct_knowns, self.total_knowns)

    def score_unknowns(self):
        return self._score(self.correct_unknowns, self.total_unknowns)

    def _score(self, correct, total, rounding=4 ):
        """Returns accuracy so far"""
        try:
            acc = correct / float(total)
            acc = round( acc, rounding)
        except ZeroDivisionError:
            acc = 0.0
        return acc

    def confusion(self):
        """Print confusion matrix"""
        print "Confusion matrix"
        print
        print '\tas'
        print 'is\t',"\t".join(self.tags.keys()).encode('utf8')
        for tag1 in self.tags.keys():
            print tag1.encode('utf8'),'\t',
            for tag2 in self.tags.keys():
                print self.matrix.get((tag1,tag2),0),'\t',
            print            
        return
        
    def predictions(self):
        truth_and_predicted = [str(x)+"\t"+y for (x,y) in self.model_predictions]
	print "====== Model predictions on test data ======"
        print "True\tPredicted"
	print "\n".join(truth_and_predicted)
	print "============================================"

    def rpf(self):
        """Print R-P-F scores by class labels"""
        print "Recall/Precision/F1 by class labels"
        print
        print "-"*80
        print "%10s | %10s %10s %10s | %10s %10s %10s" %("Label", "Recall", "Precison", "F1", "Correct", "Predicted", "Gold")
        print "-"*80
        for tag in self.tags:
            corr = self.correct_by_tag.get(tag,0)
            pred = self.pred_by_tag.get(tag,0)
            total = self.total_by_tag.get(tag,0)
            r = corr/float(total)
            p = 0.0
            if pred:
                p = corr/float(pred)
            f = 0.0
            if r+p:
                f = (2*r*p)/(r+p)
            print "%10s | %10s %10s %10s | %10s %10s %10s" %(tag.encode('utf8'), round(r,3), round(p,3), round(f,3), corr, pred, total)
        print "-"*80
        return
    


def compare_files( gold_file, pred_file, sink, known_words={}, quiet=True, encoding='utf-8'):
    gold = BrownReader( gold_file, encoding )
    pred = BrownReader( pred_file, encoding )
    s_ct = 0
    for gold_s in gold:
        s_ct += 1
        pred_s = pred.next()
        # compare tags
        for i in range(len(gold_s)):
            gwd,gtag = gold_s[i]
            try:
                pwd,ptag = pred_s[i]
            except IndexError:
                ptag = "UNK"
                print >> sys.stderr, "Warning: Missing prediction for Sentence #%s, token %s" %(s_ct,i.encode('utf8'))
            # update sinks
            sink.update(gwd,gtag,ptag,known_words)
            # errors
            if not quiet:
                if ptag<>gtag:
                    print >> sys.stderr, "%s: %s <==> *%s" %(gwd,gtag,ptag) 
            

    return







def unserialize(filepath, encoding="utf-8"):
    _file = codecs.open( filepath, 'r', encoding=encoding )
    datastruct = loads( _file.read() )
    _file.close()
    return datastruct




def tag_dict(file_path):
    tag_dict = defaultdict(dict)
    for s in BrownReader(file_path,encoding="utf-8"):
        for wd,tag in s:
            tag_dict[wd][tag] = 1
    return tag_dict


############# load dico ################################################
train_words = {}
if options.tag_dictionary:
    print >> sys.stderr, "Loading known word list from training data..."
    # train_words = tag_dict( options.tag_dictionary )
    train_words = unserialize( options.tag_dictionary )
    print >> sys.stderr, "Train word list loaded: %s words" %(len(train_words))

lex_words = {}
if options.lexicon:
    print >> sys.stderr, "Loading known word list from external lexicon..."
    lex_words = unserialize( options.lexicon )
    print >> sys.stderr, "Lexicon word list loaded: %s words" %(len(lex_words))




############ compare gold/pred files ########################################

sink = AccuracySink()

known_words = train_words
known_words.update( lex_words )

compare_files( options.gold, options.predictions, sink, known_words, quiet=not options.errors, encoding='utf-8')

############ print out scores ########################################

sink.rpf()
print 
sink.confusion()
print             
print "\n>> OVERALL ACC: %s (%s/%s)" %(sink.score(),sink.correct,sink.total)
print ">> Unknow wds: %s (%s/%s)" %(sink.score_unknowns(),sink.correct_unknowns,sink.total_unknowns)
print ">> Known wds: %s (%s/%s)" %(sink.score_knowns(),sink.correct_knowns,sink.total_knowns)

    


