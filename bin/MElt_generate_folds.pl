#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$outputdir = shift || die "Please provide an output directory";
$nfolds = shift || 10;

for $i (1..$nfolds) {
  local *FILE;
  open(FILE, ">$outputdir/fold_$i") || die;
  binmode FILE, ":utf8";
  push(@folds, *FILE);
}
for $i (1..$nfolds) {
  local *FILE;
  open(FILE, ">$outputdir/testfold_$i") || die;
  binmode FILE, ":utf8";
  push(@testfolds, *FILE);
}

$n = 0;

while (<>) {
  for $i (0..$nfolds-1) {
    $fold = $folds[$i];
    $testfold = $testfolds[$i];
    if ($n % $nfolds == $i) {
      print $testfold "$_";
    } else {
      print $fold "$_";
    }
  }
  $n++;
}
