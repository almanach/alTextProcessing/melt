#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

$datafile = shift || die "Please provide the data file";
$tagsfile = shift || die "Please provide the tags file";
$nokeeptokens = shift || 0;
$dumpencontent = shift || 0;

open DATA, "<$datafile" || die $!;
binmode DATA, ":utf8";
open TAG, "<$tagsfile" || die $!;
binmode TAG, ":utf8";
if ($dumpencontent) {
  open ENC, ">$datafile.encontent" || die $!;
  binmode ENC, ":utf8";
}
$l = 0;
while (<DATA>) {
  $l++;
  chomp;
  unless ($tags = <TAG>) {
    die "Not enough sentences in the tag file";
  }
  chomp($tags);
  if ($tags eq "__SPECIAL__") {
    s/{.*?} //g;
    print $_."\n";
    next;
  }
  @tags = split / /, $tags;
  $i = -1;
  s/({[^{}]+} *)(1?9+)/\1_\2/g;
  s/{([^{}]+)} *([^_ ][^ ]*)/\1/g;
  while (s/^([^ {}]+|{.*?} *(?:_[^ ]+))( |$)//) {
    $form = $1;
    $space = $2;
    if ($form =~ /^{(.*?)} (_[^ ]+)$/) {
      $tokens = $1;
      $en = $2;
      $en =~ s/^(_[^_ ]+)_[^_]+_$/\1/;
      $en = "_DATE" if ($en =~ /_DATE/);
      $en = "_999" if $en eq "_NUMBER" || $en eq "_NUM";
      if ($en =~ s/^_(1?9+)$/\1/) {
	$tokens =~ s/^ +//;	
	$tokens =~ s/ +$//;
	$tmp = "";
	$tmp2 = "";
	for (split / /, $tokens) {
	  $i++;
	  if ($tmp eq "") {
	    $tmp = "$en\t";
	    $tmp2 = "{";
	  } else {
	    $tmp .= " ";
	    $tmp2 .= " ";
	  } 
	  $tmp .= "$_/$tags[$i]";
	  $tmp2 .= "$_/$tags[$i]";
	}
	print ENC $tmp."\n" if ($dumpencontent);
	print $tmp2."} " unless ($nokeeptokens);
	print $en."/".$tags[$i];
      } elsif ($en eq "_ROMNUM") {
	die $tokens if $tokens =~ / /;
	$i++;
	print ENC "_ROMNUM\t$tokens/$tags[$i]\n" if ($dumpencontent);
	print "{$tokens/$tags[$i]} " unless ($nokeeptokens);
	print "_ROMNUM/".$tags[$i];
      } else {
	$tokens =~ s/^ +//;	
	$tokens =~ s/ +$//;
	$tmp = "";
	$tmp2 = "";
	for (split / /, $tokens) {
	  $i++;
	  if ($tmp eq "") {
	    $tmp = "$en\t";
	    $tmp2 = "{";
	  } else {
	    $tmp .= " ";
	    $tmp2 .= " ";
	  } 
	  $tmp .= "$_/$tags[$i]";
	  $tmp2 .= "$_/$tags[$i]";
	}
	print ENC $tmp."\n" if ($dumpencontent);
	print $tmp2."} " unless ($nokeeptokens);
	$en =~ /^_(.{1,3})/;
	print $en."/".$1;
      }
    } else {
      die if $form =~ /^{/;
      die if $form =~ / /;
      $i++;
      if ($i > $#tags) {
	print " [stopping here]\n";
	die "Word number mismatch on line $l";
      }
      print $form."/".$tags[$i];
    }
    print $space;
  }
  print "\n";
  if ($i != $#tags) {
    print " [stopping here]\n";
    die "Word number mismatch on line $l";
  }
}

if ($tags = <TAG>) {
    die "Not enough sentences in the data file";
}
