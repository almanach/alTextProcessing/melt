#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

if (0) { # default
  $features{lex_lhs} = "1";
  $features{lex_rhs} = "1";
  $features{lex_wd} = "1";
  $features{pln} = "4";
  $features{pwin} = "3";
  $features{win} = "2";
  $features{sln} = "4";
  $features{rpln} = "1";
  $features{rsln} = "0";
  $features{ffthrsld} = "2";
  $features{norm} = "0";
} elsif (0) {			# expés HDR
  $features{lex_lhs} = "0,1";
  $features{lex_rhs} = "1";
  $features{lex_wd} = "1";
  $features{pln} = "3,4";
  $features{pwin} = "2,3";
  $features{win} = "2";
  $features{sln} = "4,5,6";
  $features{rpln} = "0,1,2";
  $features{rsln} = "0,1,2";
  $features{ffthrsld} = "1,2,3";
  $features{norm} = "0";
} else { # expés réduites mais multiclass PERCEO
  $features{lex_lhs} = "1";
  $features{lex_rhs} = "1";
  $features{lex_wd} = "1";
  $features{pln} = "3,4,5";
  $features{sln} = "4,5,6,7";
  $features{pwin} = "2";
  $features{win} = "2";
  $features{rpln} = "0,1,2,3";
  $features{rsln} = "0,1,2,3";
  $features{ffthrsld} = "1,2";
  $features{norm} = "0";
}


$lex = shift || "data/lefff.ftb4.MEltlex.24";
$train = shift || "data/MElt-corpus/ftb4v6_ms_1.brown.28";
$test = shift || "data/MElt-corpus/ftb4v6_ms_3.brown.28";
$lang = shift || "fr";
$modeltype = shift || "multitron";
$maxiter = shift || 0;
$is_NE_aware = shift || 0;
$dryrun = shift || 0;

$maxiter = 40 if $maxiter == 0;

die if $lex =~ / /;
die if $train =~ / /;
die if $test =~ / /;
die "<$modeltype>" unless $modeltype =~ /^(multitron|multiclass)$/;
die unless $is_NE_aware == 0 || $is_NE_aware == 1;

$NE_option = "";
$NE_option = "-S" if $is_NE_aware;
$modeltype_option = "";
$modeltype_option = "-m $max_iter";

$options[0] = "";

for $f (sort keys %features) {
  @newoptions = ();
  for $in (@options) {
    for $v (split /,/, $features{$f}) {
      push @newoptions, $in.($in eq "" ? "" : ",")."$f=$v";
    }
  }
  @options = ();
  for (@newoptions) {
    next if (/pln=(\d+).*rpln=(\d+)/ && $1 < $2);
    next if (/sln=(\d+).*rsln=(\d+)/ && $1 < $2);
    next if (/pwin=(\d+).*win=(\d+)/ && $1 < $2);
    push @options, $_;
  }
}

$modeltypesuffix = "-$maxiter";

for (@options) {
  unless (-d "$lang-$modeltype$modeltypesuffix-$NE_option-$_") {
    $n++;
  }
}
print STDERR "$n\n";

exit (0) if $dryrun;

for (@options) {
  unless (-d "$lang-$modeltype$modeltypesuffix-$NE_option-$_") {
    system("MElt-train -s -C $modeltype -m $maxiter $NE_option -f $_ $lang-$modeltype$modeltypesuffix-$NE_option-$_ $lex $train $test && rm $lang-$modeltype$modeltypesuffix-$NE_option-$_.trainingdata && rm -f $lang-$modeltype$modeltypesuffix-$NE_option-$_/*.json && rm -f $lang-$modeltype$modeltypesuffix-$NE_option-$_/*.npy && rm -f $lang-$modeltype$modeltypesuffix-$NE_option-$_/*.ftl") == 0 or die "";
  }
}
