#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

$corpus_file = shift || die "Please provide the training corpus as an argument to the script";

print STDERR "  Reading corpus...";
open CORPUS, "<$corpus_file" || die "Could not open $corpus_file: $!";
binmode CORPUS, ":utf8";
$l = 0;
while (<CORPUS>) {
  $l++;
  chomp;
  s/^\s+//;
  s/\s+$//;
  next if $_ eq "DEV";
  for (split / +/, $_) {
    /^(.*)\/([^\/]+)$/ || die "Incorrect token/tag pair in the corpus: $_ (line $l);";
    $token_tag{$1}{$2}++;
    $tag_token{$2}{$1}++;
  }
}
close CORPUS;
print STDERR "done\n";

print STDERR "  Reading lexicon...";
$l = 0;
while (<>) {
  $l++;
  chomp;
  next if /^\s*$/;
  /^([^\t]+)\t([^\t]+)\t([^\t]+)((?:\t[^\t]+)?)$/ || die "Incorrect line in the input lexicon: $_ (line $l)";
  push @forms, $1;
  push @tags, $2;
  push @lemmas, $3;
  push @remainder, $4;
  $lexform_lextag{$1}{$2} = 1;
  $lextag_lexform{$2}{$1} = 1;
}
print STDERR "done\n";

print STDERR "  Computing mapping...";
for $token (keys %token_tag) {
  if (defined $lexform_lextag{$token}) {
    for $tag (keys %{$token_tag{$token}}) {
      for $lextag (keys %{$lexform_lextag{$token}}) {
	$mapping{$tag}{$lextag} += 1/((scalar keys %{$lexform_lextag{$token}})*(scalar keys %{$token_tag{$token}}));
      }
    }
  } else {
    for $tag (keys %{$token_tag{$token}}) {
      print "$token\t$tag\n";
    }
  }
}
print STDERR "done\n";

for $tag (keys %mapping) {
  for $lextag (keys %{$mapping{$tag}}) {
    print "$tag\t$lextag\t$mapping{$tag}{$lextag}\n";
  }
}
