#!/bin/sh

VERSION=@version@
PYTHONPATH=@pkgpythondir@:$PYTHONPATH
MODEL=@datadir@/fr
BINDIR=@bindir@
TAGDICO=${MODEL}/tag_dict.json
LEXICON=${MODEL}/lexicon.json
HANDLE_COMMENTS=
DO_TOKENIZE=0
PRINT_PROBAS=
SXPIPE_OPTIONS=
LOWERCASE_UPPERCASE_SENTENCES=
LANGUAGE=fr

MElt_option_checking_boolean=0

while getopts :m:l:s:cvthPL o
do case "$o" in
	v)  echo $VERSION && exit 1;;
	h)  echo "Usage: MElt [ -cthv ] [ -m /path/to/model | -l language ] < input > output" \
            && echo "-m	[/path/to/model] Use the MElt model given instead of the default one (model 'fr' trained on the FTB and the Lefff)" \
            && echo "-l	[language] Use the default MElt model for language 'language' instead of the default 'fr' (the MElt model folder must be in @datadir@)" \
            && echo "-c	Handle SxPipe-like 'comments' (arbitrary sequence of characters preceeding a token and surrounded by curly brackets)" \
            && echo "-t	Use the SxPipe-light tokenizer before tagging (SxPpipe-light, included in MElt's distribution, is a lightweight version of SxPipe, a full-featured pre-parsing processing chain)" \
            && echo "-P	Append the proba of the selected tag to each token" \
            && echo "-h	Output this help message" \
            && echo "-v	Output MElt version" \
            && exit 1;;
	s)  SXPIPE_OPTIONS=$OPTARG;;
	c)  HANDLE_COMMENTS="-c";;
	t)  DO_TOKENIZE=1;;
	P)  PRINT_PROBAS="-P";;
	d)  LOWERCASE_UPPERCASE_SENTENCES="-L";;
	m) rep=$OPTARG
	    if [ $MElt_option_checking_boolean -eq 1 ]; then
		echo "Error: options -m and -l can not be used at the same time" >&2
		exit 4
	    else if ! [ -d $rep -a -r $rep -a -x $rep ]; then
		echo "Error: no accessible model found in folder $rep (the directory does not exist or does not contain a MElt model)" >&2
		exit 4
	    else
		MODEL=$OPTARG
		LEXICON=${OPTARG}/lexicon.json
		TAGDICO=${OPTARG}/tag_dict.json
		MElt_option_checking_boolean=1
	    fi
	    fi
	    ;;
	l) rep=$OPTARG
	    if [ $MElt_option_checking_boolean -eq 1 ]; then
		echo "Error: options -m and -l can not be used at the same time" >&2
		exit 4		
	    else if ! [ -d @datadir@/$rep -a -r @datadir@/$rep -a -x @datadir@/$rep ]; then
		echo "Error: no accessible MElt model for language $rep found (expected to be found in @datadir@/$rep)" >&2
		exit 4
	    else
		MODEL=@datadir@/$OPTARG
		LEXICON=${MODEL}/lexicon.json
		TAGDICO=${MODEL}/tag_dict.json
		MElt_option_checking_boolean=1
	    fi
	    fi
	    ;;
	:) # ici $OPTARG==d
	    echo "Error: model or language name missing after '-m' or '-l'" >&2
	    exit 4
	    ;;
    esac
done


if [ $DO_TOKENIZE -eq 1 ]
then
    tee MElt_tmp.input | sxpipe-light -u ${SXPIPE_OPTIONS} | PYTHONPATH=$PYTHONPATH python @pythondir@/melt/pos_tag.py -m ${MODEL} -d ${TAGDICO} -l ${LEXICON} -c ${PRINT_PROBAS} ${LOWERCASE_UPPERCASE_SENTENCES} | perl -pe "s/^{([^}]+)} _XML\/[^ \\n]+\$/\\1\\/XML/; while (s/({[^} ]*) /\\1_/g) {}; s/{(.*?)}\s*\S+\//\1\//g" | tee MElt_tmp.melt1 \
	| ${BINDIR}/MElt_lemmatizer.pl | tee MElt_tmp.lem1 \
	| ${BINDIR}/MElt2ilex_candidate_entries.pl /usr/local/share/alexina/${LANGUAGE}/edylex_data | tee MElt_tmp.ilex1 \
	| perl /usr/local/share/lefff/morpho.${LANGUAGE}.dir -nosep \
	| ${BINDIR}/inflected_candidate_entries2ilex.pl | tee MElt_tmp.ilex2 \
	| perl /usr/local/share/lefff/morpho.${LANGUAGE}.dir -nosep \
	| ./data/lex2ftb4cats.pl -stdout | cat - ${MODEL}/lexicon.ftl | sort -u > MElt_tmp.new_lexicon.ftl
    PYTHONPATH=$PYTHONPATH python @bindir@/serialize_lefff.py MElt_tmp.new_lexicon.ftl -p MElt_tmp.new_lexicon.json
    cat MElt_tmp.input | sxpipe-light -u ${SXPIPE_OPTIONS} | PYTHONPATH=$PYTHONPATH python @pythondir@/melt/pos_tag.py -m ${MODEL} -d ${TAGDICO} -l MElt_tmp.new_lexicon.json -c ${PRINT_PROBAS} ${LOWERCASE_UPPERCASE_SENTENCES} | perl -pe "s/^{([^}]+)} _XML\/[^ \\n]+\$/\\1\\/XML/; while (s/({[^} ]*) /\\1_/g) {}; s/{(.*?)}\s*\S+\//\1\//g"
else
    tee MElt_tmp.input | PYTHONPATH=$PYTHONPATH python @pythondir@/melt/pos_tag.py -m ${MODEL} -d ${TAGDICO} -l ${LEXICON} ${HANDLE_COMMENTS} ${PRINT_PROBAS} ${LOWERCASE_UPPERCASE_SENTENCES} | tee MElt_tmp.melt1 \
	| ${BINDIR}/MElt_lemmatizer.pl | tee MElt_tmp.lem1 \
	| ${BINDIR}/MElt2ilex_candidate_entries.pl /usr/local/share/alexina/${LANGUAGE}/edylex_data | tee MElt_tmp.ilex1 \
	| perl /usr/local/share/lefff/morpho.${LANGUAGE}.dir -nosep \
	| ${BINDIR}/inflected_candidate_entries2ilex.pl | tee MElt_tmp.ilex2 \
	| perl /usr/local/share/lefff/morpho.${LANGUAGE}.dir -nosep \
	| ./data/lex2ftb4cats.pl -stdout | cat - ${MODEL}/lexicon.ftl | sort -u > MElt_tmp.new_lexicon.ftl
    PYTHONPATH=$PYTHONPATH python @bindir@/serialize_lefff.py MElt_tmp.new_lexicon.ftl -p MElt_tmp.new_lexicon.json
    cat MElt_tmp.input | PYTHONPATH=$PYTHONPATH python @pythondir@/melt/pos_tag.py -m ${MODEL} -d ${TAGDICO} -l MElt_tmp.new_lexicon.json ${HANDLE_COMMENTS} ${PRINT_PROBAS} ${LOWERCASE_UPPERCASE_SENTENCES}
fi
