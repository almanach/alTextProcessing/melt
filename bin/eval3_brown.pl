#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$referencefile = shift || die "Please provide the name of the reference file";
$predictionfile1 = shift || die "Please provide the name of the first predictions file";
$predictionfile2 = shift || die "Please provide the name of the second predictions file";

open REF, "<$referencefile" || die "Could not open $referencefile: $!";
binmode REF, ":utf8";
open PRED1, "<$predictionfile1" || die "Could not open $predictionfile1: $!";
binmode PRED1, ":utf8";
open PRED2, "<$predictionfile2" || die "Could not open $predictionfile2: $!";
binmode PRED2, ":utf8";

my ($r, $p1, $p2);

while ($p1=<PRED1>) {
  next if $p1 =~ /^\s*$/;
  while ($r=<REF>) {
    last unless $r =~ /^\s*$/;
  }
  while ($p2=<PRED2>) {
    last unless $p2 =~ /^\s*$/;
  }
  chomp ($p1);
  chomp ($p2);
  chomp ($r);
  $p1 =~ s/^ +//;   $p1 =~ s/ +$//; 
  $p2 =~ s/^ +//;   $p2 =~ s/ +$//; 
  $r =~ s/^ +//;   $r =~ s/ +$//; 
  @p1 = split (/ +/, $p1);
  @p2 = split (/ +/, $p2);
  @r = split (/ +/, $r);
  if ($#r != $#p1 || $#r != $#p2 || $#p1 != $#p2) {die "Error: different number of tokens in the following sentence:\nPRED1 = $p1\nPRED2 = $p2\nREF = $r\n";}
  for $i (0..$#r) {
    $rt = $r[$i];
    $p1t = $p1[$i];
    $p2t = $p2[$i];
    $rt =~ s/^(.*)\///;
    $w = $1;
    $p1t =~ s/^.*\///;
    $p2t =~ s/^.*\///;
    print STDERR "$w\t$rt\t$p1t\t$p2t";
    if ($rt ne $p1t && $rt ne $p2t) {
      print STDERR "\tDIFF";
      $c++;
    } elsif ($rt ne $p1t) {
      print STDERR "\tDIFF1";
      $a++;
    } elsif ($rt ne $p2t) {
      print STDERR "\tDIFF2";
      $b++;
    }
    print STDERR "\n";
    $tot++;
  }
}
print "Pred 1: ".($tot-$a-$c)/$tot."\n";
print "Pred 2: ".($tot-$b-$c)/$tot."\n";
print "P-value: ".abs(($a-$b)/sqrt($a+$b))."\t(significant at an \\alpha=.05 significance level if p-value > 1.95)\n";
