#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

while (<>) {
  chomp;
  if (/<\?xml.* encoding="(.*?)"\?>/) {
    binmode STDIN, ":encoding($1)";
  }
  s/\r//g;
  s/^\s+//;
  s/\s+$//;

  s/(<p[ >]+>)/\n\1\n/g;
  s/<\/p>/\n<\/p>\n/g;

  # s/<([^ >]+)([^>]*)>\s*([^ ]+)\s*<\/\1>/{◁\1\2▷\3◁\/\1▷} \3/g;
  # s/<([^ >]+)([^>]*)>\s*([^ <>]+) ([^ <>]+)\s*<\/\1>/{◁\1\2▷\3} \3 {\4◁\/\1▷} \4/g;
  # s/<([^ >]+)([^>]*)>\s*([^ <>]+) ([^<>]+) ([^ <>]+)\s*<\/\1>/{◁\1\2▷\3} \3 \4 {\5◁\/\1▷} \5/g;
  # s/^<([^ >]+)([^>]*)>(.*)<\/\1>$/<\1\2>MElt_tmp_token:inserted_newline_after\n\3\nMElt_tmp_token:inserted_newline_before<\/\1>/g;
  # s/\s*<([^>]+)>\s*/\nMElt_tmp_token:inserted_newline_before<\1>MElt_tmp_token:inserted_newline_after\n/g;
  # s/^\nMElt_tmp_token:inserted_newline_before//g;
  # s/MElt_tmp_token:inserted_newline_after\n$//g;
  # s/MElt_tmp_token:(inserted_newline_(?:before|after))/<\1\/>/g;
  # s/^\s+//;
  # s/\s+$//;

  # s/(^|\n)\s*(<[\!\?]?[\w\.:_-]+(?: .*?)?\/?>(?:.*<\/[\w\.:_-]+>)?)\s*(\n|$)/$1\{$2} _XML$3/g;
  # s/(^|\n)\s*((?:<\/?[\w\.:_-]+[^>]*\/?>)+)\s*(\n|$)/$1\{$2} _XML$3/g;
  # s/(^|\n)\s*(<\!--[^>]+>)\s*(\n|$)/$1\{$2} _XML$3/g;
  # s/(^|\n)\s*(<\/[\w\.:_-]+>)\s*(\n|$)/$1\{$2} _XML$3/g;

  # tr/◁▷/<>/;

  print $_;
}

__END__

<a>
  ceci est un <b>superbe</b> texte
</a>
