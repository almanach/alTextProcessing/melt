#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$training_corpus = shift || die "Please provide the training corpus file name as an argument (the corpus must be in the Brown format)";
$boolean = shift || 0; # 0 = untag known words whatever their tag ;  1 = untag known word/tag pairs

open TRAIN, "<$training_corpus" || die "Could not open file '$training_corpus': $!";
binmode TRAIN, ":utf8";
while (<TRAIN>) {
  chomp;
  s/^ +//;
  s/ +$//;
  for (split (/ +/, $_)) {
    /^(.*)\/([^\/]+)$/ || die "Token error: $_";
    if ($boolean) {
	$known_tag_token{$2}{$1} = 1;
    } else {
	$known_tag_token{$1} = 1;
    }
  }
}
close TRAIN;

while (<>) {
  chomp;
  s/^ +//;
  s/ +$//;
  $firstword = 1;
  for (split (/ +/, $_)) {
    if ($firstword) {
      $firstword = 0;
    } else {
      print " ";
    }
    /^(.*)\/([^\/]+)$/ || die "Token error: $_";
    if ($boolean) {
	if (defined($known_tag_token{$2}{$1})) {
	    print $1;
	} else {
	    print $_;
	}
    } else {
	if (defined($known_tag_token{$1})) {
	    print $1;
	} else {
	    print $_;
	}
    }
  }
  print "\n";
}
