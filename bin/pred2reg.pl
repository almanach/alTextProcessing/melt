#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

while (<>) {
  chomp;
  /^(.*?)\t(.*?)\t.*?\t(.*)$/ || die $_;
  $pred = $1;
  $gold = $2;
  $entry = $3;
  print $entry."\t";
  if ($pred =~ /(^|\|)$gold(\||$)/) {
    print "1";
  } else {
    print "0";
  }
  print "\n";
}
