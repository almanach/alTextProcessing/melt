#!/bin/perl
# Usage: cat my_corpus.ptb | perl reinject_tagging_in_treebank.pl my_corpus.automatically_tagged.brown > my_corpus_with_reinjected_tags.ptb

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$tagged_file = shift || die "Please provide a tagged (Brown-format) version of the treebank as an argument";
open TAGGED, "<$tagged_file" || die "Could not open $tagged_file: $!";
while (<>) {
  chomp;
  next if (/^\s*$/);
  $tagged = <TAGGED> || die "Not enough lines in the tagged file";
  while ($tagged =~ /^\s*$/) {
    $tagged = <TAGGED> || die "Not enough lines in the tagged file";
  }
  chomp($tagged);
  $tagged =~ s/^\s+//;
  $tagged =~ s/\s+$//;
  @tokens = ();
  @tags = ();
  for (split / +/, $tagged) {
    if (/^(.*)\/([^\/]+)$/) {
      $token = $1;
      $tag = $2;
      $token =~ s/\^\^/\//g;
      push @tokens, $token;
      push @tags, $tag;
    }
  }
  s/^\s+//;
  s/\s+$//;
  $output = "";
  $i = -1;
  $orig_sentence = $_;
  while (s/^(.*?)\(([^ \(\)]+) ([^ \(\)]+)\)//) {
    $i++;
    $pref = $1;
    $tag = $2;
    $token = $3;
    if ($token ne $tokens[$i]) {
      die "Misalignment on following sentence ($token vs. $tokens[$i]):\n$orig_sentence"; 
    }
    if ($tags[$i] ne "0") {
      $output .= $pref."(".$tags[$i]." $token)";
    } else {
      $output .= $pref."($tag $token)";
    }
  }
  $output .= $_;
  print "$output\n";
}
