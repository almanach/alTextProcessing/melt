#!/usr/bin/perl

# Marie Candito

use Getopt::Long;
$help = 0 ;
$outsep='space' ;
$dumpregexp = 0 ;
GetOptions("notnum!" => \$notnum,
	   "cmpdfile:s" => \$cmpdfile,
	   "dumpregexp!" => \$dumpregexp,
           "help!" => \$help) or die (&usage) ;

die(&usage) unless ($outsep eq 'newline' or $outsep eq 'space') ;
die(&usage) if ($help) ;

#pb avec "nombre de" reconnu comme déterminant
# mais "un grand/petit/faible nombre de"...

# TODO : ajouter "par ex." ?

# ajouter?: (cf. corpus out-of-domain)
# par le biais de
# en pr�sence de
# � proximit� de
# en fin de compte
# par ex.
sub usage
{
    return "Matches compounds in a tokenized text.
For recognized compounds, the space that separates the components is replaced by a _
Compounds are taken from a built-in list, or from a file
And numerical expressions are recognized as compounds (200 000 => 200_000)
$0 
[-notnum] do not transform 200 000 into 200_000 (Default=none)
[-cmpdfile = FILE containing normalised forms of compounds. If set, compounds are loaded from this file, instead of from the default compound list. Default=none]
[-dumpregexp] If set, the script compiles and dump the regexp used for compound recognition. Default=false
[ -help ]\n" ;
}

if ($dumpregexp)
{
    print "COUCOU\n" ;
    # use Regexp::List only when dumping the regexp
    # => otherwise Regexp::List needs not be installed
    eval("use Regexp::List") ;

#3.3 marks differences between v1 and v3.3 (and possibly in between versions...)
    # -------------------------------------
    # default compounds
    @Compounds = (
	'a contrario',#3.3
	'a fortiori',
	'a posteriori',
	'a priori',
	'ad hoc',#3.3
	'afin de',
	'afin que',
	'ainsi de suite',
	'ainsi que',
	'alors même que',
	'alors que',
	'après que',
	'au bord de',#3.3
	'au bout de',
	'au contraire',
	'au cours de',
	'au delà de',#3.3
	'au demeurant',#3.3
	'au dessous de',
	'au dessus de',
	'au début de',#3.3
	'au détriment de',
	'au fil de',
	'au fur et à mesure que',
	'au fur et à mesure',
	'au jour le jour',
	'au lieu de',
	'au milieu de',#3.3
	'au moins',
	'au moment de',#3.3
	'au moment où',
	'au niveau de',
#'au nom de',#3.3
	'au nombre de',#3.3
	'au plus fort de',#3.3
	'au plus tard',#3.3
	'au plus vite',#3.3
	'au prix de',
	'au profit de',
	'au sein de',
	'au sujet de',#3.3
	'au titre de',#3.3
	'au total',
	'au vu de',
	'au-delà de',
	'au-dessous de',
	'au-dessus de',
	'aujourd\' hui',
	'auprès de',
	'autour de',
	'autrement dit',
	'aux alentours de',
	'avant de',
	'avant même',
	'avant que',
	'bel et bien',
	'bien que',
	'bien sûr',
	'bon an mal an',
	'c\' est pourquoi',
	'c\' est-à-dire',
	'chaque fois que',#3.3
	'comme si',
	'compte tenu de',
	'contrairement à',
	'd\' abord',
	'd\' accord',
	'd\' ailleurs',
	'd\' après',
	'd\' autant plus que',
	'd\' autant plus',
	'd\' autant que',
	'd\' autant',
	'd\' autre part',
	'd\' autres',
	'd\' emblée',
	'd\' entre',
	'd\' ici là',#3.3
	'd\' ici à',
	'd\' ici',
	'd\' ores et déjà',
	'd\' où',
	'd\' une part',
	'dans ces conditions',
	'dans l\' immédiat',
	'dans la mesure où',
	'dans le but de',#3.3
	'dans le cadre de',
	'dans le cas de',#3.3
	'dans le domaine de',
	'dans le passé',
	'dans son ensemble',
	'de base',
	'de facto',
	'de fait',
	'de l\' ordre de',
	'de la part de',
	'de loin',
	'de longue date',
	'de manière à',#3.3
	'de moins en moins',
	'de même que',
	'de nature à',
	'de nouveau',
	'de part et d\' autre',
	'de plus en plus',
	'de surcroît',
	'de temps à autre',#3.3
	'depuis peu',
	'depuis que',
	'du coup',
	'du fait de',
	'du moins',
	'dès lors que',
	'dès lors',
	'dès que',
	'en accord avec',#3.3
	'en baisse',
	'en cas de',
	'en cause',
	'en ce qui concerne',
	'en ce sens',
	'en commun',
	'en compte',
	'en cours de',
	'en dehors de',
	'en dessous de',#3.3
	'en dessous',#3.3
	'en direction de',#3.3
	'en dépit de',
	'en effet',#3.3
	'en faveur de',
	'en fin de',#3.3
	'en fonction de',
	'en grande partie',
	'en général',
	'en hausse',
	'en jeu',
	'en l\' absence de',#3.3
	'en l\' occurrence',
	'en la matière',
	'en liaison avec',#3.3
	'en matière de',
	'en moyenne',
	'en même temps que',
	'en même temps',
	'en outre',
	'en particulier',
	'en partie',
	'en place',
	'en plus',
	'en principe',
	'en présence de',#3.3
	'en quelque sorte',#3.3
	'en question',
	'en quête de',#3.3
	'en raison de',
	'en revanche',
	'en réalité',
	'en tant que',
	'en termes de',
	'en tout cas',
	'en train de',
	'en vain',#3.3
	'en vertu de',
	'en vigueur',
	'en voie de',
	'en vue de',
	'en échange de',#3.3
	'en-dessous de',#3.3
	'en-dessous',#3.3
	'entre autres choses',
	'entre autres',
	'et / ou',
	'et même',
	'et puis',
	'ex aequo',
	'ex nihilo',#3.3
	'face à',
	'grosso modo',
	'grâce à',
	'hors de',
	'il est vrai',
	'il y a',
	'in extremis',
	'in fine',#3.3
	'in vitro',
	'in vivo',
	'jusqu\' alors',
	'jusqu\' en',
	'jusqu\' ici',
	'jusqu\' à ce que',
	'jusqu\' à présent',
	'jusqu\' à',
	'l\' on',#3.3
	'la plupart',
	'le cas échéant',
	'le plus souvent',
	'loin de',
	'lors de',
	'manu militari',#3.3
	'mot à mot',
	'même si',
	'n\' importe comment',
	'n\' importe où',
	'n\' importe quand',
	'n\' importe quel',
	'n\' importe quelle',
	'n\' importe quelles',
	'n\' importe quels',
	'n\' importe qui',
	'n\' importe quoi',
	'ne pas',
	'ne plus',
#'nombre de',#3.3
	'non plus',
	'non seulement',
	'nulle part',
	'off shore',#3.3
	'ou bien',
	'où que ce soit',#3.3
	'par ailleurs',
	'par conséquent',
	'par contre',#3.3
	'par définition',#3.3
	'par exemple',
	'par la suite',
	'par rapport à',
	'parce que',
	'pas encore',
	'pendant que',
	'petit à petit',
	'peu à peu',
	'plus ou moins',
	'plus que jamais',#3.3
	'plus tard',
	'plutôt que',
	'pour autant',
	'pour ce faire',#3.3
	'pour de bon',
	'pour l\' heure',
	'pour l\' instant',
	'pour la première fois',
	'pour le moment',#3.3
	'pour que',
	'près de',
	'quand bien même',#3.3
	'quant à',
	'quelqu\' un',
	'quelque chose',
	'quelque peu',
	'qui plus est',
	'qui que ce soit',#3.3
	'quitte à',
	'quoi que ce soit',#3.3
	'sans doute',
	'sans que',
	'sine die',#3.3
	'sine qua non',#3.3
	'sous forme de',
	'stricto sensu',#3.3
	'success story',#3.3
	'sur le point de',
	'sur place',
	'tandis que',
	'tant et si bien que',#3.3
	'tant que',#3.3
	'tour à tour',
	'tous azimuts',#3.3
	'tout au long de',
	'tout au moins',#3.3
	'tout d\' abord',
	'tout de même',
	'tout de suite',
	'tout à coup',
	'tout à fait',
	'tout à l\' heure',
	'un petit peu',
	'un peu plus',
	'un peu',
	'un tant soit peu',
	'une fois',
	'une sorte de',
	'vis-à-vis de',
	'y compris',
	'à cause de',
	'à ce sujet',
	'à cet égard',
	'à compter de',
	'à condition que',
	'à court terme',
	'à fortiori',#3.3
	'à hauteur de',
	'à l\' amiable',#3.3
	'à l\' encontre de',
	'à l\' exception de',
	'à l\' heure actuelle',#3.3
	'à l\' intérieur de',#3.3
	'à l\' inverse de',#3.3
	'à l\' issue de',
	'à l\' occasion de',
	'à l\' égard de',
	'à l\' étranger',
	'à la faveur de',#3.3
	'à la fin de',#3.3
	'à la fois',
	'à la suite de',
	'à la tête de',
	'à long terme',
	'à mesure que',
	'à moitié',
	'à moyen terme',
	'à nouveau',
	'à partir de',
	'à peine',
	'à posteriori',#3.3
	'à priori',#3.3
	'à propos de',
	'à présent',#3.3
	'à raison de',
	'à terme',
	'à tout le moins',#3.3
	'à travers',
#'à venir',#3.3
	'à vrai dire',
	'ça et là' #3.3
)
 ;

# normalise compounds
    @Compounds = map
    {
	if (/(.+?) que$/)
	{
	    ($1.' que', $1.' qu\'') ;
	}
	elsif (/(.+?) de$/)
	{
	    ($1.' de', $1.' d\'', $1.' du', $1.' des') ;
	}
	elsif (/(.+?) si$/)
	{
	    ($1.' si', $1.' s'."'") ;
	}
	elsif (/(.+?) à$/)
	{
	    ($1.' à', $1.' au', $1.' aux') ;
	}
	else
	{
	    $_ ;
	}
	#s/ que$/ qu[e\']/g ;
	#s/ de$/ d(?:es?|u|\')/g ;
	#s/ si$/ s[i\']/g ;
    } @Compounds ;
    
# build a regexp for these compounds
# TODO => handle compounds that are substrings of others ...


# an optimized regexp 
    $o  = Regexp::List->new;
    $reCompounds = $o->set(modifiers => 'i')->list2re(@Compounds) ;
    print "$reCompounds\n" ;
}
else
{
    $reCompounds = qr/(?-xism:(?i:(?=[\à\çabcdefghijlmnopqstuvy])(?:a(?:\ (?:p(?:oste)?riori|contrario|fortiori)|fin\ (?:d(?:es?|[\'u])|qu[e\'])|insi\ (?:qu[e\']|de\ suite)|lors\ (?:m\ême\ qu[e\']|qu[e\'])|pr\ès\ qu[e\']|u(?:\ (?:bo(?:rd\ d(?:es?|[\'u])|ut\ d(?:es?|[\'u]))|co(?:urs\ d(?:es?|[\'u])|ntraire)|d(?:e(?:l\à\ d(?:es?|[\'u])|ss(?:ous\ d(?:es?|[\'u])|us\ d(?:es?|[\'u]))|meurant)|\é(?:but\ d(?:es?|[\'u])|triment\ d(?:es?|[\'u])))|f(?:il\ d(?:es?|[\'u])|ur\ et\ \à\ mesure(?:\ qu[e\'])?)|lieu\ d(?:es?|[\'u])|m(?:ilieu\ d(?:es?|[\'u])|o(?:ment\ (?:d(?:es?|[\'u])|o\ù)|ins))|n(?:iveau\ d(?:es?|[\'u])|ombre\ d(?:es?|[\'u]))|p(?:lus\ (?:fort\ d(?:es?|[\'u])|tard|vite)|r(?:ix\ d(?:es?|[\'u])|ofit\ d(?:es?|[\'u])))|s(?:ein\ d(?:es?|[\'u])|ujet\ d(?:es?|[\'u]))|t(?:itre\ d(?:es?|[\'u])|otal)|vu\ d(?:es?|[\'u])|jour\ le\ jour)|\-de(?:l\à\ d(?:es?|[\'u])|ss(?:ous\ d(?:es?|[\'u])|us\ d(?:es?|[\'u])))|pr\ès\ d(?:es?|[\'u])|t(?:our\ d(?:es?|[\'u])|rement\ dit)|x\ alentours\ d(?:es?|[\'u])|jourd\'\ hui)|vant\ (?:d(?:es?|[\'u])|qu[e\']|m\ême)|d\ hoc)|b(?:ien\ (?:qu[e\']|s\ûr)|(?:el\ et\ bie|on\ an\ mal\ a)n)|c(?:\'\ est(?:\ pourquoi|\-\à\-dire)|haque\ fois\ qu[e\']|o(?:m(?:me\ s[i\']|pte\ tenu\ d(?:es?|[\'u]))|ntrairement\ (?:aux?|\à)))|d(?:\'\ (?:a(?:ut(?:ant(?:\ (?:plus(?:\ qu[e\'])?|qu[e\']))?|re(?:\ part|s))|(?:b|cc)ord|(?:illeur|pr\è)s)|e(?:mbl\é|ntr)e|ici(?:\ (?:aux?|l?\à))?|o(?:res\ et\ d\éj\à|\ù)|une\ part)|ans\ (?:l(?:e\ (?:but\ d(?:es?|[\'u])|ca(?:dre\ d(?:es?|[\'u])|s\ d(?:es?|[\'u]))|domaine\ d(?:es?|[\'u])|pass\é)|\'\ imm\édiat|a\ mesure\ o\ù)|ces\ conditions|son\ ensemble)|e(?:\ (?:fa(?:cto|it)|l(?:\'\ ordre\ d(?:es?|[\'u])|a\ part\ d(?:es?|[\'u])|o(?:in|ngue\ date))|m(?:ani\ère\ (?:aux?|\à)|\ême\ qu[e\']|oins\ en\ moins)|n(?:ature\ (?:aux?|\à)|ouveau)|p(?:art\ et\ d\'\ autre|lus\ en\ plus)|(?:bas|temps\ \à\ autr)e|surcro\ît)|puis\ (?:qu[e\']|peu))|u\ (?:fait\ d(?:es?|[\'u])|coup|moins)|\ès\ (?:lors(?:\ qu[e\'])?|qu[e\']))|e(?:n(?:\ (?:c(?:a(?:s\ d(?:es?|[\'u])|use)|e\ (?:qui\ concerne|sens)|o(?:m(?:mun|pte)|urs\ d(?:es?|[\'u])))|d(?:e(?:hors\ d(?:es?|[\'u])|ssous(?:\ d(?:es?|[\'u]))?)|irection\ d(?:es?|[\'u])|\épit\ d(?:es?|[\'u]))|f(?:aveur\ d(?:es?|[\'u])|in\ d(?:es?|[\'u])|onction\ d(?:es?|[\'u]))|g(?:rande\ partie|\én\éral)|l(?:\'\ (?:absence\ d(?:es?|[\'u])|occurrence)|a\ mati\ère|iaison\ avec)|m(?:ati\ère\ d(?:es?|[\'u])|\ême\ temps(?:\ qu[e\'])?|oyenne)|p(?:arti(?:culier|e)|l(?:ace|us)|r(?:\ésence\ d(?:es?|[\'u])|incipe))|qu(?:e(?:lque\ sorte|stion)|\ête\ d(?:es?|[\'u]))|r(?:aison\ d(?:es?|[\'u])|evanche|\éalit\é)|t(?:ant\ qu[e\']|ermes\ d(?:es?|[\'u])|rain\ d(?:es?|[\'u])|out\ cas)|v(?:ertu\ d(?:es?|[\'u])|oie\ d(?:es?|[\'u])|ue\ d(?:es?|[\'u])|ain|igueur)|\échange\ d(?:es?|[\'u])|accord\ avec|(?:(?:bai|hau)ss|outr)e|effet|jeu)|\-dessous(?:\ d(?:es?|[\'u]))?|tre\ autres(?:\ choses)?)|t\ (?:\/\ ou|m\ême|puis)|x\ (?:aequ|nihil)o)|face\ (?:aux?|\à)|gr(?:\âce\ (?:aux?|\à)|osso\ modo)|hors\ d(?:es?|[\'u])|i(?:l\ (?:est\ vrai|y\ a)|n\ (?:vi(?:tr|v)o|extremis|fine))|jusqu\'\ (?:a(?:ux?|lors)|\à(?:\ (?:ce\ qu[e\']|pr\ésent))?|en|ici)|l(?:e\ (?:cas\ \éch\éa|plus\ souve)nt|o(?:in\ d(?:es?|[\'u])|rs\ d(?:es?|[\'u]))|\'\ on|a\ plupart)|m(?:\ême\ s[i\']|anu\ militari|ot\ \à\ mot)|n(?:\'\ importe\ (?:qu(?:el(?:les?|s)?|and|o?i)|comment|o\ù)|e\ p(?:a|lu)s|on\ (?:plus|seulement)|ulle\ part)|o(?:ff\ shore|u\ bien|\ù\ que\ ce\ soit)|p(?:a(?:r(?:\ (?:con(?:s\équent|tre)|rapport\ (?:aux?|\à)|ailleurs|d\éfinition|(?:exempl|la\ suit)e)|ce\ qu[e\'])|s\ encore)|e(?:ndant\ qu[e\']|tit\ \à\ petit|u\ \à\ peu)|lu(?:s\ (?:(?:ou\ moin|que\ jamai)s|tard)|t\ôt\ qu[e\'])|our\ (?:l(?:\'\ (?:heure|instant)|a\ premi\ère\ fois|e\ moment)|qu[e\']|autant|ce\ faire|de\ bon)|r\ès\ d(?:es?|[\'u]))|qu(?:an(?:t\ (?:aux?|\à)|d\ bien\ m\ême)|elqu(?:e\ (?:chose|peu)|\'\ un)|i(?:\ (?:plus\ es|que\ ce\ soi)t|tte\ (?:aux?|\à))|oi\ que\ ce\ soit)|s(?:ans\ (?:qu[e\']|doute)|ine\ (?:die|qua\ non)|ous\ forme\ d(?:es?|[\'u])|u(?:r\ (?:le\ point\ d(?:es?|[\'u])|place)|ccess\ story)|tricto\ sensu)|t(?:an(?:dis\ qu[e\']|t\ (?:et\ si\ bien\ qu[e\']|qu[e\']))|ou(?:t\ (?:au\ (?:long\ d(?:es?|[\'u])|moins)|d(?:e\ (?:m\êm|suit)e|\'\ abord)|\à\ (?:coup|fait|l\'\ heure))|r\ \à\ tour|s\ azimuts))|un(?:\ (?:pe(?:u(?:\ plus)?|tit\ peu)|tant\ soit\ peu)|e\ (?:sorte\ d(?:es?|[\'u])|fois))|vis\-\à\-vis\ d(?:es?|[\'u])|\à\ (?:c(?:ause\ d(?:es?|[\'u])|e(?:\ sujet|t\ \égard)|o(?:mpter\ d(?:es?|[\'u])|ndition\ qu[e\']|urt\ terme))|hauteur\ d(?:es?|[\'u])|l(?:\'\ (?:e(?:ncontre\ d(?:es?|[\'u])|xception\ d(?:es?|[\'u]))|i(?:n(?:t\érieur\ d(?:es?|[\'u])|verse\ d(?:es?|[\'u]))|ssue\ d(?:es?|[\'u]))|occasion\ d(?:es?|[\'u])|\é(?:gard\ d(?:es?|[\'u])|tranger)|(?:amiab|heure\ actuel)le)|a\ (?:f(?:aveur\ d(?:es?|[\'u])|in\ d(?:es?|[\'u])|ois)|suite\ d(?:es?|[\'u])|t\ête\ d(?:es?|[\'u]))|ong\ terme)|m(?:esure\ qu[e\']|o(?:iti\é|yen\ terme))|p(?:artir\ d(?:es?|[\'u])|r(?:opos\ d(?:es?|[\'u])|iori|\ésent)|eine|osteriori)|raison\ d(?:es?|[\'u])|t(?:erme|(?:out\ le\ moin|raver)s)|fortiori|nouveau|vrai\ dire)|y\ compris|\ça\ et\ l\à)))/ ;
#    $reCompounds = qr/(?-xism:(?i:(?=[\à\çabcdefghijlmnopqstuvy])(?:a(?:\ (?:p(?:oste)?riori|fortiori)|fin\ (?:d(?:es?|[\'u])|qu[e\'])|insi\ (?:qu[e\']|de\ suite)|lors\ (?:m\ême\ qu[e\']|qu[e\'])|pr\ès\ qu[e\']|u(?:\ (?:bout\ d(?:es?|[\'u])|co(?:urs\ d(?:es?|[\'u])|ntraire)|d(?:\étriment\ d(?:es?|[\'u])|e(?:l\à\ d(?:es?|[\'u])|ss(?:ous\ d(?:es?|[\'u])|us\ d(?:es?|[\'u]))))|f(?:il\ d(?:es?|[\'u])|ur\ et\ \à\ mesure(?:\ qu[e\'])?)|lieu\ d(?:es?|[\'u])|mo(?:ins|ment\ o\ù)|n(?:iveau\ d(?:es?|[\'u])|om\ d(?:es?|[\'u]))|pr(?:ix\ d(?:es?|[\'u])|ofit\ d(?:es?|[\'u]))|sein\ d(?:es?|[\'u])|vu\ d(?:es?|[\'u])|jour\ le\ jour|total)|\-de(?:l\à\ d(?:es?|[\'u])|ss(?:ous\ d(?:es?|[\'u])|us\ d(?:es?|[\'u])))|pr\ès\ d(?:es?|[\'u])|t(?:our\ d(?:es?|[\'u])|rement\ dit)|x\ alentours\ d(?:es?|[\'u])|jourd\'\ hui)|vant\ (?:d(?:es?|[\'u])|qu[e\']|m\ême))|\à\ (?:p(?:r(?:opos\ d(?:es?|[\'u])|iori)|artir\ d(?:es?|[\'u])|osteriori|eine)|c(?:ause\ d(?:es?|[\'u])|e(?:\ sujet|t\ \égard)|o(?:mpter\ d(?:es?|[\'u])|ndition\ qu[e\']|urt\ terme))|hauteur\ d(?:es?|[\'u])|l(?:\'\ (?:e(?:ncontre\ d(?:es?|[\'u])|xception\ d(?:es?|[\'u]))|issue\ d(?:es?|[\'u])|occasion\ d(?:es?|[\'u])|\é(?:gard\ d(?:es?|[\'u])|tranger))|a\ (?:suite\ d(?:es?|[\'u])|t\ête\ d(?:es?|[\'u])|fois)|ong\ terme)|m(?:esure\ qu[e\']|o(?:iti\é|yen\ terme))|t(?:erme|ravers)|v(?:enir|rai\ dire)|fortiori|nouveau)|b(?:ien\ (?:qu[e\']|s\ûr)|(?:el\ et\ bie|on\ an\ mal\ a)n)|c(?:\'\ est(?:\ pourquoi|\-\à\-dire)|o(?:m(?:me\ s[i\']|pte\ tenu\ d(?:es?|[\'u]))|ntrairement\ (?:aux?|\à)))|d(?:\'\ (?:a(?:ut(?:ant(?:\ (?:plus(?:\ qu[e\'])?|qu[e\']))?|re(?:\ part|s))|(?:b|cc)ord|(?:illeur|pr\è)s)|e(?:mbl\é|ntr)e|ici(?:\ (?:aux?|\à))?|o(?:res\ et\ d\éj\à|\ù)|une\ part)|ans\ (?:l(?:e\ (?:cadre\ d(?:es?|[\'u])|domaine\ d(?:es?|[\'u])|pass\é)|\'\ imm\édiat|a\ mesure\ o\ù)|ces\ conditions|son\ ensemble)|e(?:\ (?:fa(?:cto|it)|l(?:\'\ ordre\ d(?:es?|[\'u])|a\ part\ d(?:es?|[\'u])|o(?:in|ngue\ date))|m(?:\ême\ qu[e\']|oins\ en\ moins)|n(?:ature\ (?:aux?|\à)|ouveau)|p(?:art\ et\ d\'\ autre|lus\ en\ plus)|base|surcro\ît)|puis\ (?:qu[e\']|peu))|u\ (?:fait\ d(?:es?|[\'u])|coup|moins)|\ès\ (?:lors(?:\ qu[e\'])?|qu[e\']))|e(?:n(?:\ (?:c(?:a(?:s\ d(?:es?|[\'u])|use)|e\ (?:qui\ concerne|sens)|o(?:m(?:mun|pte)|urs\ d(?:es?|[\'u])))|d(?:ehors\ d(?:es?|[\'u])|\épit\ d(?:es?|[\'u]))|f(?:aveur\ d(?:es?|[\'u])|onction\ d(?:es?|[\'u]))|g(?:rande\ partie|\én\éral)|l(?:\'\ occurrenc|a\ mati\èr)e|m(?:ati\ère\ d(?:es?|[\'u])|\ême\ temps(?:\ qu[e\'])?|oyenne)|p(?:arti(?:culier|e)|l(?:ace|us)|rincipe)|r(?:aison\ d(?:es?|[\'u])|evanche|\éalit\é)|t(?:ant\ qu[e\']|ermes\ d(?:es?|[\'u])|rain\ d(?:es?|[\'u])|out\ cas)|v(?:ertu\ d(?:es?|[\'u])|oie\ d(?:es?|[\'u])|ue\ d(?:es?|[\'u])|igueur)|(?:(?:bai|hau)ss|outr)e|jeu|question)|tre\ autres(?:\ choses)?)|t\ (?:m\ême|puis)|x\ aequo)|face\ (?:aux?|\à)|gr(?:\âce\ (?:aux?|\à)|osso\ modo)|hors\ d(?:es?|[\'u])|i(?:l\ (?:est\ vrai|y\ a)|n\ extremis)|jusqu\'\ (?:a(?:ux?|lors)|\à(?:\ pr\ésent)?|en|ici)|l(?:e\ (?:cas\ \éch\éa|plus\ souve)nt|o(?:in\ d(?:es?|[\'u])|rs\ d(?:es?|[\'u]))|a\ plupart)|m(?:\ême\ s[i\']|ot\ \à\ mot)|n(?:e\ p(?:a|lu)s|o(?:mbre\ d(?:es?|[\'u])|n\ (?:plus|seulement))|ulle\ part)|p(?:a(?:r(?:\ (?:rapport\ (?:aux?|\à)|ailleurs|(?:exempl|la\ suit)e)|ce\ qu[e\'])|s\ encore)|e(?:ndant\ qu[e\']|tit\ \à\ petit|u\ \à\ peu)|lu(?:s\ (?:ou\ moins|tard)|t\ôt\ qu[e\'])|our\ (?:l(?:\'\ (?:heure|instant)|a\ premi\ère\ fois)|qu[e\']|autant|de\ bon)|r\ès\ d(?:es?|[\'u]))|qu(?:ant\ (?:aux?|\à)|elqu(?:e\ (?:chose|peu)|\'\ un)|i(?:tte\ (?:aux?|\à)|\ plus\ est))|s(?:ans\ (?:qu[e\']|doute)|ous\ forme\ d(?:es?|[\'u])|ur\ place)|t(?:andis\ qu[e\']|ou(?:t\ (?:au\ long\ d(?:es?|[\'u])|d(?:e\ (?:m\êm|suit)e|\'\ abord)|\à\ (?:coup|fait|l\'\ heure))|r\ \à\ tour))|un(?:\ (?:pe(?:u(?:\ plus)?|tit\ peu)|tant\ soit\ peu)|e\ (?:sorte\ d(?:es?|[\'u])|fois))|vis\-\à\-vis\ d(?:es?|[\'u])|ou\ bien|y\ compris|\ça\ et\ l\à\ )))/ ;
}

# add numerical exp.
$reCompounds .= '|[0-9]+(?: [0-9]+)+' unless ($notnum) ;

# -------------------------------------
while(<>) 
{
    chomp ;

    $current = $_ ;
    # TODO : faire un mathc avec /g et pos (plus efficace)
    # CAUTION : very much faster if $reCompounds searched only after blanks
    while ($current =~ /(^|.*? )($reCompounds)( .*|$)/i)#(?> |$)(.*)$/i)
    {
	print $1 ;
	$c = $2 ;
	$current = $3 ;
	#print "FOUND:$c:" ;
	$c =~ s/ /_/go ;
	# hack pour normaliser au (delà/dessous/dessus) avec ou sans tiret
	$c =~ s/^(au|en)_de/$1-de/o ;
	$c =~ s/^à_(priori|fortiori|posteriori)/a_$1/o ;
	print $c ;
    }
    print $current."\n" ;
}
