#!/usr/bin/perl

#Usage standard depuis le dossier data: cat lefff.ftb4tags | ../bin/extract_sublexicons.pl EP.locc
#Fabriquer EP.locc nécessite EP.tok (tokenisé) dans le dossier data, puis make EP.locc

@sizes = (500, 1000, 2000, 5000, 10000, 20000, 50000, 110000);

$locc_file = shift || die "Please provide a file with lemma occurrences (format: col1=occ, col2=lemma)";
$prefix = shift || "lefff";
$suffix = shift || "ftb4tags";

open (LOCC, "<$locc_file") || die "Could not open $locc_file: $!";
while (<LOCC>) {
  chomp;
  s/__V[^\t]*$/__V/;
  /^(\d+)\t(.*)$/ || die "Format error in $locc_file: $_";
  $occ = $1; $lemma = $2;
  next if $lemma =~ /^\*./;
  for (split /\|/, $lemma) {
    $locc{$_} = $occ; 
  }
}
@locc = sort {$locc{$b} <=> $locc{$a}} keys %locc;
%locc = ();
for $i (0..$#locc) {
  $lrank{$locc[$i]} = $i;
}
@locc = ();

for (@sizes) {
  open my $file_handle, ">$prefix\_$_.$suffix" || die "Could not open $prefix\_$_.$suffix: $!";
  $file_handles{$_} = $file_handle;
}

while (<>) {
  chomp;
  /^[^\t]+\t([^\t]+)\t([^\t]+)$/ || die "Format error in input lexicon (expecting col1=form, col2=tag, col3=lemma): $_";
  $lemma = $2."__".$1;
  $lemma =~ s/__V.*$/__V/;
  for $size (@sizes) {
    if (defined($lrank{$lemma}) && $lrank{$lemma} < $size) {
      print {$file_handles{$size}} $_."\n";
    }
  }
}

for (keys %file_handles) {
  close $_;
}
