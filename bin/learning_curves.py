import sys
import os
import codecs
import optparse
import tempfile
from pos_tagger import POSTagger
from result_sink import AccuracySink

##################### I/O ##################################
usage = "usage: %prog [options] <input_file>"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-s", "--train_size", action="store", help="min,step,max for number of training instances", default="1000,1000,10000")
parser.add_option("-t", "--training", action="store", help="training data file")
(options, args) = parser.parse_args()
train_file = options.training
infile = args[0]

_min,step,_max = map( int, options.train_size.split(',') )

# initialize POS Tagger
pos_tagger = POSTagger()

# create data file for entire training data
print >> sys.stderr, "Generating entire training data..."
data_file = pos_tagger.generate_training_data( infile )

# create data files for each slice and train/run tagger
run_files = {}
run_scores = {}
for r in range(_min,_max,step):
    print "Data size", r
    run_files[r] = tempfile.mktemp()
    os.system( 'head -%s %s > %s' %(r,data_file,run_files[r]) )
    # train classifier for tagger
    pos_tagger.classifier.train( run_files[r], quiet=True )
    # create result sink
    sink = AccuracySink()
    # apply tagger to test data
    pos_tagger.apply( infile, sink )
    run_scores[r] = sink.score()
    print run_scores[r]


try:
    import pylab
    pylab.grid(True)
    pylab.title('POS Tagger learning curve')
    pylab.xlabel('Training size (in # of tokens)')
    pylab.ylabel('Accuracy on test data')
    # pylab.plot(run_scores.keys(),run_scores.values())
    pylab.scatter(run_scores.keys(),run_scores.values())
    pylab.savefig('learning_curves.eps')
    pylab.show()
except Exception:
    sizes = run_scores.keys()
    sizes.sort()
    for size in sizes:
        print size, run_scores[size]


