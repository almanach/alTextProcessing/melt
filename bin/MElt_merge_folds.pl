#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$foldsdir = shift || die "Please provide the directory where to find folds";
$suffix = shift || ".melt";

$i =  1;
while (-e "$foldsdir/testfold_$i$suffix") {
  local *FILE;
  open(FILE, "<$foldsdir/testfold_$i$suffix") || die;
  binmode FILE, ":utf8";
  push(@testfolds, *FILE);
  $i++;
}

$nfolds = $i-1;

die "No fold found in directory \"$foldsdir\" with suffix \"$suffix\"" if $nfolds == 0;

print STDERR "Merging $nfolds folds\n";

$n = 0;

while (1) {
  $i = $n % $nfolds;
  $handle = $testfolds[$i];
  $line = <$handle> || last;
  print $line;
  $n++;
}
