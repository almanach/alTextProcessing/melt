#!/usr/bin/perl

# cat data/ftb_spmrl.train.9881 | perl bin/ftb2tt_data.pl data/ftb_spmrl.train.9881 data/lefff.ftb4.MEltlex
# train-tree-tagger -utf8 data/ftb_spmrl.train.9881.ttlex data/ftb_spmrl.train.9881.ttocl data/ftb_spmrl.train.9881.ttcrp data/ftb_spmrl.train.9881.ttout
# cat data/ftb_spmrl.test.1235.tok | perl -pe "s/$/\n<s>\n/; s/ /\n/g" | tree-tagger -token data/ftb_spmrl.train.9881.ttout | perl -pe "s/\t/\//; s/\n/ /;" | perl -pe "s/<s>\/[^ ]+/\n/g" > data/ftb_spmrl.test.1235.treetagged_with_lexicon
# python bin/eval_brown.py -g data/ftb_spmrl.test.1235 -p data/ftb_spmrl.test.1235.treetagged_with_lexicon -l frHDR/lexicon.json

$base = shift || die "Please provide a base for the TreeTagger training";

$lefff = shift || "";

while (<>) {
  chomp;
  /(^| )([^ \t]+)\/([^ ]+) *$/ || die "Format error: $_";
  for (split(/ /,$_)) {
    /^(.*)\/(.*)$/;
    $word = $1;
    $tag = $2;
    $tags{$tag}++;
    $lex{$word}{$tag}++;
    push @corpus, "$word\t$tag";
  }
  $lex{"<s>"}{SENT}++;
  $tags{SENT}++;
  push @corpus, "<s>\tSENT";
}

if ($lefff ne "") {
  open (LEFFF, "<$lefff") || die "Could not open $lefff: $!";
  while (<LEFFF>) {
    chomp;
    /^([^\t]+)\t([^\t]+)/ || next;
    $lex{$1}{$2}++;
  }
}

open (CRP, ">$base.ttcrp") || die "Could not open $base.ttcrp: $!";
open (OCL, ">$base.ttocl") || die "Could not open $base.ttocl: $!";
open (LEX, ">$base.ttlex") || die "Could not open $base.ttlex: $!";
for (@corpus) {print CRP "$_\tunknown_lemma\n"}
for $word (sort keys %lex) {
  print LEX "$word";
  for (sort keys %{$lex{$word}}) {
    print LEX "\t$_\tunknown_lemma";
  }
  print LEX "\n";
}
for (sort keys %tags) {
  print OCL "$_ " if /^(ADJ|ADV|ET|[VN].*)$/;
}
close (CRP);
close (OCL);
close (LEX);
