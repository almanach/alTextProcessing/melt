#!/usr/bin/perl
#cat ../../../emea-fr-v2/emea-fr-dev.MElt.lemmatized | perl MElt2ilex_candidate_entries.pl lefff.edylex_data

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$edylex_data_file = shift;

open EDF, "<$edylex_data_file" || die "Could not open $edylex_data_file: $!";
binmode EDF, ":utf8";
while (<EDF>) {
  chomp;
  s/^([^\t]*)\t\d+/\1/;
  @line = split /\t/, $_;
  $ed{$line[0]}{$line[1]}{$line[2]} = $line[3] unless ($line[0] eq "adv" && $line[1] !~ /ment$/);
}

while (<>) {
  chomp;
  for (split) {
    next unless s/\/\*/\//;
    next if /[0-9\*\@\+]/;
    next if /[A-ZÉ].*\/.*\// && !/^.[A-Z]{1,3}\/.*\//;
    next if /\/.*\+.*\//;
    next if /^.{1,2}\// && !/^.[A-Z]{1,3}\/.*\//;
    next if /_.*\/.*\//;
#    next if /-.*\/.*\//;
    next if /\/.*\/.*\//;
    /^(.*?)\/(.*?)\/(.*)$/;
    $form = $1;
    $cat = MEltcat2cat($2);
    $lemma = $3;
    if ($cat ne "") {
      for (split /\|/, $lemma) {
	$cat_form_lemma{$cat}{$form}{$_}++;
	$cat_lemma_form{$cat}{$_}{$form}++;
      }
    }
    $form_cat{$form}{$cat}++;
#    print STDERR "\$cat_form_lemma{MEltcat2cat($2)}{$1}{$3}++\n";
  }
}

for $form (keys %form_cat) {
  $best_cat = (sort {$form_cat{$form}{$b} <=> $form_cat{$form}{$a}} %{$form_cat{$form}})[0];
  for (keys %{$form_cat{$form}}) {
    if ($_ ne $best_cat) {
      delete ($form_cat{$form}{$_});
      delete ($cat_form_lemma{$_}{$form});
    }
  }
}

for $cat (keys %cat_form_lemma) {
  for $form (keys %{$cat_form_lemma{$cat}}) {
    for $lemma (keys %{$cat_form_lemma{$cat}{$form}}) {
      $form_lemma = "$form###$lemma";
      print STDERR "$form_lemma\n";
      if ($lemma =~ /^[A-Z]+$/) {
	$candidates{$cat}{$lemma}{inv} += $cat_form_lemma{$cat}{$form}{$lemma};
      } else {
	while (length($form_lemma) > 3) {
	  $form_lemma =~ /^(.*)###(.*)$/;
	  $f = $1;
	  $l = $2;
	  if (defined($ed{$cat}{$f}{$l})) {
	    $candidates{$cat}{$lemma}{$ed{$cat}{$f}{$l}} += $cat_form_lemma{$cat}{$form}{$lemma};
	    #	  print STDERR "\$candidates{$cat}{$lemma}{$ed{$cat}{$f}{$l}} += $cat_form_lemma{$cat}{$form}{$lemma};\n";
	    last;
	  }
	  if ($form_lemma !~ s/^(.)(.*?###)\1/\2/) {
	    $form_lemma = "";
	  }
	}
	if ($form_lemma eq "") {
	  print STDERR "not found: $cat / $form / $lemma\n";
	}
      }
    }
  }
}

for $cat (keys %candidates) {
  for $lemma (keys %{$candidates{$cat}}) {
    for $inflclass (keys %{$candidates{$cat}{$lemma}}) {
      print "$lemma\t$inflclass\t$cat\t$candidates{$cat}{$lemma}{$inflclass}\t";
      print join " _ ", map {$_.":".$cat_lemma_form{$cat}{$lemma}{$_}} keys %{$cat_lemma_form{$cat}{$lemma}};
      print "\n"
#	unless ($candidates{$cat}{$lemma}{$inflclass} <= 1
#	       || (length($lemma) < 5 && $candidates{$cat}{$lemma}{$inflclass} <= 2))
	  ;
    }
  }
}

sub MEltcat2cat {
  my $MEltcat = shift;
  return "nc" if $MEltcat eq "NC";
  return "np" if $MEltcat eq "NPP";
  return "adj" if $MEltcat eq "ADJ";
  return "adjPref" if $MEltcat eq "PREF";
  return "adv" if $MEltcat eq "ADV";
  return "v" if $MEltcat =~ /^V/;
  return "";
}
