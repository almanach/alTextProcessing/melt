#!/usr/bin/env python

import matplotlib
from matplotlib import cm
import pylab
import random
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d






def plot_3D(triple_list,type="wireframe",x_label="R",y_label="C",z_label="Value",title=None):

    fig = pylab.figure() 
    ax = axes3d.Axes3D(fig)

    x_list,y_list,z_list = zip(*triple_list)
    z_dict = dict([((x,y),z) for (x,y,z) in triple_list])
    X, Y = np.meshgrid( x_list,y_list )
    #X = np.array([x_list]*len(x_list))
    #Y = np.array([y_list]*len(y_list))
    Z = np.zeros((len(X), len(Y)))
    for i in range(len(X)):
        for j in range(len(Y)):
            Z[j,i] = z_dict[x_list[i],y_list[j]]
            
    if type=="wireframe": 
        ax.plot_wireframe(X,Y,Z,rstride=10, cstride=10,linestyle="dotted") 
    elif type=="surface": 
        ax.plot_surface(X,Y,Z, rstride=10, cstride=10, cmap=cm.jet) 
    elif type=="contour": 
        ax.contour3D(X,Y,Z)
    elif type=="scatter":
        ax.scatter3D(x_list,y_list,z_list)
    else: 
        raise ValueError("Unknown plot type "+str(type)) 
    
    ax.set_xlabel( x_label ) 
    ax.set_ylabel( y_label ) 
    ax.set_zlabel( z_label )

    #if title: windowtitle(title) 
    #pylab.show._needmain=False 
    
    pylab.show()
    
    return



def plot_2D(x_values,y_values,x_label="X",y_label="Y",title=None):
    pylab.grid(True)
    if title:
        pylab.title( title )
    pylab.xlabel( x_label )
    pylab.ylabel( y_label )
    pylab.scatter( x_values,y_values )
    # pylab.savefig('curves-2d.eps')
    pylab.show()
    return




def plot_2D_contour(triple_list,x_label="X",y_label="Y",title=None):

    x_list,y_list,z_list = zip(*triple_list)
    z_dict = dict([((x,y),z) for (x,y,z) in triple_list])
    X, Y = np.meshgrid( x_list,y_list )
    Z = np.zeros((len(X), len(Y)))
    for i in range(len(X)):
        for j in range(len(Y)):
            Z[j,i] = z_dict[x_list[i],y_list[j]]

    cmap = cm.get_cmap('jet', 100)    # 10 discrete colors

    im = pylab.imshow(Z, cmap=cmap, interpolation='bilinear')
    #pylab.axis('off')
    pylab.colorbar()

    pylab.xlabel( x_label )
    pylab.ylabel( y_label )

    pylab.show()

    # plt.figure()
#     CS = plt.contour(X, Y, Z)
#     plt.clabel(CS, inline=1, fontsize=10)
#     plt.title(title)
    return




if __name__ == '__main__':

    import sys
    import optparse

    usage = "usage: %prog [options] <input_file>"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-d", "--dimension", type=int, default=3)
    parser.add_option("-t", "--type", choices=["wireframe","scatter","surface","contour"], default="wireframe")
    (options, args) = parser.parse_args()

    result_file = args[0]

    # read in scores
    best_acc_scores = {}

    for line in open(result_file):
        # read in scores
        items = line.strip().split()
        items = items[0:3] + items[4:]
        # print items
        try:
            items = map(float,items)
            tb_size,lefff_size,prior,acc,unk_acc = items
            # pick best acc scores for a given tb size and lefff size
            if acc > best_acc_scores.get((tb_size,lefff_size),0):
                best_acc_scores[(tb_size,lefff_size)] = acc
        except ValueError: 
            pass

    # convert to triples
    scores = [(tb,lefff,best_acc_scores[(tb,lefff)]) for tb,lefff in best_acc_scores]
    scores.sort()

    # plot    
    if options.dimension == 2:
        X,Y,Z = zip(*scores)
        x_label = 'Training size (in # of sentences)'
        # x_label = 'Dictionary size (in # of lemmas)'
        y_label = 'Accuracy on test data'
        # y_label = 'Dictionary size (in # of lemmas)'
        plot_2D( X, Y, x_label, y_label, title="POS Tagger learning curve" )
        # plot_2D_contour( scores, x_label, y_label, title="POS Tagger learning curve" )
        
    else:
        plot_3D(scores,type=options.type,
                x_label="Training size (in # of sentences)",
                y_label="Dictionary size (in # of lemmas)",
                z_label="Accuracy on test data",
                title="POS Tagger learning curve")






