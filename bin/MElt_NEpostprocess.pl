#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$lang = "fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-l(?:ang(?:age)?)?$/) {$lang=shift || die "Please provide a language code after -l option (en, fr)";}
}

while (<>) {
  chomp;
  s/{([^{} ]*)} *[^_ ][^ ]*\/([^\/ ]+)/\1\/\2/g;
  s/}\s*1?9+(\/|$)/} _NUM\1/g;
  if ($lang eq "fr") {
    s/_NUM\/[^ ]+ {%} _DIMENSION/_NUM\/DET {%} _DIMENSION/g;
    s/(années {[^{}]+} _NUM)\/[^ ]+/$1\/NC/g;
  }
  while (s/^(.*?)\{ *([^{}]*?) *\} *_([^ \/]+)\/([^ \/]+)//) {
    print $1;
    $tokens = $2;
    $type = $3;
    $tag = $4;
    $rtokens = $tokens;
    $rtokens =~ s/\/[A-Z]+( |$)/\1/g;
    if ($type eq "NUM") {
      $bool = 0;
      if ($rtokens =~ s/^([\+\-]) //) {
	print $1."/ADV";
	$bool = 1;
      }
      for (split / /, $rtokens) {
	print " " if $bool;
	$bool = 1;
	if (/\d/) {
	  print $_."/$tag";
	} else {
	  print $_."/PONCT";
	}
      }
    } elsif ($type eq "ROMNUM") {
      print $rtokens."/".$tag;
    } elsif ($type eq "META_TEXTUAL_PONCT") {
      $bool = 0;
      for (split / /, $rtokens) {
	print " " if $bool;
	$bool = 1;
	if (/^\d+$/) {
	  print $_."/ADJ";
	} else {
	  print $_."/PONCT";
	}
      }
    } elsif ($type eq "META_TEXTUAL_GN") {
      $bool = 0;
      for (split / /, $rtokens) {
	print " " if $bool;
	$bool = 1;
	if (/^\d+$/) {
	  print $_."/ADJ";
	} else {
	  print $_."/PONCT";
	}
      }
    } elsif ($type eq "ADRESSE") {
      $bool = 0;
      for (split / /, $rtokens) {
	print " " if $bool;
	$bool = 1;
	if (/^\d+$/) {
	  print $_."/NC";
	} elsif (/^[,\-]$/) {
	  print $_."/PONCT";
	} elsif (/^de$/) {
	  print $_."/P";
	} elsif (/^S(?:ain)te?s?$/) {
	  print $_."/ADJ";
	} elsif (/^[A-Z]/) {
	  print $_."/NPP";
	} else {
	  print $_."/NC";
	}
      }
    } elsif ($type eq "DIMENSION") {
      print $rtokens."/NC";
    } elsif ($type eq "DATE") {
      if ($rtokens =~ /^\d{4}$/) {
	print $rtokens."/NC";
      } elsif ($rtokens =~ /^\d+(?:er|nd|[eè]me|[eè])?$/) {
	print $rtokens."/ADJ";
      } elsif ($rtokens =~ /^(\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/ADJ $2/NC";
      } elsif ($rtokens =~ /^(\d+(?:er|nd|[eè]me|[eè])?) (-) (\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/ADJ $2/PONCT $3/ADJ $4/NC";
      } elsif ($rtokens =~ /^(\d+(?:er|nd|[eè]me|[eè])?) (-\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/ADJ $2/ADJ $3/NC";
      } elsif ($rtokens =~ /^(\d+(?:er|nd|[eè]me|[eè])?) (-) (\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*) (\d+)$/) {
	print "$1/ADJ $2/PONCT $3/ADJ $4/NC $5/NC";
      } elsif ($rtokens =~ /^(\d+(?:er|nd|[eè]me|[eè])?) (-\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*) (\d+)$/) {
	print "$1/ADJ $2/ADJ $3/NC $4/NC";
      } elsif ($rtokens =~ /^([XIV]+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/ADJ $2/NC";
      } elsif ($rtokens =~ /^(\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*) (prochains?|derniers?)$/) {
	print "$1/ADJ $2/NC $3/ADJ";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) (\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/NC $2/ADJ $3/NC";
      } elsif ($rtokens =~ /^(\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*) (\d+)$/) {
	print "$1/ADJ $2/NC $3/NC";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) (\d+(?:er|nd|[eè]me|[eè])?) ([^ ]*[A-Za-z][^ ]*) (\d+)$/) {
	print "$1/NC $2/ADJ $3/NC $4/NC";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) (\d+)$/) {
	print "$1/NC $2/NC";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) (-) ([^ ]*[A-Za-z][^ ]*) (\d+)$/) {
	print "$1/NC $2/PONCT $3/NC $4/NC";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) (\d+) (-) ([^ ]*[A-Za-z][^ ]*) (\d+)$/) {
	print "$1/NC $2/NC $3/PONCT $4/NC $5/NC";
      } elsif ($rtokens =~ /^(\d+) ([-\/]) (\d+)$/) {
	print "$1/NC $2/PONCT $3/NC";
      } elsif ($rtokens =~ /^(\d{4}) (,) (\d{4})$/) { #"2005 , 2009 et 2010"
	print STDERR "<là>\n";
	print "$1/NC $2/PONCT $3/NC";
      } elsif ($rtokens =~ /^(\d+) ([-\/]\d+)$/) {
	print "$1/NC $2/NC";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) (prochains?|derniers?)$/) {
	print "$1/NC $2/ADJ";
      } elsif ($rtokens !~ / /) {
	print $rtokens."/NC";
      } else {
	print STDERR "$type\t<$tokens>\t$tag\n";
      }
    } elsif ($type eq "HEURE") {
      if ($rtokens =~ /^\d+$/) {
	print $rtokens."/ADJ";
      } elsif ($rtokens !~ / /) {
	print $rtokens."/NC";
      } elsif ($rtokens =~ /^(\d+) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/DET $2/NC";
      } elsif ($rtokens =~ /^(\d+) ([^ ]*[A-Za-z][^ ]*) (\d+)$/) {
	print "$1/DET $2/NC $3/ADJ";
      } elsif ($rtokens =~ /^(\d+) ([^ ]*[A-Za-z][^ ]*) (et)$/) {
	print "$1/DET $2/NC $3/CC";
      } elsif ($rtokens =~ /^(\d+) ([^ ]*[A-Za-z][^ ]*) (\d+) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/DET $2/NC $3/DET $4/NC";
      } else {
	print STDERR "$type\t$tokens\t$tag\n";
      }
    } elsif ($type eq "CURRENCY") {
      $bool = 0;
      if ($rtokens !~ / /) {
	print $rtokens."/NC";
      } elsif ($rtokens =~ /^(\d+(?:,\d+)?) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/DET $2/NC";
      } elsif ($rtokens =~ /^(\d+(?:,\d+)?) (-) (\d+(?:,\d+)?) ([^ ]*[A-Za-z][^ ]*)$/) {
	print "$1/DET $2/PONCT $3/DET $4/NC";
      } else {
	for (split / /, $rtokens) {
	  print " " if $bool;
	  $bool = 1;
	  if (/^\d+$/) {
	    print $_."/DET";
	  } elsif (/^[,]$/) {
	    print $_."/PONCT";
	  } else {
	    print $_."/NC";
	  }
	}
#	print STDERR "$type\t$tokens\t$tag\n";
      }   
    } elsif ($type eq "JURIDID") {
      my $tmp = "";
      if ($rtokens =~ s/ (\.)$//) {
	$tmp = " $1/PONCT";
      }
      if ($rtokens !~ /[ 0-9]/) {
	print $rtokens."/NC";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) ([A-Z])$/) {
	print "$1/NC $2/NC";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) (de) (\d+)$/) {
	print "$1/NC $2/P $3/NC";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) ([^ ]*[0-9][^ ]*)$/) {
	print "$1/NC $2/ADJ";
      } elsif ($rtokens =~ /^([^ ]*[A-Za-z][^ ]*) (n°) ([^ ]*[0-9][^ ]*)$/) {
	print "$1/NC $2/NC $3/ADJ";
      } else {
	print STDERR "$type\t$tokens\t$tag\n";
      }
      print $tmp;
    } elsif ($type eq "NP") {
      if ($rtokens !~ / /) {
	print $rtokens."/NPP"; # ne devrait jamais arriver vu gl_np.pl
      } elsif ($rtokens =~ /^(M\.|Mr\.?|Mme|Me|Mll?e|sir|Sir|lady|Lady|commandant|général) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/NPP";
      } elsif ($rtokens =~ /^(M\.|Mr\.?|Mme|Me|Mll?e|sir|Sir|lady|Lady|commandant|général) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/NPP $3/NPP";
      } elsif ($rtokens =~ /^(M\.|Mr\.?|Mme|Me|Mll?e|sir|Sir|lady|Lady|commandant|général) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/NPP $3/NPP $3/NPP";
      } elsif ($rtokens =~ /^(M\.|Mr\.?|Mme|Me|Mll?e|sir|Sir|lady|Lady|commandant|général) ([A-ZÉÂÊÛÔÎ][^ ]+) (-) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/NPP $3/PONCT $4/NPP $5/NPP";
      } elsif ($rtokens =~ /^(M\.|Mr\.?|Mme|Me|Mll?e|sir|Sir|lady|Lady|commandant|général) ([A-ZÉÂÊÛÔÎ][^ ]+) (-) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/NPP $3/PONCT $4/NPP $5/NPP $6/NPP";
      } elsif ($rtokens =~ /^([A-ZÉÂÊÛÔÎ]\.) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NPP $2/NPP";
      } elsif ($rtokens =~ /^(général) (d'|de) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/ADJ $2/P $3/NPP";
      } elsif ($rtokens =~ /^(général) (du|des) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/ADJ $2/P+D $3/NPP";
      } elsif ($rtokens =~ /^(mère) (du|de|d') ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/P $3/NPP";
      } elsif ($rtokens =~ /^(mère) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/NPP";
      } elsif ($rtokens =~ /^(mère) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/NPP $3/NPP";
      } elsif ($rtokens =~ /^(général) (d'|de) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/ADJ $2/P $3/NPP $4/NPP";
      } elsif ($rtokens =~ /^(général) (du|des) ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/ADJ $2/P+D $3/NPP $4/NPP";
      } elsif ($rtokens =~ /^(mère) (du|de|d') ([A-ZÉÂÊÛÔÎ][^ ]+) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/NC $2/P $3/NPP $4/NPP";
      } elsif ($rtokens =~ /^(le) (roi) (d'|de) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/DET $2/NC $3/P $4/NPP";
      } elsif ($rtokens =~ /^(le) (roi) (du|des) ([A-ZÉÂÊÛÔÎ][^ ]+)$/) {
	print "$1/DET $2/NC $3/P+D $4/NPP";
      } else {
	$bool = 0;
	for (split / /, $tokens) {
	  print " " if $bool;
	  $bool = 1;
	  if (/^de$/) {
	    print $_."/P";
	  } elsif (/^du$/) {
	    print $_."/P+D";
	  } elsif (/^-$/) {
	    print $_."/PONCT";
	  } elsif (/^(M\.|Mr\.?|Mme|Me|Mll?e|Sir|lady|Lady|commandant|général)$/) {
	    print $_."/NC";
	  } elsif (/^[A-ZÉÂÊÛÔÎ]/) {
	    print $_."/NPP";
	  } else {
	    print $_."/NC";
	  }
	}
	print STDERR "$type\t$tokens\t$tag\n";
      }   
    } else {
      print STDERR "$type\t$tokens\t$tag\n";
    }
  }
  print $_."\n";
}
