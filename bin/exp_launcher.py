#!/usr/bin/env python

"""
Example:

...

"""

import sys
import os
import optparse



# TODO features


usage = "usage: %prog [options] <input_file>"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-l", "--lefff", action="store", help="lefff (def: 000,13,24,24ms,g,o)", type=str, default="000,13,24,24ms,g,o")
parser.add_option("-b", "--beam_sizes", action="store", help="beam sizes (def: 1,3,5,10,20)", type=str, default="1,3,5,10,20")
parser.add_option("-e", "--tagsets", action="store", help="tagsets (def: 15,28,g,o,28ms)", type=str, default="15,28,g,o,28ms")
parser.add_option("-t", "--tokenizations", action="store", help="tokenizations (def: 0,3,N)", type=str, default="0,3,N")
parser.add_option("-w", "--windows", action="store", help="context window (def: 1,2,3)", type=str, default="1,2,3")
parser.add_option("-f", "--lefff_features", action="store", help="lefff feature combinations (def: c,l,r,lc,cr,lr,lcr)", type=str, default="c,l,r,lc,cr,lr,lcr")
(options, args) = parser.parse_args()


data_dir = args[0]
MODEL_DIR="./models"
JSON_DIR="./json"
SCORE_DIR="./scores"
OUT_DIR="./outputs"
TRAIN_STEM="ftb4v6_ms_1"
TEST_STEM="ftb4v6_ms_2"
LEX_SUFFIX="lefff"



TAGSETS = options.tagsets.split(',')
TOKENIZATIONS = ['tok'+s for s in options.tokenizations.split(',')]
BEAM_SIZES = options.beam_sizes.split(',')
WINDOWS = options.windows.split(',')
LEFFFS = options.lefff.split(',')
LEFFF_FEATS = options.lefff_features.split(',')

train_corpus = dict([(".".join(f.split(".")[-3:]),os.path.join(data_dir,f)) for f in os.listdir(data_dir)
                     if f.startswith(TRAIN_STEM) and f.split(".")[-1] in TAGSETS])
lexicons = dict([(f,os.path.join(data_dir,f)) for f in os.listdir(data_dir)
            if f.startswith(LEX_SUFFIX) and f.split('_')[-1] in LEFFFS])
test_corpus = dict([(f.split(".")[1],os.path.join(data_dir,f)) for f in os.listdir(data_dir)
                    if f.startswith(TEST_STEM) and f.endswith(".raw")])
eval_corpus = dict([(".".join(f.split(".")[-3:]),os.path.join(data_dir,f)) for f in os.listdir(data_dir)
                    if f.startswith(TEST_STEM) and f.split(".")[-1] in TAGSETS])

base_exp_cmd = "python ../src/melt/pos_tag.py"


#print train_corpus.keys()
#print eval_corpus.keys()
#print lexicons.keys()
#print test_corpus
#sys.exit()

ct = 0

built_lex = {}
built_tag_dict = {}
trained_models = {}


for tok in TOKENIZATIONS:
    test = test_corpus[tok]

    for lex in lexicons:
        lexicon = lexicons[lex]
        compiled_lexicon = os.path.join(JSON_DIR,lex+".json") 
        # get compiled lexicon
        if compiled_lexicon not in built_lex:
            print "echo 'Building lexicon for %s...'" %lex
            build_lefff_cmd = "python ../bin/serialize_lefff.py -p %s %s" %(compiled_lexicon, lexicon)
            print build_lefff_cmd
            built_lex[compiled_lexicon] = 1
                    
        for win in WINDOWS:

            for tagset in TAGSETS:    
                key = ".".join( [tok,"brown",tagset]) 
                train = train_corpus[key]
                gold = eval_corpus[key]
                compiled_tag_dict = os.path.join(JSON_DIR,tagset+".json") 
                # get compiled lexicon
                if compiled_tag_dict not in built_tag_dict:
                    print "echo 'Building tag dico for %s...'" %train
                    build_tag_dict = "python ../bin/build_tag_dict.py -p %s %s" %(compiled_tag_dict,train)
                    print build_tag_dict
                    built_tag_dict[compiled_tag_dict] = 1


                for fs in LEFFF_FEATS:
                    fs_opt = []
                    if 'c' in fs:
                        fs_opt.append( "lex_wd=1" )
                    else:
                        fs_opt.append( "lex_wd=0" )
                    if 'l' in fs:
                        fs_opt.append( "lex_lhs=1" )
                    else:
                        fs_opt.append( "lex_lhs=0" )
                    if 'r' in fs:
                        fs_opt.append( "lex_rhs=1" )
                    else:
                        fs_opt.append( "lex_rhs=0" )
                    fs_opt = ",".join( fs_opt )

                    # file names
                    out_stem = "tok=%s-tag=%s-lex=%s-win=%s-fs=%s" %(tok,tagset,lex,win,fs)
                    model = os.path.join(MODEL_DIR,"model-%s" %out_stem)
                
                    for bs in BEAM_SIZES: 
                        out_stem_b = out_stem + "-b=%s" %bs 
                        out = os.path.join(OUT_DIR,TEST_STEM+out_stem_b+".out")
                        score = os.path.join(SCORE_DIR,TEST_STEM+out_stem_b+".scores")
                        exp_cmd = base_exp_cmd + " -b %s -m %s -l %s -d %s -g %s -o %s -f win=%s,%s" \
                                  %(bs,model,compiled_lexicon,compiled_tag_dict,gold,out,win,fs_opt)
                        if model not in trained_models:
                            exp_cmd += " -t %s" %train
                            trained_models[model] = 1
                        exp_cmd += " %s > %s" %(test,score)
                
                        print "\necho '>>> Experiment %s'" %ct
                        print "echo 'Train/test...'"
                        print exp_cmd
                        ct += 1
    print




