#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$outputdir = shift || die "Please provide an output directory";

@sizemap = (50,100,200,500,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,12000,14000,16000,18000,20000,
25000,30000,35000,40000,45000,50000,60000,70000,80000,90000,100000,120000,140000,160000,180000,200000,250000,30000,350000,400000,450000,500000);

while (<>) {
  push @lines, $_;
}

$i = 1;

while ($sizemap[$i-1] <= $#lines+1) {
  open(FILE, ">$outputdir/subcorpus_$i") || die;
  binmode FILE, ":utf8";
  for (0..$sizemap[$i-1]-1) {
    print FILE $lines[$_];
  }
  close FILE;
  $i++;
}

open(FILE, ">$outputdir/subcorpus_$i") || die;
binmode FILE, ":utf8";
for (0..$#lines) {
  print FILE $lines[$_];
}
close FILE;
