#!/usr/bin/perl
# cat /usr/local/share/perlex/*.lex | recode l2..u8 | perl perlex2MEltfalex.pl | sort -u | perl -pe "s/â/ā/g;s/ł/ħ/g;s/ż/ẓ/g;"> perlex2

use utf8;

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

open CATMS, ">perlex_ms" || die;
# open CAT, ">perlex_noms" || die;
binmode CATMS, ":utf8";
#binmode CAT, ":utf8";

while (<>) {
  chomp;
  next if /^#/;
  s/([^\\])#.*$/\1/;
  /^(.*?)\t.*?\t(.*?)\t.*?\t([^\t]*?)(?:__[^\t]*?)?\t.*?\t(.*?)\t/ || next;
  $form = $1;
  $perlexcat = $2;
  $perlexlemma = $3;
  $perlexms = $4;
  $cat = "";
  $ms = "";
#  next if ($perlexlemma =~ / / && $form !~ / /);
  $form =~ s/ / /;
  $perlexlemma =~ s/ / /;
  if ($perlexcat eq "N") {
    $cat = "N";
    if ($perlexms =~ /Sing/) {
      $ms .= "sing"; 
    } elsif ($perlexms =~ /Pl/) {
      $ms .= "pl";
    } else {
      die "noun '$form' (lemma '$perlexlemma', category $perlexcat) has no number information";
    }
    if ($perlexms =~ /Indef/) {
      $ms .= "_indef"; 
    } elsif ($perlexms =~ /Pers/) {
      $ms .= "_pers"; 
    }
  }
  elsif ($perlexcat eq "ADJ") {
    $cat = "ADJ";
    if ($perlexms =~ /Cmp/) {
      $ms .= "comp"; 
    } elsif ($perlexms =~ /Sup/) {
      $ms .= "sup";
    }
  }
  elsif ($perlexcat eq "V" || $perlexcat eq "VAUX") {
    $cat = "V";
    if ($perlexms =~ /inf/) {
      $ms .= "inf"; 
    } elsif ($perlexms =~ /partPas/) {
      $ms .= "part_pa";
    } elsif ($perlexms =~ /imper/) {
      $ms .= "imper";
    } elsif ($perlexms =~ /partOblig/) {
      $ms .= "part_oblig";
    } elsif ($perlexms =~ /Pastprog/) {
      $ms .= "pastprog";
    } elsif ($perlexms =~ /partPres/) {
      $ms .= "part_pres";
    } elsif ($perlexms =~ /PreSubj/) {
      $ms .= "subj_pres";
    } elsif ($perlexms =~ /PreInd/) {
      $ms .= "ind_pres";
    } elsif ($perlexms =~ /Preterit/) {
      $ms .= "simplpast";
    } elsif ($perlexms =~ /Preparf/) {
      $ms .= "preparf";
    } elsif ($perlexms =~ /Imp/) {
      $ms .= "imp";
      if ($perlexms =~ /Sg/) {
	$ms .= "_sg"; 
      } elsif ($perlexms =~ /Pl/) {
	$ms .= "_pl";
      }
    } elsif ($perlexms =~ /Prohib/) {
      $ms .= "prohib";
      if ($perlexms =~ /Sg/) {
	$ms .= "_sg"; 
      } elsif ($perlexms =~ /Pl/) {
	$ms .= "_pl";
      }
    }
    if ($perlexms =~ /Apocope/) {
      $ms .= "_apo"; 
    }
    if ($perlexms =~ /Comp/) {
      $ms .= "_comp"; 
    }  
    if ($perlexms =~ /Form/) {
      $ms .= "_form"; 
    }  
    if ($perlexms =~ /Fam/) {
      $ms .= "_fam"; 
    }
    if ($perlexms =~ /Neg/) {
      $ms .= "_neg"; 
    }  
  } elsif ($perlexcat eq "MS") {
    $cat = "Nunit";
  }
  elsif ($perlexcat =~ /^(Nunit|PRO(pers|refl|recip|dem|inter|quindef|quant|num|indef|mal)|PN|INT(s|excl)?|RA|ADV.*|AR|CONJ(rel|coord|subord|cond)?|DELM|DET(dem|inter|quant)|P(pn|np|pp)?|CLASS(sing|pl))$/) {   #toléreance sur COND seul à supprimer asap
    $cat = $perlexcat;
  }
  else {
    die "Unknown perlex cat '$perlexcat' found for form '$form' (line '$_')\n";
  }

  print "$form\t$cat$ms\t$perlexlemma\n";

}

close CATMS;
#close CAT;
