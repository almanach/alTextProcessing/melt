#!/usr/bin/perl

use utf8;

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

open CATMS, ">kurlex_ms" || die;
open CAT, ">kurlex_noms" || die;
binmode CATMS, ":utf8";
binmode CAT, ":utf8";

while (<>) {
  chomp;
  next if /^#/;
  s/([^\\])#.*$/\1/;
  /^(.*?)\t.*?\t(.*?)\t.*?\t([^\t]*?)(?:__[^\t]*?)?\t.*?\t(.*?)\t/ || next;
  $form = $1;
  $kurlexcat = $2;
  $kurlexlemma = $3;
  $kurlexms = $4;
  $cat = "";
  $ms = "";
#  next if ($kurlexlemma =~ / / && $form !~ / /);
  next if ($form =~ / /);
  if ($kurlexcat eq "N" || $kurlexcat eq "nc" || $kurlexcat eq "etr") {
    $cat = "N";
    $ms = lc($kurlexms);
    $ms =~ s/^.*?(indef).*$/\1/i || $ms =~ s/^.*?(def).*$/\1/i;
    if ($ms =~ /^(card|ord)$/) {$cat = "NUM"}
  }
  elsif ($kurlexcat =~ /^prn(.*)$/i) {
    $cat = "PRN";
    $ms = lc($1);
    if ($kurlexms =~ /(Nom|Obl|Constr)/) {
      $ms .= lc($1);
    }
  }
  elsif ($kurlexcat eq "pro") {
    $cat = "PRN";
    $ms = "nom";
    $catms{"$form\t$cat$ms\t$kurlexlemma\n"} = 1;
    $cat{"$form\t$cat\t$kurlexlemma\n"} = 1;
    $ms = "obl";
    $catms{"$form\t$cat$ms\t$kurlexlemma\n"} = 1;
    $cat{"$form\t$cat\t$kurlexlemma\n"} = 1;
    $ms = "constr";
  }
  elsif ($kurlexcat eq "V") {
    $cat = "V";
    if ($kurlexms =~ /^P?(Part|Imp|Inf)/) {
      $ms = lc($1);
    } elsif ($kurlexms =~ /Pres/) {
      $ms = "s1";
    } else {
      $ms = "s2";
    }
  }
  elsif ($kurlexcat =~ /^(adj|num)$/i) {
    $cat = uc($kurlexcat);
    $ms = lc($kurlexms);
  }
  elsif ($kurlexcat eq "PART") {
    $cat = uc($kurlexcat);
    if ($kurlexlemma eq "jî") {
      $ms = "emph";
    } elsif ($kurlexlemma =~ /^(ma|çend|çiqas)$/) {
      $ms = "quest";
    } elsif ($kurlexlemma eq "yê" || $kurlexlemma eq "ê") {
      $ms = "constr2";
    } elsif ($kurlexlemma eq "bi") {
      $ms = "deriv";
    } elsif ($kurlexlemma eq "ne") {
      $ms = "neg";
    } else {
      die "Unknown particle: $kurlexlemma\n";
    }
  }
  elsif ($kurlexcat =~ /^det$/i) {
    $cat = uc($kurlexcat);
    if ($kurlexlemma eq "her") {
      $ms = "every";
    } elsif ($kurlexlemma =~ /^_?y?ek$/) {
      $ms = "indef";
    } elsif ($kurlexlemma =~ /^e[vw]$/) {
      $ms = "dems";
    } elsif ($kurlexlemma =~ /^_(?:ROM)?NUM/) {
      $cat = "NUM";
      $ms = "card";
      $catms{"$form\t$cat$ms\t$kurlexlemma\n"} = 1;
      $cat{"$form\t$cat\t$kurlexlemma\n"} = 1;
      $ms = "ord";
    } else {
      die "Unknown determiner: $kurlexlemma\n";
    }
  }
  elsif ($kurlexcat =~ /^(ponct|parent|epsilon|sbound)/i) {
    $cat = "PONCT";
  }
  elsif ($kurlexcat eq "np") {
    $cat = "PN";
  }
  elsif ($kurlexcat =~ /^adv$/i) {
    $cat = "PART";
  }
  elsif ($kurlexcat =~ /^(pn|conj|neg|compl|meta|prep(?:\+prn)?|postp)$/i) {
    $cat = uc($kurlexcat);
  }
  else {
    die "Unknown kurlex cat '$kurlexcat' found for form '$form'\n";
  }
  $catms{"$form\t$cat$ms\t$kurlexlemma\n"} = 1;
  $cat{"$form\t$cat\t$kurlexlemma\n"} = 1;
}

for (sort keys %catms) {
  print CATMS "$_";
}
for (sort keys %cat) {
  print CAT "$_";
}

close CATMS;
close CAT;
