#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $is_lex = 0;

# Usage: cat perceo-lexicon | perl perceo2ftbspmrl.pl > perceo-lexicon.converted OU cat perceo-corpus | perl perceo2ftbspmrl.pl > perceo-corpus.converted

while (<>) {
  chomp;
  if (/^([^\t]+)\t([^\t]+)(.*)$/) {
    die if $is_lex == -1;
    $is_lex = 1;
    $form = $1;
    $tag = $2;
    $remainder = $3;
    $newtag = perceotag2ftbspmrltag($tag);
    $hash{$form."\t".$newtag.$remainder}++ unless $newtag eq "";
  } elsif (/^\s*$/) {
  } else {
    die if $is_lex == 1;
    $is_lex = -1;
    $l = "";
    s/^\s+//;
    while (s/^([^ ]+)\/([^ \/]+)\s*(\s|$)//) {
      $form = $1;
      $tag = $2;
      $newtag = perceotag2ftbspmrltag($tag);
      $l .= " " unless $l eq "";
      if ($newtag eq "") {
	if ($form =~ /^est-ce_(que|qu')$/ && $tag eq "PRT:int") {
	  $l .= "est/V -ce/CLS $1/CS";
	} elsif ($form =~ /^est-ce_qui$/ && $tag eq "PRT:int") {
	  $l .= "est/V -ce/CLS qu'/CS il/CLS";
	} else {
	  die "Unknown tag '$newtag' on form '$form'";
	}
      } else {
	$l .= $form."/".$newtag;
      }
    }
    die "$_" unless $_ eq "";
    print $l."\n";
  }
}

if ($is_lex == 1) {
  for (sort {$a cmp $b} keys %hash) {
    print $_."\n";
  }
}

sub perceotag2ftbspmrltag {
  my $tag = shift;
  if ($tag eq "PRO:cls") {return "CLS"}
  elsif ($tag eq "NOM") {return "NC"}
  elsif ($tag eq "ADV") {return "ADV"}
  elsif ($tag eq "VER:pres") {return "V"}
  elsif ($tag eq "PRP") {return "PREP"}
  elsif ($tag eq "KON") {return "CS"}
  elsif ($tag eq "INT") {return "I"}
  elsif ($tag eq "LOC") {return "LCT"}
  elsif ($tag eq "DET:def") {return "DET"}
  elsif ($tag eq "FNO") {return "I"}
  elsif ($tag eq "PRO:clo") {return "CLO"}
  elsif ($tag eq "VER:infi") {return "VINF"}
  elsif ($tag eq "ADJ") {return "ADJ"}
  elsif ($tag eq "DET:ind") {return "DET"}
  elsif ($tag eq "VER:pper") {return "VPP"}
  elsif ($tag eq "AUX:pres") {return "V"}
  elsif ($tag eq "PRO:ton") {return "PRO"}
  elsif ($tag eq "NAM") {return "NPP"}
  elsif ($tag eq "PRO:rel") {return "PROREL"}
  elsif ($tag eq "VER:impf") {return "V"}
  elsif ($tag eq "NUM") {return "ADJ"}
  elsif ($tag eq "PRO:clsi") {return "CLS"}
  elsif ($tag eq "PRP:det") {return "P+D"}
  elsif ($tag eq "DET:pos") {return "DET"}
  elsif ($tag eq "TRC") {return "TRC"}
  elsif ($tag eq "PRO:int") {return "PROWH"}
  elsif ($tag eq "PRO:dem") {return "PRO"}
  elsif ($tag eq "PRO:ind") {return "PRO"}
  elsif ($tag eq "DET:pre") {return "ADJ"}
  elsif ($tag eq "VER:futu") {return "V"}
  elsif ($tag eq "DET:dem") {return "DET"}
  elsif ($tag eq "NOM:trc") {return "NC"}
  elsif ($tag eq "VER:cond") {return "V"}
  elsif ($tag eq "AUX:impf") {return "V"}
  elsif ($tag eq "MLT") {return "MLT"}
  elsif ($tag eq "VER:impe") {return "VIMP"}
  elsif ($tag eq "NOM:sig") {return "NC"}
  elsif ($tag eq "VER:subp") {return "VS"}
  elsif ($tag eq "PRT:int") {return ""}
  elsif ($tag eq "DET:par") {return "DET"}
  elsif ($tag eq "AUX:pper") {return "VPP"}
  elsif ($tag eq "VER:ppre") {return "VPR"}
  elsif ($tag eq "NAM:sig") {return "NPP"}
  elsif ($tag eq "AUX:cond") {return "V"}
  elsif ($tag eq "DET:int") {return "DETWH"}
  elsif ($tag eq "AUX:infi") {return "VINF"}
  elsif ($tag eq "ADJ:trc") {return "ADJ"}
  elsif ($tag eq "PRO:pos") {return "PRO"}
  elsif ($tag eq "AUX:subp") {return "VS"}
  elsif ($tag eq "NAM:trc") {return "NPP"}
  elsif ($tag eq "AUX:futu") {return "V"}
  elsif ($tag eq "ETR") {return "ET"}
  elsif ($tag eq "VER") {return "V"}
  elsif ($tag eq "EPE") {return "EPE"} # à vérifier
  elsif ($tag eq "VER:trc") {return "V"}
  elsif ($tag eq "AUX:ppre") {return "VPR"}
  elsif ($tag eq "VER:simp") {return "V"}
  elsif ($tag eq "VER:subi") {return "VS"}
  elsif ($tag eq "AUX:impe") {return "VIMP"}
  elsif ($tag eq "AUX:simp") {return "V"}
  elsif ($tag eq "AUX:subi") {return "V"}
  die "Unknown tag '$tag'";
}
