#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use utf8;

# cat /usr/local/share/enlex/?.lex /usr/local/share/enlex/ponct.lex /usr/local/share/enlex/{lostandfound,addons}.lex | recode l1..u8 | grep -v "@?" | perl data/lex2ftb4cats.pl -l en -ms -o data/enlex_ms

# cat /usr/local/share/enlex/?.lex /usr/local/share/enlex/ponct.lex /usr/local/share/enlex/{lostandfound,addons}.lex | recode l1..u8 | grep -v "@?" | perl lex2ftb4cats.pl -l en -ms -o enlex.ms.meltlex
# cat /usr/local/share/enlex/?.lex /usr/local/share/enlex/ponct.lex /usr/local/share/enlex/{lostandfound,addons}.lex | recode l1..u8 | grep -v "@?" | perl lex2ftb4cats.pl -l en -nms -o enlex.meltlex

# cat /usr/local/share/delex/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l de -ms -o delex.ms.meltlex
# cat /usr/local/share/delex/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l de -nms -o delex.meltlex

# cat /usr/local/share/leffe/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l es -ms -o leffe.ms.meltlex
# cat /usr/local/share/leffe/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l es -nms -o leffe.meltlex

# cat /usr/local/share/saldo/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l sv -ms -o saldo.ms.meltlex
# cat /usr/local/share/saldo/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l sv -nms -o saldo.meltlex

# cat /usr/local/share/sloleks/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l si -ms -o sloleks.ms.meltlex
# cat /usr/local/share/sloleks/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l si -nms -o sloleks.meltlex
# cat /usr/local/share/sloleks/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l si -lms -o sloleks.ms2.meltlex

# cat /usr/local/share/lefff/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l fr -ms -o lefff.ms.meltlex
# cat /usr/local/share/lefff/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l fr -nms -o lefff.meltlex
# cat /usr/local/share/lefff/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l fr -lms -o lefff.ms2.meltlex

# cat /usr/local/share/multext-east-4-0-ro/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l ro -ms -o multext-east-4-0-ro.ms.meltlex
# cat /usr/local/share/multext-east-4-0-ro/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l ro -nms -o multext-east-4-0-ro.meltlex
# cat /usr/local/share/multext-east-4-0-ro/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l ro -lms -o multext-east-4-0-ro.ms2.meltlex

# cat /usr/local/share/multext-east-4-0-et/*.lex | perl lex2ftb4cats.pl -l et -ms -o multext-east-4-0-et.ms.meltlex
# cat /usr/local/share/multext-east-4-0-et/*.lex | perl lex2ftb4cats.pl -l et -nms -o multext-east-4-0-et.meltlex
# cat /usr/local/share/multext-east-4-0-et/*.lex | perl lex2ftb4cats.pl -l et -lms -o multext-east-4-0-et.ms2.meltlex

# cat /usr/local/share/multext-east-4-0-bg/*.lex | perl lex2ftb4cats.pl -l bg -ms -o multext-east-4-0-bg.ms.meltlex
# cat /usr/local/share/multext-east-4-0-bg/*.lex | perl lex2ftb4cats.pl -l bg -nms -o multext-east-4-0-bg.meltlex
# cat /usr/local/share/multext-east-4-0-bg/*.lex | perl lex2ftb4cats.pl -l bg -lms -o multext-east-4-0-bg.ms2.meltlex

# cat /usr/local/share/multext-east-4-0-hu/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l hu -ms -o multext-east-4-0-hu.ms.meltlex
# cat /usr/local/share/multext-east-4-0-hu/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l hu -nms -o multext-east-4-0-hu.meltlex
# cat /usr/local/share/multext-east-4-0-hu/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l hu -lms -o multext-east-4-0-hu.ms2.meltlex

# cat /usr/local/share/delex/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l de -ms -o delex.ms.meltlex
# cat /usr/local/share/delex/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l de -nms -o delex.meltlex

# cat /usr/local/share/inmdb/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l ga -ms -o inmdb.ms.meltlex
# cat /usr/local/share/inmdb/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l ga -nms -o inmdb.meltlex

# cat /usr/local/share/leffga/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l gl -ms -o leffga.ms.meltlex
# cat /usr/local/share/leffga/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l gl -nms -o leffga.meltlex

# cat /usr/local/share/dela_gr/*.lex | perl lex2ftb4cats.pl -l el -ms -o dela_gr.ms.meltlex
# cat /usr/local/share/dela_gr/*.lex | perl lex2ftb4cats.pl -l el -nms -o dela_gr.meltlex

# cat /usr/local/share/pollex/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l sk -ms -o pollex.ms.meltlex
# cat /usr/local/share/pollex/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l sk -nms -o pollex.meltlex

# cat /usr/local/share/enlex/?.lex  /usr/local/share/enlex/ponct.lex /usr/local/share/enlex/{lostandfound,addons}.lex | recode l1..u8 | grep -v "@?" | perl lex2ftb4cats.pl -l en -ms -o enlex.ms.meltlex
# cat /usr/local/share/enlex/?.lex  /usr/local/share/enlex/ponct.lex /usr/local/share/enlex/{lostandfound,addons}.lex | recode l1..u8 | grep -v "@?" | perl lex2ftb4cats.pl -l en -nms -o enlex.meltlex

# cat /usr/local/share/morph_it/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l it -ms -o morph_it.ms.meltlex
# cat /usr/local/share/morph_it/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l it -nms -o morph_it.meltlex

# cat /usr/local/share/hml_50_metashare_v2/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l hr -ms -o hml.ms.meltlex
# cat /usr/local/share/hml_50_metashare_v2/*.lex | recode l2..u8 | perl lex2ftb4cats.pl -l hr -nms -o hml.meltlex

# cat /usr/local/share/labellex_pt/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l pt -ms -o labellex_pt.ms.meltlex
# cat /usr/local/share/labellex_pt/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l pt -nms -o labellex_pt.meltlex

# cat /usr/local/share/alpino/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l nl -ms -o alpino.ms.meltlex
# cat /usr/local/share/alpino/*.lex | recode l1..u8 | perl lex2ftb4cats.pl -l nl -nms -o alpino.meltlex

$output_file = "lefff.ftb4tags";
$stdout = 0;
$lang = "fr";
$use_ms = -1;

while (1) {
  $_ = shift;
  if (/^--?all$/) {$all = 1; $use_ms = 0}
  elsif (/^-o$/) {$output_file = shift || die "Option -o must be followed by the name of the output file"}
  elsif (/^-l$/) {$lang = shift || die "Option -l must be followed by the code of the language (e.g., 'fr' or 'en')"}
  elsif (/^-stdout$/) {$output_file = "lex2ftb4cats.tmpfile"; $stdout = 1}
  elsif (/^-nms$/) {$use_ms = 0}
  elsif (/^-ms$/) {$use_ms = 1}
  elsif (/^-lms$/) {$use_ms = 2}
  elsif (/^-synt$/) {$use_synt = 1}
  elsif (/^-vsynt$/) {$use_synt = 2}
  elsif (/^$/) {last}
  else {die "Unknown option '$_'"}
}

if ($use_ms == -1) {
  if ($lang eq "en") {$use_ms = 1}
  elsif ($lang eq "es") {$use_ms = 2}
  else {$use_ms = 0}
}

if ($all) {
  open CAT24MS, ">$output_file.24ms" || die;
  open CAT24, ">$output_file.24" || die;
  open CAT13, ">$output_file.13" || die;
  open CATO, ">$output_file.o" || die;
  open CATG, ">$output_file.g" || die;
  binmode CAT24MS, ":utf8";
  binmode CAT24, ":utf8";
  binmode CAT13, ":utf8";
  binmode CATO, ":utf8";
  binmode CATG, ":utf8";
} else {
  open CAT24, ">$output_file" || die;
  binmode CAT24, ":utf8";
}

while (<>) {
  chomp;
  next if /^#/;
  next if ($lang eq "fr" && /\%ppp_employé_comme_adj/);
  s/([^\\])#.*$/\1/;
  if (/^(.*?)\t.*?\t(.*?)\t(.*?)\t([^\t]*?)(?:__[^\t]*?)?\t.*?\t(.*?)\t/) {
    $form = $1;
    $lexcat = $2;
    $lexsynt = $3;
    $lexlemma = $4;
    $lexms = $5;
  } else {
    next;
  }
  next if $form eq "_error";
  next if $form eq "_uw";
  $cat24 = "";
  $cat13 = "";
  $synt = "";
  $ms = "";
  $form =~ s/__[a-z]+$//;
  $form =~ s/^_(?=[^A-Z])//;
  $form =~ s/_$//;
  $form =~ s/ /_/g;
  $lexlemma =~ s/ /_/g;
  next if ($form =~ /^[-+]$/ && $lexcat =~ /^adv/);
  if ($use_ms == 2) {
    if ($lexms =~ /([mf]?)/) {$ms .= $1}
    if ($lexms =~ /([ps]?)/) {$ms .= $1}
  } elsif ($use_ms == 1) {
    $ms = $lexms;
  }
  if ($use_synt) {
    if (($use_synt == 1 || $lexcat eq "v") && $lexsynt =~ s/^.*?"((?:\\"|[^"])+)"/\1/) {
      if ($lexsynt =~ /[\(:\|]de-/) {
	$synt .= "__de";
      }
    }
  }
  next if ($lexlemma =~ / / && $form !~ / /);
  if ($lang eq "fr") {
    if ($lexcat eq "adj" || $lexcat eq "suffAdj") {$cat24 = "ADJ"; $cat13 = $cat24;}
    elsif ($lexcat =~ /^adv[mp]?$/ || $lexcat eq "advneg" || $lexcat eq "clneg" || $lexcat eq "que_restr") {$cat24 = "ADV"; $cat13 = $cat24;}
    elsif ($lexcat eq "adjPref" || $lexcat eq "advPref") {$cat24 = "PREF"; $cat13 = $cat24;}
    elsif ($lexcat eq "caimp" || $lexcat eq "ilimp" || $lexcat eq "cln") {$cat24 = "CLS"; $cat13 = $cat24;}
    elsif ($lexcat eq "clar" || $lexcat eq "cldr" || $lexcat eq "clr") {$cat24 = "CLR"; $cat13 = $cat24;}
    elsif ($lexcat eq "cla" || $lexcat eq "cld" || $lexcat eq "cll" || $lexcat eq "clg") {$cat24 = "CLO"; $cat13 = $cat24;}
    elsif ($lexcat eq "cf" || $lexcat eq "cfi") {if ($form =~ /[ _]/) {next} $cat24 = "NC"; $cat13 = $cat24;}
    elsif ($lexcat eq "ce" || $lexcat eq "pro") {$cat24 = "PRO"; $cat13 = $cat24;}
    elsif ($lexcat eq "coo") {$cat24 = "CC"; $cat13 = $cat24;}
    elsif ($lexcat eq "csu" || $lexcat eq "que") {$cat24 = "CS"; $cat13 = $cat24;}
    elsif ($lexcat eq "det") {
      if ($form =~ /^quell?e?s?$/) {
	$cat24 = "DETWH";
	$ms = "" unless ($use_ms);
	print CAT24 "$form\t$cat24$ms\t$lexlemma\n";
	if ($all) {
	  print CAT24MS "$form\t$cat24$ms\t$lexlemma\n";
	  print CAT13 "$form\tDET\t$lexlemma\n";
	  print CATO "$form\tGRAM\t$lexlemma\n";
	  print CATG "$form\tDET\t$lexlemma\n";
	}
      }
      $cat24 = "DET"; $cat13 = $cat24;
    }
    elsif ($lexcat eq "epsilon" || $lexcat eq "meta" || $lexcat eq "sbound") {next;}
    elsif ($lexcat eq "etr") {$cat24 = "ETR"; $cat13 = $cat24;}
    elsif ($lexcat eq "nc") {$cat24 = "NC"; $cat13 = $cat24;}
    elsif ($lexcat eq "np") {$cat24 = "NPP"; $cat13 = $cat24;}
    elsif ($lexcat =~ /^parent[fo]$/ || $lexcat =~ /^ponct[sw]$/) {$cat24 = "PONCT"; $cat13 = $cat24;}
    elsif ($lexcat eq "prel") {$cat24 = "PROREL"; $cat13 = "PRO";}
    elsif ($lexcat eq "pri") {$cat24 = "PROWH"; $cat13 = "PRO";}
    elsif ($lexcat eq "prep") {$cat24 = "P"; $cat13 = $cat24;}
    elsif ($lexcat eq "pres") {$cat24 = "I"; $cat13 = $cat24;}
    elsif ($lexcat =~ /^aux(Avoir|Etre)$/ || $lexcat eq "v") {
      $cat13 = "V";
      if ($lexms =~ /^Y/) {
	$cat24 = "VIMP";
      } elsif ($lexms eq "W") {
	$cat24 = "VINF";
      } elsif ($lexms =~ /^K/) {
	$cat24 = "VPP";
      } elsif ($lexms eq "G") {
	$cat24 = "VPR";
      } elsif ($lexms =~ /^[ST]/) {
	$cat24 = "VS";
      } else {
	if ($lexms =~ /[ST]/) {
	  $cat24 = "VS";
	  if ($all) {
	    print CAT24MS "$form\t".$cat24."$ms\t$lexlemma\n";
	  }
	  $ms = "" unless ($use_ms);
	  print CAT24 "$form\t$cat24$ms\t$lexlemma\n";
	}
	$cat24 = "V";
      }
    }
    else {
      die "Unknown lex cat '$lexcat' found for form '$form'\n";
    }
  } elsif ($lang eq "en") {
    if ($lexcat =~ /^([ACDINPRSV]|parent[of]|ponct[sw])$/) {$cat24 = $lexcat; $cat13 = $lexcat;}
    elsif ($lexcat eq "epsilon" || $lexcat eq "meta" || $lexcat eq "sbound") {next;}
  } elsif ($lang eq "pl") {
    if ($lexcat =~ /^(N)$/) {
      $cat24 = "Noun"; $cat13 = "Noun";
    } elsif ($lexcat =~ /^(A)$/) {
      $cat24 = "Adj"; $cat13 = "Adj";
    } elsif ($lexcat =~ /^(adv)$/) {
      $cat24 = "Adv"; $cat13 = "Adv";
    } elsif ($lexcat =~ /^(D)$/) {
      $cat24 = "Prep"; $cat13 = "Prep";
    } elsif ($lexcat =~ /^(I)$/) {
      $cat24 = "Adv"; $cat13 = "Adv";
    } elsif ($lexcat =~ /^(C)$/) {
      $cat24 = "Comp"; $cat13 = "Comp";
    } elsif ($lexcat =~ /^(ponct[sw]?|parent[of])$/) {
      $cat24 = "Interp"; $cat13 = "Interp";
    } elsif ($lexcat =~ /^(siebie)$/) {
      $cat24 = "Siebie"; $cat13 = "Siebie";
    } elsif ($lexcat =~ /^(W)$/) {
      $cat24 = "Winien"; $cat13 = "Winien";
    } elsif ($lexcat =~ /^(pred)$/) {
      $cat24 = "Pred"; $cat13 = "Pred";
    } elsif ($lexcat =~ /^(P)$/) {
      if ($lexms =~ /:(pri|sec):/) {
	$cat24 = "Pron12"; $cat13 = "Pron12";
      } elsif ($lexms =~ /:(ter):/) {
	$cat24 = "Pron3"; $cat13 = "Pron3";
      } else {
	$cat24 = "Pron"; $cat13 = "Pron";
      }
    } elsif ($lexcat =~ /^(V)$/) {
      if ($lexsynt =~ /cat=inf/) {
	$cat24 = "Inf"; $cat13 = "Inf";
      } elsif ($lexsynt =~ /cat=ppas/) {
	$cat24 = "Ppas"; $cat13 = "Ppas";
      } elsif ($lexsynt =~ /cat=pact/) {
	$cat24 = "Pact"; $cat13 = "Pact";
      } elsif ($lexsynt =~ /cat=pant/) {
	$cat24 = "Pant"; $cat13 = "Pant";
      } elsif ($lexsynt =~ /cat=pcon/) {
	$cat24 = "Pcon"; $cat13 = "Pcon";
      } else {
	$cat24 = "Verbfin"; $cat13 = "Verbfin";
      }
    } elsif ($lexcat eq "epsilon" || $lexcat eq "meta" || $lexcat eq "sbound") {
      next;
    } else {
      $cat24 = $lexcat; $cat13 = $lexcat;
    }
  } elsif ($lang eq "de") {
    if ($lexcat =~ /^ADJ$/) {
      if ($lexms =~ /noagr/) {$cat24 = "ADJD"; $cat13 = "ADJD";}
      else {$cat24 = "ADJA"; $cat13 = "ADJA";}
    } elsif ($lexcat =~ /^(AUX|MOD|V)$/) {
      $cat24 = "V"; $cat13 = "V";
      if ($lexcat eq "V") {
	$cat24 .= "V"; $cat13 .= "V";
      } elsif ($lexcat eq "AUX") {
	$cat24 .= "A"; $cat13 .= "A";
      } elsif ($lexcat eq "MOD") {
	$cat24 .= "M"; $cat13 .= "M";
      }
      if ($lexms =~ /imp/) {$cat24 .= "IMP"; $cat13 .= "IMP";}
      elsif ($lexms =~ /infzu/) {$cat24 .= "IZU"; $cat13 .= "IZU";}
      elsif ($lexms =~ /inf/) {$cat24 .= "INF"; $cat13 .= "INF";}
      elsif ($lexms =~ /ppart/) {$cat24 .= "PP"; $cat13 .= "PP";}
      else {$cat24 .= "FIN"; $cat13 .= "FIN";}
    } elsif ($lexcat eq "N") {$cat24 = "NN"; $cat13 = "NN";}
    elsif ($lexcat =~ s/^(.*?)\+//) {
      print CAT24 "$form\t$1\t$lexlemma\n";
      print CAT13 "$form\t$1\t$lexlemma\n";
      $cat24 = $lexcat; $cat13 = $lexcat;
    } elsif ($lexcat eq "epsilon" || $lexcat eq "meta" || $lexcat eq "sbound") {next;}
    else  {$cat24 = $lexcat; $cat13 = $lexcat;}
  } else {
    $cat24 = $lexcat;
  }
  if ($use_synt) {
    $cat24 .= $synt;
    $cat13 .= $synt;
  }
  $ms = "" unless ($use_ms);
  print CAT24 "$form\t$cat24$ms\t$lexlemma\n";
  if ($all) {
    print CAT24MS "$form\t$cat24$lexms\t$lexlemma\n";
    print CAT13 "$form\t$cat13\t$lexlemma\n";
    if ($cat13 =~ /^(V|ADV|ADJ|NC|NPP)$/) {
      print CATO "$form\t$cat13\t$lexlemma\n";
      print CATG "$form\tGRAM\t$lexlemma\n";
    } else {
      print CATO "$form\tLEX\t$lexlemma\n";
      print CATG "$form\t$cat13\t$lexlemma\n";
    }
  }
}
if ($lang eq "en") {
  print CAT24 "<<\t-LRB-\t<<\n";
  print CAT24 ">>\t-RRB-\t>>\n";
  print CAT24 "-LRB-\t-LRB-\t-LRB-\n";
  print CAT24 "-RRB-\t-RRB-\t-RRB-\n";
  print CAT24 "--\t:\t--\n";
  print CAT24 "-\t,\t-\n";
  print CAT24 "/\t,\t/\n";
  print CAT24 "--\t,\t--\n";
  print CAT24 "---\t,\t---\n";
  print CAT24 "'\t''\t'\n";
  print CAT24 "\"\t''\t\"\n";
  print CAT24 "''\t''\t''\n";
  print CAT24 "'\t\`\`\t'\n";
  print CAT24 "\"\t\`\`\t\"\n";
  print CAT24 "\`\`\t\`\`\t\`\`\n";
  print CAT24 "\%\tNN\t\%\n";
  print CAT24 "\$\t\$\t\$\n";
  print CAT24 "\@\tIN\t\@\n";
  print CAT24 "Ok\tI\tOk";
  print CAT24 "Oh\tI\tOh";
  print CAT24 "yes\tI\tyes";
  print CAT24 "yeah\tI\tyeah";
  print CAT24 "oh\tI\toh";
  print CAT24 "no\tI\tno";
  print CAT24 "well\tI\twell";
  print CAT24 "hello\tI\thello";
  print CAT24 "OK\TI\TOK";
  print CAT24 "hey\tI\they";
  print CAT24 "hi\tI\thi";
  print CAT24 "please\tI\tplease";
  print CAT24 "<3\tI\tVBP";
  print CAT24 "meh\tI\tJJ";
  print CAT24 "RIB\tI\tUH";
}

unless ($all) {
  `sort -u "$output_file" > lex2ftb4cats_temporary_file`;
  open TMP, "<lex2ftb4cats_temporary_file" || die "Could not open temporary file lex2ftb4cats_temporary_file: $!";
  binmode TMP, ":utf8";
  unless ($stdout) {
    open OUT, ">$output_file";
    binmode OUT, ":utf8";
  }
  while (<TMP>) {
    chomp;
    /^(.*?)\t(.*?)((?:__.*?)?)\t(.*)$/ || die $_;
    $f = $1;
    $cat = $2;
    $synt = $3;
    $l = $4;
    if (!defined($hash{$f}{$cat}{$l}) || (length($synt) > length($hash{$f}{$cat}{$l}))) {
      $hash{$f}{$cat}{$l} = $synt;
      if ($lang eq "fr" && $f =~ s/(?<=.)-(?=.)/_-_/g) {
	$hash{$f}{$cat}{$l} = $synt;
      }
      if ($lang eq "fr" && $f =~ s/(?<=.)'(?=.)/'_/g) {
	$hash{$f}{$cat}{$l} = $synt;
      }
    }
  }
  for $f (sort keys %hash) {
    for $cat (sort keys %{$hash{$f}}) {
      for $l (sort keys %{$hash{$f}{$cat}}) {
	if ($stdout) {
	  print STDOUT $f."\t".$cat.$hash{$f}{$cat}{$l}."\t".$l."\n";
	} else {
	  print OUT $f."\t".$cat.$hash{$f}{$cat}{$l}."\t".$l."\n";
	}
      }
    }
  }
}
