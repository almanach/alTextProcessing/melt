# -*- coding:utf8 -*-
import re

WD_TAG_RE = re.compile(r'^(.+)/([^\/]+)$')
CAPONLYLINE_RE = re.compile(r'^([^a-z]+)$')
number = re.compile("\d")
hyphen = re.compile("\-")
equals = re.compile("=")
upper = re.compile("[A-Z]")
allcaps = re.compile("^[A-Z]+$")



def suivants_in_dag(tokens,fin):
    suivants = []
    for tok in tokens:#[i+1:len(tokens)]:
        if tok.position[0] == fin :
           suivants.append(tok)
    return set(suivants)


def precedents_in_dag(tokens,debut):
    suivants = []
    for tok in tokens:
        if tok.position[1] == debut :
           suivants.append(tok)
    return set(suivants)



class DagInstance:

    def __init__(self, index, tokens, label=None, lex_dict={}, tag_dict={},
                 feat_selection={}, cache={}):
                 
        #suppose que les tokens sont renseignés en token.position et token.index
        self.label = label
        self.fv = []
        self.feat_selection = feat_selection
        # token 
        self.tokens = tokens
        self.token = tokens[index]
        self.index = index
        self.word = self.token.string
        # lexicons
        self.lex_dict = lex_dict
        self.tag_dict = tag_dict  
        self.cache = cache ## TODO
        # contexts
        win = feat_selection.get('win',2) # TODO: different wins for different types of contexts
        self.context_window = win
        self.set_contexts( tokens, index, win )
        return



    def set_contexts(self, toks, idx, win):
        rconx = [[t] for t in suivants_in_dag(toks,toks[idx].position[1])]
        lconx = [[t] for t in precedents_in_dag(toks,toks[idx].position[0])]
        print "set_ctxt", idx, [x.position for x in toks], rconx, lconx
        for i in range(win-1):
            for j,ctx in enumerate(rconx):
                suivs = suivants_in_dag(toks, ctx[-1].position[1])
                if len(suivs) > 0:
                    rconx[j] = [ ctx + [suiv] for ctx in rconx[j]]
            for j,ctx in enumerate(lconx):
                precs = precedents_in_dag(toks, ctx[0].position[0])
                if len(precs) > 0 :
                    lconx[j] = [ [prec] + ctx for ctx in lconx[j]]
            #for ctx in rconx :
            #    rconx = [ ctx + [suiv] for suiv in suivants_in_dag(toks,ctx[-1].position[1]) ]
            #for ctx in lconx :
            #    lconx = [ [prec] + ctx for prec in precedents_in_dag(toks,ctx[0].position[0]) ]
        rconx = set(map(tuple,rconx)) if len(rconx)>0 else set([()])
        lconx = set(map(tuple,lconx)) if len(lconx)>0 else set([()])
        self.left_labels = []
        self.left_wds = []
        self.lex_left_tags = []
        self.train_left_tags = []
        for ctx in lconx :
            wds = [tok.string for tok in ctx]
            if len(wds) < win:
                wds.insert(0,"<s>")
            self.left_wds.append(wds)
            self.left_labels.append([tok.label for tok in ctx])
            if self.lex_dict:
                self.lex_left_tags.extend(["|".join(self.lex_dict.get(tok.string,{"unk":1}).keys()) for tok in ctx if tok is not None])
            if self.tag_dict:
                self.train_left_tags.extend(["|".join(self.tag_dict.get(tok.string,{"unk":1}).keys()) for tok in ctx if tok is not None])
                
        self.right_wds = []
        self.lex_right_tags = []
        self.train_right_tags = []
        for ctx in rconx:
            wds = [tok.string for tok in ctx]
            if len(wds) < win :
                wds.append("</s>")
            self.right_wds.append(wds)
            if self.lex_dict:
                self.lex_right_tags.extend(["|".join(self.lex_dict.get(tok.string,{"unk":1}).keys()) for tok in ctx if tok is not None])
            if self.tag_dict:
                self.train_right_tags.extend(["|".join(self.tag_dict.get(tok.string,{"unk":1}).keys()) for tok in ctx if tok is not None])
        
        return

    def add(self,name,key,value=-1):
        if value == -1:
            f = u'%s=%s' %(name,key)
        else:
            f = u'%s=%s=%s' %(name,key,value)
        self.fv.append( f )
        return f


    def add_cached_feats(self,features):
        self.fv.extend(features)
        return


    def __str__(self):                                                     
        return u'%s\t%s' %(self.label,u" ".join(self.fv))
    def weighted_str(self,w):
        return u'%s $$$WEIGHT %f\t%s' %(self.label,w,u" ".join(self.fv))

    def get_features(self):
        self.get_static_features()
        self.get_sequential_features()
        return
    

    def get_sequential_features(self):
        ''' features based on preceding tagging decisions '''
        for left_labels in self.left_labels :
            prev_labels = left_labels
            for n in range(1,self.context_window+1):
                if len(prev_labels) >= n:
                    # unigram for each position
                    if n == 1:
                        unigram = prev_labels[-n]
                    else:
                        unigram = prev_labels[-n:-n+1][0]
                    self.add('ptag-%s' %n, unigram)
                    if n > 1:
                        # ngrams where 1 < n < window 
                        ngram = prev_labels[:n]
                        self.add('ptagS-%s' %n, "#".join(ngram))
            lex_rhs_feats = self.feat_selection.get('lex_rhs',0)
            rtags = self.lex_right_tags
            print rtags
            if lex_rhs_feats:
                if (len(prev_labels) >= 1) and (len(rtags) >= 1):
                    self.add('lpred-rlex-surr', prev_labels[-1] + "#" + rtags[0]) 
        return


    def get_static_features(self):
        ''' features that can be computed independently from previous
        decisions'''
        self.get_word_features()
        self.get_conx_features()
        if self.lex_dict:
            self.add_lexicon_features()
        # NOTE: features for tag dict currently turned off
        # if self.tag_dict: 
        #     self.add_tag_dict_features()
        return


    def get_word_features(self, ln=5):
        ''' features computed based on word form: word form itself,
        prefix/suffix-es of length ln: 0 < n < 5, and certain regex
        patterns'''
        pln = self.feat_selection.get('pln',4) # 5
        sln = self.feat_selection.get('sln',4) # 5
        word = self.word
        index = self.index
        dico = self.lex_dict
        lex_tags = dico.get(word,{})
        # selecting the suffix confidence class for the word
        val = 1;
        if len(lex_tags) == 1:
            val = lex_tags.values()[0]
        else:
            val = 1
            for v in lex_tags.values():
                if v == "0":
                    val = 0
                    break
        # word string-based features
        if word in self.cache:
            # if wd has been seen, use cache
            self.add_cached_features(self.cache[word])
        else:
            # word string
            self.add('wd',word)
            # suffix/prefix
            wd_ln = len(word)
            if pln > 0:
                for i in range(1,pln+1):
                    if wd_ln >= i:
                        self.add('pref%i' %i, word[:i])
            if sln > 0:
                for i in range(1, sln+1):
                    if wd_ln >= i:
                        self.add('suff%i' %i, word[-i:], val)
        # regex-based features
        self.add( 'nb', number.search(word) != None )
        self.add( 'hyph', hyphen.search(word) != None )
#        self.add( 'eq', equals.search(word) != None )
        uc = upper.search(word)
        self.add( 'uc', uc != None)
        self.add( 'niuc', uc != None and index > 0)
        self.add( 'auc', allcaps.match(word) != None)
        return



    def get_conx_features(self):
        ''' ngrams word forms in left and right contexts '''
        win = self.context_window
        rpln = self.feat_selection.get('rpln',1)
        rsln = self.feat_selection.get('rsln',1)
        print "ctxt feats", self.left_wds, self.right_wds
        for lwds in self.left_wds :
            # left/right contexts: ONLY UNIGRAMS FOR NOW
            for n in range(1,win+1):
                # LHS
                if len(lwds) >= n:
                    # unigram
                    if n == 1:
                        left_unigram = lwds[-n] 
                    else:
                        left_unigram = lwds[-n:-n+1][0]
                    self.add('wd-%s' %n, left_unigram)
        
        for rwds in self.right_wds :
            # left/right contexts: ONLY UNIGRAMS FOR NOW
            for n in range(1,win+1):
                if len(rwds) >= n:
                    # unigram
                    right_unigram = rwds[n-1:n][0]
                    self.add('wd+%s' %n, right_unigram)
                wd_ln = len(right_unigram)
                if rpln > 0:
                    for i in range(1,rpln+1):
                        if wd_ln >= i:
                            self.add('pref+1-%i' %i, right_unigram[:i])
                if rsln > 0:
                    for i in range(1,rsln+1):
                        if wd_ln >= i:
                            self.add('suff+1-%i' %i, right_unigram[-i:])

        return

    def _add_lex_features(self, dico, ltags, rtags, feat_suffix): # for lex name
        lex_wd_feats = self.feat_selection.get('lex_wd',0)
        lex_lhs_feats = self.feat_selection.get('lex_lhs',0)
        lex_rhs_feats = self.feat_selection.get('lex_rhs',0)
        if lex_wd_feats:
            # ------------------------------------------------------------
            # current word
            # ------------------------------------------------------------
            word = self.word
            lex_tags = dico.get(word,{})
            if not lex_tags and self.index == 0:
                # try lc'ed version for sent initial words
                lex_tags = dico.get(word.lower(),{})
            if len(lex_tags) == 0:
                self.add('%s' %feat_suffix, "unk")
            elif len(lex_tags) == 1:
                # unique tag
                t = lex_tags.keys()[0]
                self.add('%s-u' %feat_suffix,t,lex_tags[t])
            else:
                # disjunctive tag
                self.add('%s-disj' %feat_suffix,"|".join(lex_tags))
                # individual tags in disjunction
                for t in lex_tags:
                    self.add('%s-in' %feat_suffix,t)
                    f = u'%s=%s:%s' %(feat_suffix,t,lex_tags[t])

        # left and right contexts
        win = self.context_window
        for n in range(1,win+1):
            # ------------------------------------------------------------
            # LHS
            # ------------------------------------------------------------
            if lex_lhs_feats:
                if len(ltags) >= n:
                    # unigram
                    if n == 1:
                        left_unigram = ltags[-n]
                    else:
                        left_unigram = ltags[-n:-n+1][0]
                    self.add('%s-%s' %(feat_suffix,n), left_unigram)
                    # ngram
                    if n > 1:
                        left_ngram = ltags[-n:]
                        self.add('%sS-%s' %(feat_suffix,n), "#".join(left_ngram))
            # ------------------------------------------------------------
            # RHS
            # ------------------------------------------------------------
            if lex_rhs_feats:
                if len(rtags) >= n:
                    # unigram
                    right_unigram = rtags[n-1:n][0]
                    self.add('%s+%s' %(feat_suffix,n), right_unigram)
                    # ngram
                    if n > 1:
                        right_ngram = rtags[:n]
                        self.add('%sS+%s' %(feat_suffix,n), "#".join(right_ngram))
                        
        # surronding contexts
        #TODO: à refaire !
        if lex_lhs_feats and lex_rhs_feats:
            if win % 2 == 0:
                win /= 2
                for n in range(1,win+1):
                    surr_ngram = ltags[-n:] + rtags[:n]
                    if len(surr_ngram) == 2*n:
                        self.add('%s-surr-%s' %(feat_suffix,n), "#".join(surr_ngram))        
        return


    def add_lexicon_features(self):
        lex = self.lex_dict
        #for l_tags in self.lex_left_tags :
        l_tags = self.lex_left_tags
        self._add_lex_features( lex, l_tags, [], feat_suffix='lex')
        #for r_tags in self.lex_right_tags :
        r_tags = self.lex_right_tags
        self._add_lex_features( lex, l_tags, r_tags, feat_suffix='lex')
        
        #TODO : surrounding
        return


    def add_tag_dict_features(self):
        lex = self.tag_dict
        for l_tags in self.train_left_tags :
            #self._add_lex_features( lex, l_tags, [], feat_suffix='tdict' )
            for r_tags in self.train_right_tags :
                self._add_lex_features( lex, l_tags, r_tags, feat_suffix='tdict' )
        return

