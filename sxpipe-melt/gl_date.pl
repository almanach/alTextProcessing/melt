#!/usr/bin/env perl
# $Id: gl_date.pl 5501 2014-09-29 12:40:11Z sagot $
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

$lang="fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/-robust/)  {$robust=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

# variables
if ($lang =~ /^zh-/) {
  $year = qr/(?:年)/; 
  $month = qr/(?:月)/;
  $day = qr/(?:[日號号])/;
  $hour = qr/(?:[時时])/;
  $digit = qr/(?:[〇零一二三四五六七八九○0-9０-９])/;
  $order = qr/(?:[十百千萬億兆万亿])/;
  $yearid = qr/(?:$digit(?:$digit|[十百]){1,4}$year)/;
  $monthid = qr/(?:(?:[0-9０-９]{1,2}|[一二三四五六七八九]|十[一二]?)$month)/;
  $dayid = qr/(?:(?:[0-9０-９]{1,2}|(?:二?十)?[一二三四五六七八九]|三十一?)$day)/;
  $specialyearid = qr/(?:$digit{4}$year)/;
} elsif ($lang eq "th") {
  $era = qr/(?:พ\.ศ\.)/io;
  $digit = qr/(?:[๐๑๒๓๔๕๖๗๘๙0-9])/io;
  $month = qr/(?:มกราคม|กุมภาพันธ์|มีนาคม|เมษายน|พฤษภาคม|มิถุนายน|กรกฎาคม|สิงหาคม|กันยายน|ตุลาคม|พฤศจิกายน|ธันวาคม|เดือนหนึ่ง|เดือนสอง|เดือนสาม|เดือนสี่|เดือนห้า|เดือนหก|เดือนเจ็ด|เดือนแปด|เดือนเก้า|เดือนสิบ|เดือนสิบเอ็ด|เดือนสิบสอง)/io;
  $dayname = qr/(?:วันจันทร์|วันอังคาร|วันพุธ|วันพฤหัส|วันศุกร์|วันเสาร์|วันอาทิตย์|ม\.ค\.|ก\.พ\.|มี\.ค\.|เม\.ย\.|พ\.ค\.|มิ\.ย\.|ก\.ค\.|ส\.ค\.|ก\.ย\.|ต\.ค\.|พ\.ย\.|ธ\.ค\.)/io;
} elsif ($lang eq "sk") {
  $monthabr   = qr/(?:jan|feb|mar|apr|maj|jun|jul|aug|sept?|okt|nov|dec)/io;
  $month      = qr/(?:(?:janu[^\s]+r|febru[^\s]+r|apr[^\s]+l|mare?c|j[u]+[nl]|m[^\s]{1,2}j|august|septembe?r|oktobe?r|novembe?r|decembe?r)(?:a|i|om)?|$monthabr *\.?)/io;
  $weekday    = qr/(?:pondelok|utorok|streda|tvrtok|piatok|sobota|nedela)/io;
  $weekdayabr = qr/(?:pon?|uto?|str?|tv?|pia?|sob?|ned?)/io;
  $ordsuff1   = qr/(?:\.)/io;
  $ordsuff    = qr/(?:\.)/io;
  $centurySuff= qr/(?:si[e]cles?|s\.)/io;
  $yearEquiv  = qr/(?:XXXXX)/io;
  $yearPref   = qr/(?:zzzzzzzzzz)/io;
  $lsrefJC    = qr/(?:\s*(?:p(?:\s*\.\s*)?)?n(?:\s*\.\s*)?e(?:\s*\.)?)/io;
  $JCprep     = qr/(?:pred|po)/io;
  $coordArto  = qr/(?:do|[ai]|alebo)/io;
  $coordArtf  = qr/(?:do|[ai]|alebo)/io;
  $coordYear  = qr/(?:do|[ai]|[ai]\sv|alebo|alebo\sv)/io;
} elsif ($lang eq "pl") {
    $monthabr   = qr/(?:sty|lut|mar|kwi|maja?|cze|lip|sie|wrz|pa[z]|lis|gru)/io;
    $month      = qr/(?:(?:stycze?[n]|luty|mar(?:ze)c|kwie(?:cie|t)[n]|maj|czerw(?:ie)?c|lip(?:ie)?c|sierp(?:ie)?[n]|wrze(?:sie|)[n]|pa[z]dziernik|listopad|grud(?:zie)?[n])(?:i?[au])?|$monthabr\s*\.?)/io;
    $weekday    = qr/(?:poniedzia[l]ek|wtorek|[s]roda|czwartek|pi[a]tek|sobota|niedziela)/io;
    $weekdayabr = qr/(?:pon|wto|[s]ro|czw|pi[a]|sob|nie)/io;
    $ordsuff1   = qr/(?:\.)/io;
    $ordsuff    = qr/(?:\.)/io;
    $centurySuff= qr/(?:(?:wiek|stuleci)(?:u|i?em)?|w\.)/io;
    $yearEquiv  = qr/(?:br(?:\s*\.)?)/io;
    $yearPref   = qr/(?:zzzzzzzzzzz)/io;
    $lsrefJC    = qr/(?:\s*(?:p(?:\s*\.\s*)?)?n(?:\s*\.\s*)?e(?:\s*\.)?|\s*r\s*\.)/io;
    $JCprep     = qr/(?:pred|po)/io;
    $coordArto  = qr/(?:do|i|lub)/io;
    $coordArtf  = qr/(?:do|i|lub)/io;
    $coordYear  = qr/(?:do|i|i\sw|lub|lub\sw|albo|albo\sw)/io;
} elsif ($lang eq "en") {
    $monthabr   = qr/(?:jan|febr?|mar|apr|may|jun|jul|aug|sept?|oct|nov|dec)/io;
    $month      = qr/(?:january|february|march|april|may|june|july|august|septembe?re?|octobe?re?|novembe?re?|decembe?re?|$monthabr\s*\.?)/io;
    $weekday    = qr/(?:(?:mon|tues|wednes|thurs|fri|satur|sun)day)/io;
    $weekdayabr = qr/(?:mon|tue|wed|thu|fri|sat|sun)/io;
    $ordsuff1   = qr/(?:st)/io;
    $ordsuff    = qr/(?:nd|rd|i?th)/io;
    $centurySuff= qr/(?:centur(?:y|ies)|c\.)/io;
    $yearEquiv = qr/(?:xxxxxxxx)/io;
    $yearPref   = qr/(?:end)/io;
    $lsrefJC    = qr/(?:B(?:\. ?)?C(?:\. ?)?|A(?:\. ?)?D(?:\. ?)?)/io;
    $JCprep     = qr/(?:before|after)/io;
    $coordArto  = qr/(?:to the|to|and the|and|or|or the)/io;
    $coordArtf  = qr/(?:to|and|or)/io;
    $coordYear  = qr/(?:to|and|or|and in|or in)/io;
} elsif ($lang eq "de") {
    $monthabr   = qr/(?:j[aä]n|febr?|mär|apr|mai|juni?|juli?|aug|sept?|okt|nov|dez)/io;
    $month      = qr/(?:januar|jänner|februar|märz|april|mai|jun|juli|august|september|october|november|dezember|$monthabr\s*\.?)/io;
    $weekday    = qr/(?:(?:mon|tues|wednes|thurs|fri|satur|sun)day)/io;
    $weekdayabr = qr/(?:mon|tue|wed|thu|fri|sat|sun)/io;
    $ordsuff1   = qr/(?:st)/io;
    $ordsuff    = qr/(?:nd|rd|i?th)/io;
    $centurySuff= qr/(?:centur(?:y|ies)|c\.)/io;
    $yearEquiv = qr/(?:xxxxxxxx)/io;
    $yearPref   = qr/(?:Ende)/io;
    $lsrefJC    = qr/(?:B(?:\. ?)?C(?:\. ?)?|A(?:\. ?)?D(?:\. ?)?)/io;
    $JCprep     = qr/(?:[vn]\.?|vor|nach)/io;
    $coordArto  = qr/(?:to the|to|and the|and|or|or the)/io;
    $coordArtf  = qr/(?:to|and|or)/io;
    $coordYear  = qr/(?:to|and|or|and in|or in)/io;
} else {
    $monthabr   = qr/(?:janv?|f[eé]vr?|mar|avr|mai|jun|jui?l|ao[uû]|sept?|oct|nov|d[eé]c)/io;
    $month      = qr/(?:janvier|f[eé]vrier|mars|avril|mai|juin|juillet|ao[uû]t|septembre|octobre|novembre|d[eé]cembre|courants?|$monthabr\s*\.?)/io;
    $weekday    = qr/(?:(?:lun|mar|mercre|jeu|vendre|same)di|dimanche)/io;
    $weekdayabr = qr/(?:lun|mar|mer|jeu|ven|sam|dim)/io;
    $ordsuff1   = qr/(?:er)/io;
    $ordsuff    = qr/(?:[eè](?:me))/io;
    $centurySuff= qr/(?:si[eè]cles?|s\.)/io;
    $yearPref   = qr/(?:début|fin)/io;
    $yearEquiv  = qr/(?:derniers?|courants?|prochains?)/io;
    $lsrefJC    = qr/(?:\s+(?:de|av\.?|avant)\s+notre\s+[eè]re|\s+de\s+l\'\s*[Hh][ée]gire)/io;
    $JCprep     = qr/(?:avant|av\.?|apr[èe]s|apr?\.?)/io;
    $coordArto  = qr/(?:au|et\sle|et|ou|ou\sle)/io;
    $coordArtf  = qr/(?:à|et|ou)/io;
    $coordYear  = qr/(?:à|et|ou|et\sen|ou\sen)/io;
}

if ($lang !~ /^(zh|tw|ja)/) {
  $ENweekdayabr = qr/(?:mon|tue|wed|thu|fri|sat|sun)/io;
  $ENmonthabr   = qr/(?:jan|febr?|mar|apr|may|jun|jul|aug|sept?|oct|nov|dec)/io;

  unless (defined $robust) {
    $dayN      = qr/(?:[12]\d|3[01]|0?\d)/o;
  } else {
      $dayN      = qr/[0-9iIzZfG><\%\'\"\^jJ\N{U+00CF}\N{U+00EF}\N{U+00CE}\N{U+00EE}\N{U+00FB}]{1,2}/;  # for OCR errors
  }

  $day       = qr/(?:(?:$weekday\s+|$weekdayabr(?:\s*\.)?\s+)?(?:1$ordsuff1|$dayN(?:\s*\.)?))/io;
  $dayR      = qr/(?:$day\s*\-\s*$day)/o;
  $monthN      = qr/(?:1[0-2]|0?[1-9])/o;
  $monthRom    = qr/(?:III|I[VIX]?|VIII|VII?|XII?|X)/o;
  $centuryRom  = qr/(?:I$ordsuff1|(?:III?|I?V|VII?|VIII|I?X|XII?|XIII|XI?V|XVII?|XVIII|XI?X|XXII?|XXIII|XXI?V|XXVII?|XXVIII|XXI?X)$ordsuff?)/io;
  $centuryAr  = qr/(?:1$ordsuff1|(?:[2-9]|1[0-9]|2[0-1])$ordsuff?)/io;
  $century     = qr/(?:(?:$centuryRom|$centuryAr)\s+$centurySuff)/io;

  unless (defined $robust) {
     $yearN         = qr/(?:[1-9]\d*|0|0\d|l9[0-9][0-9])/o; # a little bit of tolerance
  } else { 
      $yearN         = qr/[1-9IiZzlS][0-9iIzZsSlOjJ\'\"<>\N{U+00CF}\N{U+00EF}\N{U+00CE}\N{U+00EE}\N{U+00FB}]{1,3}/o; # a little more tolerance, for OCR errors
  }

  $JC         = qr/(?:J(?:[eé]sus|\s*\.)?\s*[\s\-]?\s*C(?:hr(?:ist(?:us)?|\s*\.)?|\s*\.)?)/io;
  $refJC      = qr/(?:\s+$JCprep\s*$JC|\s*\(\s*$JCprep\s*$JC\s*\)|\s+[Aa]\s*\.\s*[Dd]\s*\.|\s+[Bb]\s*\.\s*[Cc]\s*\.|AD|BC|$lsrefJC)/io;
  $yearJC    = qr/(?:$yearN$refJC)/o;
  $year      = qr/(?:$yearJC|(?:[\-\~]\s*)?$yearN)/o;
  $yearR     = qr/(?:[12l][0-9l]{3}\s*[\-\/\­]\s*[12l][0-9l]{3})/io; # a little bit of tolerance
  $year2     = qr/(?:(?:\s+)?$year|$yearEquiv)/io;
  #$year2     = qr/(?:(?:,\s+)?$year|$yearEquiv)/io;
  
  if ($no_sw) {
    $lctxt=qr/(?<=[\s])/o;
  } else {
    $lctxt=qr/(?<=[^0-9\{\-a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ])/o;
  }
  $rctxt=qr/(?=[^0-9\}\-a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ])/o;
}

while (<>) {
    # formattage
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    s/^\s*/ /o;
    s/\s*$/ /o;

    if ($lang =~ /^(zh|tw)-/) {
      if ($lang eq "zh-pku") {
	s/($yearid|$specialyearid)($monthid)($dayid)/ {$1 $2 $3} _DATE /go;
	s/(?<!{)($yearid|$specialyearid)($monthid)/ {$1 $2} _DATE /go;
	s/($monthid)($dayid)(?!})/ {$1 $2} _DATE /go;
	s/(?<!{)($specialyearid)/ {$1 $2} _DATE /go;
      } else {
	s/((?:$yearid|$specialyearid)$monthid$dayid?|$monthid$dayid)/ {$1} _DATE /go;
      }
    } elsif ($lang eq "th") {
      s/((?:1[012]|[1-9])\/(?:[1-9]|[12][0-9]|3[012])\/[0-9]{2,})/ {$1} _DATE /go;
#      s/($era\s*$digit+(?:\s*-\s*$digit+)?)/ {$1} _DATE /go;      
      s/((?:(?:$dayname\s*)?(?:[1-9]|[12][0-9]|3[012](?:\s*-\s*[1-9]|[12][0-9]|3[012])?)\s*)?($month\s*|$era\s*)+$digit+(?:\s*-\s*$digit+)?)/ {$1} _DATE /g;
    } elsif ($lang !~ /^(zh|tw|ja)$/) {
      # reconnaissance - dates complètes

      if ($no_sw) {
	s/$lctxt($dayR\s+$month(?:\s+$year2)?)(\s+-\s+)($dayR\s+$month(?:\s+$year2)?)$rctxt/{$1}_DATE_arto$2\{$3}_DATE_arto/go;
	s/$lctxt($day\s+$month(?:\s*[\-\­]\s*$day\s+$month)?(?:\s+$year2)?)(\s+-\s+)($day\s+$month(?:\s*[\-\­]\s*$day\s+$month)?(?:\s+$year2)?)$rctxt/{$1}_DATE_arto$2\{$3}_DATE_arto/go;
	s/$lctxt($dayN\s*[\-\­]\s*$monthN\s*[\-\­]\s*$yearN)(\s+-\s+)($dayN\s*[\-\­]\s*$monthN\s*[\-\­]\s*$yearN)$rctxt/{$1}_DATE_arto$2\{$3}_DATE_arto/go;
	s/$lctxt($dayN\s*\/\s*$monthN(?:\s*[\-\­]\s*$day\s*\/\s*$monthN)?(?:\s*\/\s*$yearN)?)(\s+-\s+)($dayN\s*\/\s*$monthN(?:\s*[\-\­]\s*$day\s*\/\s*$monthN)?(?:\s*\/\s*$yearN)?)$rctxt/{$1}_DATE_arto$2\{$3}_DATE_arto/go;
      } else {
	s/$lctxt($dayR\s+$month(?:\s+$year2)?)\s*(-)\s*($dayR\s+$month(?:\s+$year2)?)$rctxt/{$1}_DATE_arto $2 \{$3}_DATE_arto/go;
	s/$lctxt($day\s+$month(?:\s*[\-\­]\s*$day\s+$month)?(?:\s+$year2)?)\s*(-)\s*($day\s+$month(?:\s*[\-\­]\s*$day\s+$month)?(?:\s+$year2)?)$rctxt/{$1}_DATE_arto $2 \{$3}_DATE_arto/go;
	s/$lctxt($dayN\s*[\-\­]\s*$monthN\s*[\-\­]\s*$yearN)\s*(-)\s*($dayN\s*[\-\­]\s*$monthN\s*[\-\­]\s*$yearN)$rctxt/{$1}_DATE_arto $2 \{$3}_DATE_arto/go;
	s/$lctxt($dayN\s*\/\s*$monthN(?:\s*[\-\­]\s*$day\s*\/\s*$monthN)?(?:\s*\/\s*$yearN)?)\s*(-)\s*($dayN\s*\/\s*$monthN(?:\s*[\-\­]\s*$day\s*\/\s*$monthN)?(?:\s*\/\s*$yearN)?)$rctxt/{$1}_DATE_arto $2 \{$3}_DATE_arto/go;
      }

      s/$lctxt($dayR\s+$month(?:\s+$year2)?)$rctxt/{$1}_DATE_arto/go;
      s/$lctxt($day\s+$month(?:\s*[\-\­]\s*$day\s+$month)?(?:\s+$year2)?)$rctxt/{$1}_DATE_arto/go;
      s/$lctxt($dayN\s*[\-\­]\s*$monthN\s*[\-\­]\s*$yearN)$rctxt/{$1}_DATE_arto/go;
      s/$lctxt($dayN\s*\/\s*$monthN(?:\s*[\-\­]\s*$day\s*\/\s*$monthN)?(?:\s*\/\s*$yearN)?)$rctxt/{$1}_DATE_arto/go;

      s/$lctxt($dayN\s*\.\s*(?:$monthRom|$monthN)?\s*\.\s*$yearN)$rctxt/{$1}_DATE_arto/go;
      s/$lctxt($dayN\s*[\-\­]\s*$day\s*\/\s*$monthN(?:\s*\/\s*$yearN)?)$rctxt/{$1}_DATE_arto/go;
      s/$lctxt($yearN\s*[\-\­]\s*$monthN\s*[\-\­]\s*$dayN(?:T\d{2}:\d{2}:\d{2}.\d{2,}\+\d{2}:\d{2})?)$rctxt/{$1}_DATE_artf/go;
      s/$lctxt((?:$ENweekdayabr,?\s*)?$ENmonthabr\s*$dayN,?\s*$year)$rctxt/{$1}_DATE_artf/go;
      s/$lctxt($century$refJC?)$rctxt/{$1}_DATE_arto/go;
      s/$lctxt($month\s+$year2\s*[\-\­]\s*$month\s+$year2)$rctxt/{$1}_DATE_year/go;
      s/$lctxt($month(?:\s*[\-\­]\s*$month)?\s+$year2)$rctxt/{$1}_DATE_year/go;
      s/$lctxt($ENmonthabr,?\s*$year)$rctxt/{$1}_DATE_artf/go;
      s/$lctxt($yearN\s+$JCprep\s*$JC)$rctxt/{$1}_DATE_year/go;
      s/$lctxt($yearR)$rctxt/{$1}_DATE_year/go;
    
      if ($lang eq "fr") { # traitement de cas comme "matériaux 1/2 ronds, 1/2 cassés en carrière", mais en conservant des dates pour "le 1/2" ou "du 1/2"
	s/(le|du) \{(1\/\d+|3\/4|2\/3)\}(_DATE_[a-z]+)/$1 \{$2}_PROTECT_$3/go;
	s/{(1\/\d+|3\/4|2\/3)}_DATE_[a-z]+/$1/go;
	s/}_PROTECT_DATE/}_DATE/go;
      }

      # reconnaissance - dates possibles dans un contexte qui fait penser que ce sont effectivement des dates
      if ($lang eq "sk") {
      } elsif ($lang eq "pl") {
	# "l" instead of "1" is tolerated in these pretty safe situations
	s/$lctxt((?:$yearR|$yearN)\s*r\s*\.)$rctxt/{$1}_DATE_year/gio;
	s/$lctxt(rok(?:u|iem)?|latach|lat(?:a|ami)?|końca|r\s*\.)(\s+)($yearR|$yearN)$rctxt/$1$2\{$3\}_DATE_year/gio;
	s/$lctxt(w|od|do)(\s+)($yearR|1[0-9]{3}|2[01][0-9]{2})$rctxt/$1$2\{$3}_DATE_year/gio;
	s/$lctxt($yearR|$yearN)(\s+)(roku?)$rctxt/{$1}_DATE_year$2$3/gio;
      } elsif ($lang eq "en") {
	s/$lctxt(mid\s*-?\s*${yearN}s?)$rctxt/{$1}_DATE_artf/go;
	s/$lctxt(the\s*${yearN}s)$rctxt/{$1}_DATE_artf/go;
      } elsif ($lang eq "fr") {
	s/$lctxt(mi\s*-?\s*$yearN)$rctxt/{$1}_DATE_artf/go;
	s/$lctxt(l\'an\s+$year)$rctxt/{$1}_DATE_artf/go;
	s/$lctxt($weekday(?:\s+(?:dernier|prochain))?)$rctxt/{$1}_DATE_artf/go;
	s/$lctxt(en|avant|après|depuis|dès|courant|vers)(\s+)($yearR|1[0-9]{3}|2[01][0-9]{2})(\s*(?:an(?:née)?s?|jours?|semaines?|heures?|minutes?|\S*minutes?|siècles?|millénaires?))/$1$2$3PROTECT__$4/gio;
	s/$lctxt(en|avant|après|depuis|dès|courant|vers)(\s+)($yearR|1[0-9]{3}|2[01][0-9]{2})$rctxt/$1$2\{$3}_DATE_year/gio;
	s/PROTECT__//go;
	s/$lctxt(courant\s+(?:1[0-9]{3}|2[01][0-9]{2}))$rctxt/{$1}_DATE_artf/gio;
	s/$lctxt(de\s+)(1[0-9]{3}|2[01][0-9]{2})(\s+à\s+)(1[7-9][0-9]{2}|2[01][0-9]{2})$rctxt/$1\{$2}_DATE_year$3\{$4}_DATE_year/gio;
	s/(saisons?|années?(?: (?:scolaires?|universitaires?|calendaires?))?) (\d{0,2}\d{2}-\d{2})$rctxt/$1 \{$2}_DATE_year/gio;
      }

      s/$lctxt($yearPref)\s*(${yearN})$rctxt/$1 {$2}_DATE_artf/go;

      # reconnaissance - dates incomplètes coordonnées à des dates complètes
      $fulldate=qr/(?:\{[^\}]*\}\s*_DATE)/o;
      s/$lctxt($day|$centuryRom|$centuryAr)(\s+$coordArto\s+$fulldate\_arto)$rctxt/{$1}_DATE_arto$2/gio;
      s/$lctxt($month|$yearN|[\-\­\~]\s*$yearN)(\s+$coordYear\s+$fulldate\_year)$rctxt/{$1}_DATE_year$2/gio;
      s/$lctxt($weekday)(\s+$coordArto\s+$fulldate\_arto)$rctxt/{$1}_DATE_arto$2/gio;
      s/$lctxt($weekday)(\s+$coordArtf\s+$fulldate\_artf)$rctxt/{$1}_DATE_artf$2/gio;
      s/($fulldate\_idef\s+$coordYear\s+)($month|$yearN|[\-\­\~]\s*$yearN)$rctxt/$1\{$2}_DATE_year/gio;

      if ($lang ne "fr") {
	s/_DATE(?:_arto|_artf)/_DATE/g;
      }

      if ($no_sw) {
	s/(_DATE_(?:arto|artf|year))([^\s])/$1 _REGLUE_$2/g;
      } else {
	s/(_DATE_(?:arto|artf|year))([^\s])/$1 $2/g;
      }
    }
    s/^ //o;
    s/ $//o;
    print "$_\n";
}

