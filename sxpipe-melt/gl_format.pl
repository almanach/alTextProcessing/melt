#!/usr/bin/env perl
# $Id: gl_format.pl 4820 2012-08-03 13:58:55Z sagot $
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

$lang="fr";

while (<>) {
    # formattage
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    s/^\s*/ /o;
    s/\s*$/ /o;

    # reconnaissance
    s/(\s)_UNDERSCORE([^\s_{}]+)_UNDERSCORE(\s)/$1\{_UNDERSCORE$2\_UNDERSCORE\} $2$3/o;
    s/(\s)\*([^ _\*{}]+)\*(\s)/$1\{*$2\*\} $2$3/o;

    # sortie
    s/^ //o;
    s/ $//o;
    print "$_\n";
}
