#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$|=1;

while (<>) {
  chomp;
  while (s/(<F id.*?>[^<]+<\/F>)(.*?)\1/\1\2/g) {}
  s/<F id.*?>//g;
  s/<\/F>//g;
  s/  +/ /g;
  s/{ /{/g;
  s/ }/}/g;
  print "$_\n";
}
