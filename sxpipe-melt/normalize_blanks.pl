#!/usr/bin/env perl
# $Id: normalize_blanks.pl 4820 2012-08-03 13:58:55Z sagot $
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

while (<>) {
    chomp;
    if (/ (_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";
	next;
    }
    s/  */ /g;
    s/{ /{/g;
    s/ }/}/g;
    s/\\/\\\\/g;
    s/%/\\%/g;
    print $_."\n";
}
