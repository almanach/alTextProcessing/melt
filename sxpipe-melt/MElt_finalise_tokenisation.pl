#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use strict;

my $tokeniser_mode = 0;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-tok$/) {$tokeniser_mode = 1}
}

while (<>) {
  chomp;
  s/{[^{}]+} _SPECWORD_//g;
  s/_ACC_([OF])/_ACC\1/g;
  s/(} *_[A-Za-z_]+[A-Za-z])_[^_]+_( |$)/\1\2/g;

  if ($tokeniser_mode) {
    s/{([^{}]*)} *_(?:SENT_BOUND) //g;
    s/(^| ){([^{}]*)} *_(?:SENT_BOUND)$//g;
    s/{([^{}]*)} *_(?:NUM)( |$)/"{".$1."} ".remove_blanks_except_between_digits($1).$2/ge;
    s/{([^{}]*)} *_(?:ROMNUM|URL|EMAIL|SMILEY|META[^ ]*)( |$)/"{".$1."} ".remove_blanks($1).$2/ge;
    s/{([^{}]*)} *_NPREF /$1/g;
  } else {
    s/{([0-9 ]+)([^}]*)} *_NUM( |$)/"{$1$2} ".remove_blanks($1).$3/ge;
  }

  s/{([^{}]*)} _XML {/{_<_\1_>_/g;
  s/} *([^ {}]+) *{([^{}]+)} _XML/_<_\2_>_} \1/g;


  if ($tokeniser_mode) {
    s/(^|[^\\])\\(.)/$1$2/g;
    s/_(UNDERSCORE|ACC[OF])/ʃəƙʀɛƭ ɱɜʂʂɐʥ$1/g;
    s/_/ /g;
    s/ʃəƙʀɛƭ ɱɜʂʂɐʥUNDERSCORE/_/g;
    s/ʃəƙʀɛƭ ɱɜʂʂɐʥ/_/g;
  }
  
  s/_ACC([OF])/_ACC_\1/g;

  print "$_\n";
}

sub remove_blanks {
  my $s = shift;
  $s =~ s/ //g;
  return $s;
}

sub remove_blanks_except_between_digits {
  my $s = shift;
  $s =~ s/(\d) (\d)/$1_UNDERSCORE$2/g;
  $s =~ s/ //g;
  return $s;
}
