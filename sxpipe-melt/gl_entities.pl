#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$|=1;

my $lang="fr";
my $no_nbsp = 0;

while (1) {
  $_=shift;
  if (/^$/) {last}
  elsif (/^-no_?nbsp$/) {$no_nbsp=1}
  elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;}
  elsif (/^-l(?:ang)?$/) {$lang=shift}
}

$a_star = qr/(?:[-'a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]*)/o;
$not_a = qr/(?:[^-'a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ])/o;
$ent = qr/(?:\&\#\d+;)/o;
$w_with_ent = qr/(?:$a_star$ent(?:$a_star|$ent)*)/o;

$jap_num_bullets = qr/(?:&#226;&#145;&#16[0-8];)/o;
#$jap_highdot = qr/(?:&#227;&#131;&#187;)/o;
$jap_bigbullet = qr/(?:&#226;&#151;&#143;)/o;
$jap_bullets = qr/(?:^$jap_bigbullet(?!$jap_bigbullet)|(?<!$jap_bigbullet)$jap_bigbullet$|(?<!$jap_bigbullet)$jap_bigbullet(?!$jap_bigbullet))/o;
$independent_entity = qr/(?:$jap_num_bullets|$jap_bullets)/o;

while (<>) {
    # formattage
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    s/ / /g if $no_nbsp;

#    s/^[^\S ]*/ /o;
#    s/[^\S ]*$/ /o;

    # reconnaissance
    # la regexp ci-dessous, quoique bonne, segfaulte sur un exemple en japonais.
    # On la remplace donc par les 4 regexp qui suivent, qui font le même travail par étapes.
    #s/($w_with_ent+(?:\s+$w_with_ent+)*)/{$1}_ETR/go;

    s/(?<=$not_a)($w_with_ent+)(?=$not_a)/\{\{$1\}\}/go;
    s/($independent_entity)/}}{_{\1}_} {{/go;
    s/\{\{\}\}//g;
    s/\}\} \{\{/ /g;
    s/\{_?\{/ {/g;
    s/\}_?\}/}_ETR /g;
    s/^\s*\}\}\s*//;
    s/\s*\{\{\s*$//;

#    s/_ETR([^ {]+)/_ETR _UNSPLIT_\1 /g;

    # sortie
    s/^ //o;
    s/ $//o;
    print "$_\n";
}
