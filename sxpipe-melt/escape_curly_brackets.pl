#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

while (<>) {
  chomp;
  s/\{/◁/g;
  s/\}/▷/g;
  print "$_\n";
}
