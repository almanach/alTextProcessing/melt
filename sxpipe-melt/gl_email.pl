#!/usr/bin/env perl
# $Id: gl_email.pl 5376 2014-02-23 11:56:56Z sagot $
# added -no-correction option gabor

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

# language independant

$| = 1;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-sc$/ || /^-spelling[-_]correction$/i) {$do_spelling_correction=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

$an          = qr/[0-9a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑａ-ｚＡ-Ｚ]/o;
while (<>) {
    # formattage
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    s/^\s*//o;
    s/\s$//o;
    # variables

    # correction
    if (!$no_sw && $lang !~ /^zh-/ && $do_spelling_correction) {
	s/\s+\@/\@/o;
	s/\@\s+/\@/o;
	s/(\@$an+),\s/$1./go;
	s/(\@$an+\.)\s/$1/go;
	s/(\@$an+\.$an+\.)\s([a-z]{2,})\b/$1$2/go;
    }

    # reconnaissance
    if ($no_sw) {
	s/(^| )((?:mailto:)?[\w\.．\%-]+\s*[＠\@]\s*(?:[\w-]+[\.．])+\s?$an{2,4})\b/$1\{$2}_EMAIL/go;
    } else {
	s/\b((?:mailto:)?[\w\.．\%-]+\s*[＠\@]\s*(?:[\wａ-ｚＡ-Ｚ-]+[\.．])+\s?$an{2,4})\b/{$1}_EMAIL/go;
    }
    s/(^|\s)(\&lt;\s*){([^}]*)}_EMAIL(\s*\&gt;)(\s|$)/$1\{$2$3$4\}_EMAIL$5/go;
    
    if ($no_sw) {
	s/(_EMAIL)([^\s])/$1 _REGLUE_$2/g;
	s/(^|\s)([^\s]+){([^{]*)}_EMAIL/$1\{$2$3\} $2 _REGLUE__EMAIL/g;
    } else {
	s/(_EMAIL)([^\s])/$1 $2/g;
    }

    # sortie
    print "$_\n";
}
