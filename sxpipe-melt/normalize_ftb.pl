#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$| = 1;

while (1) {
  $_=shift;
  if (/^$/) {last}
  elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1}
}


# if ($no_sw) {
#   while (<>) {
#     s/_//g;
#     print $_;
#   }
# } else {
  while (<>) {
    s/_-_/-/g;
    print $_;
  }
#}
