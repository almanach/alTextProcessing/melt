#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$n = 3;

while (1) {
  $_ = shift;
  if (/^-l$/) {$lang = shift;}
  elsif (/^-n$/) {$n = shift;}
  elsif (/^-t$/) {$use_tokens = 1}
  elsif (/^$/) {last}
  else {$cgram_file = $_}
}

my %cgram;
my $total;
my $l = 0;
while (<>) {
  $l++;
  print STDERR "$l\r";
  chomp;
  s/œ/oe/g;
  s/Œ/OE/g;
  if ($lang eq "sl") {
    s/[^àåâäéêèëîïöôùûüÿçøóúíáòìšžčđa-zA-ZÀÅÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌŠŽČĐ0-9,;:=\?\.\/\+\\\@\#\&\"\'\(\§\!\)\°_\$\^\%\`€\* \t-]/⚂/g;
  } else { # default = lang eq fr
    s/[^àåâäéêèëîïöôùûüÿçøóúíáòìa-zA-ZÀÅÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌ0-9,;:=\?\.\/\+\\\@\#\&\"\'\(\§\!\)\°_\$\^\%\`€\* \t-]/⚂/g;
  }
  s/^/ /;
  s/$/ /;
  s/\s/ /g;
  s/  +/ /g;
  if ($use_tokens) {
    @chars = split / /, $_;
  } else {
    @chars = split //, $_;
  }
  for $i (0..$#chars-$n) {
    $cg = join "", @chars[$i..$i+$n-1];
    $cgram{$cg}++;
    $total++;
  }
}

for (sort {$cgram{$b} <=> $cgram{$a}} keys %cgram) {
  print $_."\t".$cgram{$_}/($total+0.1)."\n";
}
print "UNKNOWNCGRAM\t".(0.1/($total+0.1))."\n";
