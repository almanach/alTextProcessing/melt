#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

while (<>) {
  chomp;
  $_ .= " ";
  while (s/^(.*? )(.*? .*?) /\2 /) {
    $trigrams{$1.$2}++;
  }
  $size = scalar keys %trigrams;
  print STDERR "$size\r" if ($size % 10000 == 0);
  last if $size >= 10000000;
}
for (sort {$trigrams{$b} <=> $trigrams{$a}} keys %trigrams) {
  print "$trigrams{$_}\t$_\n";
}
