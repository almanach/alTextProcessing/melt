#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

while (<>) {
  chomp;
  $_ = "$_ ";
  while (s/^(.*? )(.*?) /\2 /) {
    $bigrams{$1.$2}++;
  }
}
for (sort {$bigrams{$b} <=> $bigrams{$a}} keys %bigrams) {
  print "$bigrams{$_}\t$_\n";
}
