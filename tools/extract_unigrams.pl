#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$lang = shift || "fr";

for $file (</usr/local/share/alexina/$lang/*.lex>) {
  open FILE, "$file";
  binmode FILE, ":encoding(iso-8859-1)";
  while (<FILE>) {
    chomp;
    s/(^|[^\\])#.*//;
    next if /^\s*$/;
    s/^(.*?)\t.*$/\1/;
    s/__.*$//;
    $hash{$_} = 1;
  }
  close FILE;
}

while (<>) {
  chomp;
  $_ = "$_ ";
  while (s/^(.*?) //) {
    $unigrams{$1}++;
  }
}
for (sort {$unigrams{$b} <=> $unigrams{$a}} keys %unigrams) {
  print "$unigrams{$_}\t$_\t$_\n" unless defined($hash{$_});
}
