#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;



while (1) {
  $_ = shift;
  if (/^-l$/) {$lang = shift;}
  elsif (/^-no_r$/) {$no_retweets = 1;}
  elsif (/^-n$/) {$n = shift;}
  elsif (/^-ut$/) {$use_tokens = shift;}
  elsif (/^$/) {last}
  else {$cgram_file = $_}
}

die if $cgram_file eq "";
open CGRAMS, "<$cgram_file" || die $!;
binmode CGRAMS, ":utf8";
while (<CGRAMS>) {
  chomp;
  /^(.*?)\t(.*)$/ || die $_;
  $cgram{$1} = $2;
}

my %gram;

my $l = 0;
while (<>) {
  $l++;
#  print STDERR "$l\r";
  chomp;
  next if /^RT / && $no_retweets;
  next if (/^Posté le/ || /(^| )auteur : / || /^(thème|sous-thème) / || /^<\/?post/ || /-LT-post-GT/ || /-LT-slash-post-GT/); # jv
  next if (/(^| )Requête ?: / || /^\s*\d+\.\d+\s*$/ || /^\d+ \.$/ || /^# [^ ]+$/ || / \@[^ ]+$/); # twitter
  next if (/ · / || /LikeUnlike/); #fb
  next if (/Ligne_\d+-non_parsed/);
  s/https?:[^ ]*/⚂/g;
  s/www.[^ ]*/⚂/g;
  s/bit\.ly\/[^ ]*/⚂/g;
  s/[^ ]+\.(com|fr|sl)\/[^ ]*/⚂/g;
  s/ [\@\#][^ ]+/ ⚂/g;
  s/_A_ANONYMISER//g;
  s/ bidouille / /g;
  s/^bidouille( |$)//g;
  s/⚂//g;
  s/^\s*$//;
  $line = $_;
  s/œ/oe/g;
  s/Œ/OE/g;
  if ($lang eq "sl") {
    s/[^àåâäéêèëîïöôùûüÿçøóúíáòìšžčđa-zA-ZÀÅÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌŠŽČĐ0-9,;:=\?\.\/\+\\\@\#\&\"\'\(\§\!\)\°_\$\^\%\`€\* \t-]/⚂/g;
  } else { # default = lang eq fr
    s/[^àåâäéêèëîïöôùûüÿçøóúíáòìa-zA-ZÀÅÉÈÊËÂÄÔÖÛÜÇÓÚÍÁÒØÌ0-9,;:=\?\.\/\+\\\@\#\&\"\'\(\§\!\)\°_\$\^\%\`€\* \t-]/⚂/g;
  }
  s/\s/ /g;
  s/^/ /;
  s/$/ /;
  s/  +/ /g;
  if ($use_tokens) {
    @chars = split / /, $_;
  } else {
    @chars = split //, $_;
  }
  if ($#chars > $n) {
    for $i (0..$#chars-$n) {
      $cg = join "", @chars[$i..$i+$n-1];
      $gram{$cg}++;
      $total++;
    }
  }
}
$score = 0;
for (keys %gram) {
  $gram{$_} /= $total;
  if (defined($cgram{$_})) {
#    print STDERR "$_ : \$score += $cgram{$_}*log($cgram{$_}/$gram{$_});\n";
    $score += $cgram{$_}*log($cgram{$_}/$gram{$_});
  } else {
    $score += $cgram{UNKNOWNCGRAM}*log($cgram{UNKNOWNCGRAM}/$gram{$_});
  }
}
print $score;
print "\n";
