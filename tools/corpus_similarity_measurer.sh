#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

perl ${DIR}/extract_cgrams.pl 3 0 < $1 > $1.cgrams && perl ${DIR}/crap_level_measurer.pl $1.cgrams -n 3 -ut 0 < $2 | tail -1
