\documentclass[10pt]{beamer}
\mode<presentation>
{
  \usetheme{default}
  
  \usecolortheme{rose}

  % Only the current or past bullets are opaque, rest are transparent
  \setbeamercovered{transparent}
  
  % Create notes slides after main slides
  % \setbeameroption{show notes}
  
  % Use this option to print only the notes slide
  % \setbeameroption{show only notes}
  
  % Use this to remove the footline on the title page
  % You can build one document with this enabled and another
  % with this disabled. Use Preview in Mac to remove the title page
  % with the footline and insert it from the other document.
  %\setbeamertemplate{footline}{}  
}


\usepackage[frenchb]{babel}
% or whatever

\usepackage[utf8]{inputenc}
% or whatever

\usepackage{graphicx}
\usepackage{times}
\usepackage[T1]{fontenc}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.
\usepackage{color}
\definecolor{darkgray}{rgb}{.6, .6, .6}
\definecolor{darkgreen}{rgb}{0, .6, 0}
\definecolor{strange}{rgb}{.8,.8, 0}
\usepackage{xspace}
\usepackage{array}

\def\lefff{\textrm{Le}\textit{f{}f{}f}\xspace}
\def\leffe{\textrm{Le}\textit{f{}f}$\!$e\xspace}
\def\perlex{PerLex\xspace}
\def\sxpipe{\textsc{Sx}Pipe\xspace}
\def\alexina{{\sf Alexina}\xspace}
\def\melt{\textrm{MElt}\xspace}
\def\mmme{\textsc{mmme}\xspace}
\def\pergram{PerGram\xspace}
\newcommand{\postag}[1]{\texttt{#1}}
\def\ftb{\textsc{ftb}\xspace}
\def\train{\textsc{ftb-train}\xspace}
\def\dev{\textsc{ftb-dev}\xspace}
\def\test{\textsc{ftb-test}\xspace}
\def\sxspell{\textsc{Sx}Spell\xspace}
\def\texttodag{\textsc{text2dag}\xspace}
\def\syntax{\textsc{Syntax}\xspace}
\def\treetagger{TreeTagger\xspace}
\def\treetaggerlefff{TreeTagger$_{\rm\scriptstyle Le{\it fff}}$\xspace}
\def\berkeley{\textsc{f-bky}\xspace}
\def\unigramtagger{\textsc{unigram}\xspace}
\def\leffftagger{\textsc{unigram}$_{\rm\scriptstyle Le{\it fff}}$\xspace}

\usepackage{setspace}
\setstretch{1.2}

\def\lefff{Le{\it f}{\it f}{\it f}\xspace}
\def\alexina{Alexina\xspace}

\title{\melt}
\subtitle{Étiquetage morphosyntaxique de corpus édités ou bruités}

\author[Benoît Sagot] % (optional, use only with lots of authors)
{Benoît Sagot\\
\texttt{benoit.sagot@inria.fr}}

\institute[Alpage]{
  Alpage ---
  INRIA \& Université Paris--Diderot \\[1mm]
 \pgfuseimage{alpage}
  }

\date[Formation Corpus Écrits]{Formation Corpus Écrits\\
10 décembre 2013}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

%\subject{Comparing Complexity Measures}
% This is only inserted into the PDF information catalog. Can be left
% out. 



% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:
\pgfdeclareimage[height=1.5cm]{alpage}{alpage}
%\logo{\pgfuseimage{university-logo}}



% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>{Outline}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}
\AtBeginSubsection[]
{
  \begin{frame}<beamer>{Outline}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 

%\beamerdefaultoverlayspecification{<+->}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Qu'est-ce qu'étiqueter?}

\begin{frame}{Pourquoi étiqueter?}
  \begin{itemize}
  \item L'étiquetage morphosyntaxique est une des tâches les plus anciennes du
    TAL et de la linguistique informatique.
  \item Objectif: {\bf désambiguïser} pour préciser la nature de chaque
    mot dans des textes
  \item Applications:
    \begin{itemize}
    \item préalable possible à d'autre traitements automatiques (analyse
      syntaxique, désambiguïsation sémantique, extraction
      d'informations\ldots)
    \item préalable possible à des analyses linguistiques outillées
    \end{itemize}
  \item C'est un cas particulier de la tâche générale d'{\bf étiquetage
      de séquences}: associer une étiquette à chaque élément d'une séquence
  \end{itemize}
\end{frame}

\begin{frame}{Quelles étiquettes?}
  \begin{itemize}
  \item On parle d'\og annotation morphosyntaxique\fg, de \og
    part-of-speech tagging\fg, d'\og étiquetage en catégories\fg\ldots
  \item Ces notions diffèrent moins pour l'anglais que pour de
    nombreuses autres langues
  \item Dans toute langue à morphologie plus riche, on peut distinguer
    par convention:
    \begin{itemize}
    \item les parties du discours (nom, verbe, adjectif\ldots)
    \item les étiquettes morphosyntaxiques, qui incluent une partie du
      discours et des traits morphosyntaxiques
    \end{itemize}
  \item On peut convenir de conférer à un \og tagger\fg la tâche
    d'annoter en {\bf catégories}:
    \begin{itemize}
    \item chaque tagger peut avoir un jeu de catégories qui lui est propre
      (ou qui est propre au corpus annoté qui a servi à sa construction)
    \item ces catégories sont en général d'une granularité intermédiaire
      entre parties du discours et étiquettes morphosyntaxiques
    \item il se peut que tel jeu d'étiquette convienne pour tel type de
      corpus et pas pour tel autre\ldots
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Quelles séquences? (1/2)}
  \begin{itemize}
  \item Les étiquettes que l'on cherche à attribuer sont donc des
    informations linguistiques, à associer à des unités lexicales
  \item En pratique, on n'observe pas des unités lexicales, mais des
    {\em tokens} (unités définies typographiquement)
  \item La correspondance entre tokens et unités lexicales est non
    triviale et ambiguë, très délicate à définir linguistiquement et à
    automatiser informatiquement\\
    \begin{tabular}{ll}
    {\em aux} $\leftrightarrow$ {\em à le} &
    {\em en vertu de} $\leftrightarrow$ {\em en\_vertu\_de}\\
    {\em en vertu du} $\leftrightarrow$ {\em en\_vertu\_de le} &
    {\em bien que} $\leftrightarrow$ {\em bien que} ou {\em bien\_que}
  \end{tabular}
  \item En pratique, {\em tous} les taggers étiquettent presque
    exclusivement des tokens
    \begin{itemize}
      \item étiquettes multiples pour les amalgames: {\em aux/P+D}
      \item annotations un peu artificielles pour les composés: {\em
          bien/ADV que/CS}, {\em en/P vertu/NC de/P}
      \end{itemize}
    \item C'est un compromis acceptable pour les principales langues européennes
  \end{itemize}
\end{frame}


\begin{frame}{Quelles séquences? (2/2)}
  \begin{itemize}
  \item Nous nommerons \og {\bf mot}\fg cette notion intermédiaire et mal
    définie qui est presque celle de token mais pas toujours
    exactement, et sur laquelle nous calculerons des étiquettes
  \item Nous annotons donc des séquences de mots
    \begin{itemize}
      \item En général, ces séquences ne peuvent pas être trop longues
        (on cherche un étiquetage optimal à l'échelle de la séquence
        étiquetée, et plus elle est longue plus c'est difficile)
      \item On a donc besoin d'une seconde convention: l'unité de
        traitement, la séquence qui sera étiquetée comme un tout,
        indépendamment des autres séquences
      \item Par facilité nous nommerons \og {\bf phrase}\fg cette unité
      \end{itemize}
  \end{itemize}
\end{frame}

\section{Comment étiqueter?}

\begin{frame}{Approches par règles}
  \begin{itemize}
  \item Règles développées à la main
  \item Règles apprises automatiquement, notamment lorsqu'il s'agit de
    règles de post-édition du résultat d'un premier étiquetage\\
    Cf.~Brill tagger (Brill, 1995)
  \end{itemize}
\end{frame}

\begin{frame}{Approches statistiques (1/3)}
  Pour chaque mot, on calcule un score
  (souvent, une probabilité) pour chaque étiquette possible et on
  attribue au mot l'étiquette la plus probable
  \begin{itemize}
  \item fondées sur les modèles de Markov cachés (HMM): on fait
    l'hypothèse que l'on peut choisir l'étiquette du $n$-ième mot
    d'une phrase à partir de l'observation des seuls $k$ mots qui le
    précède
  \item dans un étiqueteur HMM de base, on n'utilise que deux informations pour
    calculer la probabilité d'une étiquette pour un mot donné:
    \begin{enumerate}
    \item la probabilité de trouver cette catégorie comme faisant suite aux $k$
      catégories précédentes
    \item la probabilité que le mot à étiqueter ait cette catégorie,
      indépendamment de tout contexte d'occurrence
    \end{enumerate}
  \end{itemize}
  Tout modèle statistique cherche à reproduire ce qu'il a appris dans
  son corpus d'entraînement. Par exemple, étiqueter un texte avec {\em
    je} et {\em tu} avec un étiqueteur entraîné sur des données
  journalistiques ne peut pas bien fonctionner\ldots
\end{frame}

\begin{frame}{Approches statistiques (2/3)}
  \begin{itemize}
  \item Plusieurs extensions sont possibles
  \item Schmid (1994, 1995) a identifié une difficulté dans le point 1 ci-dessus:
    si l'on a 50 catégories et que l'on fait $k=2$ (comme
    habituellement), on a donc 50$^3$ = 125~000 probabilités à estimer, ce qui
    nécessite des corpus énormes
    \begin{itemize}
    \item il a proposé et implémenté dans TreeTagger l'idée suivante:
      on peut regrouper différents contextes comme étant équivalents
      et donnant lieu à des probabilités pour chaque catégorie qui
      sont identiques; ces regroupements sont effectués au moyen d'un
      algorithme dit \og arbre de décision\fg
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Approches statistiques (3/3)}
  \begin{itemize}
    \item Ratnaparkhi (1996) a proposé (pour l'anglais) une autre approche: on peut utiliser également
      d'autres informations que les catégories précédentes, à
      condition d'utiliser les bons outils statistiques
      \begin{itemize}
        \item Toutes ces informations sont codées
          sous forme de {\bf traits} (ou \og features\fg) binaires\\
          Exemples: {\tt étiq$_{-2}$ = NC} ; {\tt suff2$_0$ = ez} ;
          {\tt mot$_{-1}$ = pour}\ldots
        \item La probabilité de chaque catégorie pour le mot courant
          est obtenue en additionnant des {\bf poids} associés à
          chaque couple (trait,~catégorie)
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{}
  \begin{tabular}{ccccc}
&&$\downarrow$&&\\
\#&  {\bf je} & {\bf le} & {\bf vois}&\#\\
\#&  CLS & &&\\
  \end{tabular}
\end{frame}


\begin{frame}{}
\begin{tabular}{ccc}
  \begin{tabular}{ccccc}
&&$\downarrow$&&\\
\#&  {\bf je} & {\bf le} & {\bf vois}&\#\\
\#&  CLS & &&\\
  \end{tabular}
  & $\longrightarrow$&
\begin{tabular}{l}
{\tt étiq$_{-2}$ = \#}\\
{\tt étiq$_{-1}$ = CLS}\\
{\tt étiq$_{-2..-1}$ = \#\_CLS}\\
{\tt mot$_{-2}$ = \#}\\
{\tt mot$_{-1}$ = je}\\
{\tt mot$_{0}$ = le}\\
{\tt mot$_{1}$ = vois}\\
{\tt suff2$_{-1}$ = je}\\
{\tt suff1$_{-1}$ = e}\\
{\tt suff2$_{0}$ = le}\\
{\tt suff1$_{0}$ = e}\\
\ldots
\end{tabular}
  \end{tabular}
\end{frame}


\begin{frame}{}
\footnotesize
\begin{tabular}{lccccc}
                               &CLO&NC&ADJ&\#&\ldots\\
{\tt étiq$_{-2}$ = \#}&+0.1&&&-1&\\
{\tt étiq$_{-2}$ = CLS}&-0.8&-2&&-1&\\
{\tt étiq$_{-2}$ = NC}&+0.1&+0.3&&-0.9&\\
{\tt étiq$_{-2}$ = ADJ}&-0.1&+0.2&&-0.9&\\
{\tt étiq$_{-1}$ = \#}&+0.1&+0.4&&-2&\\
{\tt étiq$_{-1}$ = CLS}&+0.4&-4&&-1.5&\\
{\tt étiq$_{-1}$ = NC}&+0.2&-0.1&&-1&\\
{\tt étiq$_{-1}$ = ADJ}&+0.1&+0.2&&-1&\\
{\tt mot$_{-1}$ = \#}&-4&-1&&-2&\\
{\tt mot$_{-1}$ = je}&+0.3&-4&&-2&\\
{\tt mot$_{-1}$ = tu}&+0.3&-5&&-2&\\
{\tt mot$_{-1}$ = le}&-5&+2&&-1.2&\\
{\tt mot$_{-1}$ = formation}&+0.01&+0.01&&-0.2&\\
{\tt mot$_{-1}$ = soleil}&+0.01&-0.1&&-0.1&\\
{\tt mot$_{0}$ = je}&-9&-10&&-0.8&\\
{\tt mot$_{0}$ = tu}&-8&-11&&-0.9&\\
{\tt mot$_{0}$ = le}&-0.6&-10&&-0.2&\\
{\tt mot$_{0}$ = formation}&-10&+3&&-0.1&\\
{\tt mot$_{0}$ = soleil}&-12&+4&&-0.3&\\
{\tt mot$_{0}$ = \#}&-19&-17&&+25&\\
\ldots &&&&&\\
\end{tabular}
\end{frame}

\begin{frame}{}
\footnotesize
\begin{tabular}{lccccc}
                               &CLO&NC&DET&\#&\ldots\\
{\tt étiq$_{-2}$ = \#}&+0.1&&&-1&\\
{\tt étiq$_{-1}$ = CLS}&+0.4&-4&&-1.5&\\
{\tt mot$_{-1}$ = je}&+0.3&-4&&-2&\\
{\tt mot$_{0}$ = le}&-0.6&-10&&-0.2&\\
(+ les autres traits) &&&&&\\\hline
total & 4 & -3 & 2 & -100 & \ldots\\
\end{tabular}
\normalsize
~\\[1cm]
  \begin{tabular}{ccccc}
&&$\downarrow$&&\\
\#&  {\bf je} & {\bf le} & {\bf vois}&\#\\
\#&  CLS & &&\\
  \end{tabular}
\end{frame}

\begin{frame}{}
\footnotesize
\begin{tabular}{lccccc}
                               &{\bf CLO}&NC&DET&\#&\ldots\\
{\tt étiq$_{-2}$ = \#}&+0.1&&&-1&\\
{\tt étiq$_{-1}$ = CLS}&+0.4&-4&&-1.5&\\
{\tt mot$_{-1}$ = je}&+0.3&-4&&-2&\\
{\tt mot$_{0}$ = le}&-0.6&-10&&-0.2&\\
(+ les autres traits) &&&&&\\\hline
total & {\bf 4} & -3 & 2 & -100 & \ldots\\
\end{tabular}
\normalsize
~\\[1cm]
  \begin{tabular}{ccccc}
&&&$\downarrow$&\\
\#&  {\bf je} & {\bf le} & {\bf vois}&\#\\
\#&  CLS & CLO &&\\
  \end{tabular}
\end{frame}


\begin{frame}{Modèles linéaires}
  \begin{itemize}
  \item Quels traits?
    \begin{itemize}
      \item plus de traits = plus d'informations
      \item trop de traits = pas assez de données pour les exploiter correctement
      \item chez Rathnaparki, les traits sont contextuels (sur les
        mots et les étiquettes déjà attribuées) et
        typographiques (suffixes, préfixes, majuscule présente, nombre
        présent)
    \end{itemize}
  \item Comment calculer ces poids (i.e., le {\bf modèle})?
    \begin{itemize}
    \item {\bf Entraînement} sur un corpus d'apprentissage annoté
      manuellement (annotations \og parfaites\fg): un
      algorithme d'apprentissage lit le corpus, l'étudie, et en
      déduit appoximativement les poids \og optimaux\fg (en un sens
      qui dépend de l'algorithme retenu)
      \item modèle à {\bf maximisation d'entropie} (ou régression
        logistique) $\rightarrow$ MEMM (Maximum Entropy Markov Model)
      \item {\bf perceptron} (bien plus rapide à entraîner, souvent très
        légèrement moins bon que les MEMM)
    \end{itemize}
  \end{itemize}
\end{frame}

\section{\protect\melt}

\begin{frame}{\melt: Problématiques scientifiques}
  \begin{itemize}
  \item Comment mieux exploiter le contexte droit, pour lequel on
    n'a pas encore calculé d'étiquettes?
  \item Comment améliorer l'étiquetage des mots inconnus (non vus dans
    le corpus d'entraînement)?
  \item Comment exploiter un lexique externe, source d'informations
    complémentaires à celles que l'on peut extraire du corpus
    d'entraînement? Comme traits? Comme contraintes?
  \item Si l'on veut développer un nouveau tagger (par exemple pour
    une nouvelle langue non dotée ou un nouveau type de textes), vaut-il mieux passer tout son
    temps à annoter du corpus, ou est-il plus efficace de dédier une
    partie de son temps au développement d'un lexique?
  \item Quelle est l'influence des problèmes d'orthographe et de
    typographie sur l'annotation morphosyntaxique? Comment remédier
    aux problèmes que cela induit?
  \end{itemize}
\end{frame}


\begin{frame}{\melt: Idées principales}
  \begin{itemize}
  \item Partir d'un modèle de type MEMM (ou perceptron)
  \item Ajouter de {\bf nouveaux traits extraits d'un lexique externe}, y
    compris sur les mots du {\bf contexte droit} (utiliser le lexique comme
    contrainte marche moins bien) (Denis et Sagot, 2009, 2012)\\
    Exemples: {\tt catlex$_{1}$ = NC|ADJ} (contribue sûrement
    positivement à la catégorie DET) ; {\tt catlex$_0$ = V} (aide à
    étiqueter un {\bf mot inconnu})
  \item Dans le cas de données bruitées, ne pas annoter les \og
    mots\fg, mais procéder en trois étapes
    \begin{itemize}
    \item normalisation temporaire: {\em chépa} $\rightarrow$ {\em je
        sais pas}
      \item étiquetage des mots normalisés: {\em je
        sais pas} $\rightarrow$ {\em je/CLS
        sais/V pas/ADV}
      \item dé-normalisation et redistribution des étiquettes: {\em je/CLS
        sais/V pas/ADV} $\rightarrow$ {\em chépa/CLS+V+ADV}
      \item Approche utilisée en pré-annotation pour le développement
        du French Social Media Bank (Seddah et al., 2012a) et en
        annotation classique sur l'anglais (Seddah et al., 2012b)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{\melt: Caractéristiques de l'archive distribuée}
  \begin{itemize}
  \item {\bf Logiciel libre (LGPL)} --- mais logiciel de recherche, en
    ligne de commande (mais intégration prévue dans TXM)
  \item Sont distribués:
    \begin{itemize}
    \item Un ensemble de {\bf modèles d'étiquetage} pour diverses langues
      (français, allemand, anglais, espagnol, italien, polonais, français
      oral transcrit)
    \item Le {\bf système \melt} proprement dit, pour l'étiquetage à l'aide d'un
      modèle et l'entraînement de nouveaux modèles
    \item Un {\bf lemmatiseur}, qui peut être appelé sur option comme post-traitement après l'étiquetage
    \item Un ensemble de scripts de {\bf pré-traitement} qui permettent, à la demande de l'utilisateur, de tokeniser un texte, de le segmenter en énoncés et d'y reconnaître certaines entités (URL, adresses e-mail, etc) avant de l'étiqueter
    \item Des outils, qui peuvent également être demandés sur simple
      option, qui permettent de traiter des {\bf données bruitées}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{\melt: Performances sur du texte édité}
\begin{itemize}
\item Corpus: French TreeBank (FTB, Corpus Arboré de Paris 7): section 1 pour
l'entraînement (9881 phrases), section 3 pour l'évaluation. Jeu de 28 étiquettes.\\
\item Lexique externe: le \lefff
\item Évaluation: \og précision\fg = pourcentage de mots ayant reçu la
  bonne étiquette. 
\end{itemize}
\begin{tabular}{lrr}\hline
  \textbf{Tagger} & \textbf{Précision globale (\%)} & \textbf{Précision  sur les}\\ 
& & \textbf{mots inconnus(\%)}\\ \hline
  \treetagger sans \lefff       & $96.12$ & $75.77$ \\
  \treetagger avec \lefff  & $96.55$ & $82.14$ \\
  \melt dans \lefff         & $97.00$ & $86.10$ \\
  \melt avec \lefff     & \textbf{97.75} & \textbf{91.36} \\
  \hline
\end{tabular}
\end{frame}


\begin{frame}{\melt: Performances sur du texte édité}
\begin{itemize}
\item Quelles erreurs? Comparaison entre résultats de \melt et corpus
  de développement (section~2 du FTB)
\end{itemize}
\footnotesize
\begin{tabular}{llr}\hline
\multicolumn{2}{l}{{\bf Type d'erreur}} & {\bf Fréquence}\\\hline
                & Adjectif vs. participe passé & $5.5\%$ \\
Erreurs standard & Erreurs sur {\em de}, {\em du}, {\em des} & $4.0\%$\\
                & Autres erreurs & $34.0\%$\\\hline
\multicolumn{2}{l}{Erreurs sur les nombres} & $15.5\%$\\\hline
\multicolumn{2}{l}{Erreurs liées à des entités nommées} & $27.5\%$\\\hline
\melt semble avoir raison  & Erreurs dans le corpus & $9\%$\\
& Cas pas clairs& $4.5\%$\\\hline
\end{tabular}
\end{frame}


\begin{frame}{Annoter plus de corpus ou développer un lexique?}
\resizebox{1\linewidth}{!}{\includegraphics{contours.pdf}}
\end{frame}

\begin{frame}{\melt: Étiquetage via normalisation, sur un exemple}
\scriptsize
\begin{tabular}{llll}\hline
Mots & Gold corrected             & Automatically corrected      & POS tags automatically \\
d'origine   & ``tokens''                    & and POS-tagged ``tokens'' & assigned to the                 \\%\hline
  &                  & & original tokens                  \\\hline
sa & ça & ça/{\em PRO} & sa/{\em PRO} \\
fé & fait & fait/{\em V} & fé/{\em V} \\
o & au & au/{\em P+D} & o/{\em P+D} \\
moin & moins & moins/{\em ADV} & moin/{\em ADV} \\
6 & 6 & 6/{\em DET} & 6/{\em DET} \\
mois & mois & mois/{\em NC} & mois/{\em NC} \\
qe & que & que/{\em PROREL} & qe/{\em PROREL} \\
les & les & les/{\em DET} & les/{\em DET} \\
preliminaires & préliminaires & preliminaires/{\em NC} & preliminaires/{\em NC} \\
sont & sont & sont/{\em V} & sont/{\em V} \\
" & "& "/{\em PONCT} & "/{\em PONCT} \\
sauté & sautés & sauté/{\em VPP} & sauté/{\em VPP} \\
" & "& "/{\em PONCT} & "/{\em PONCT} \\
c a dire & c'est-à-dire & c'est-à-dire/{\em CC} & c/{\em Y} a/{\em Y} dire/{\em CC} \\
qil & qu' il & qu'/{\em CS} il/{\em CLS} & qil/{\em CS+CLS} \\
yen & y en & y/{\em CLO} en/{\em CLO} & yen/{\em CLO+CLO} \\
a & a & a/{\em V} & a/{\em V} \\
presk & presque & presque/{\em ADV} & presk/{\em ADV} \\
pa & pas & pas/{\em ADV} & pa/{\em ADV} \\\hline
\end{tabular}
\end{frame}


\begin{frame}{\melt: Étiquetage via normalisation, sur un exemple}
\scriptsize
\begin{tabular}{llll}\hline
Mots & Gold corrected             & Automatically corrected      & POS tags automatically \\
d'origine   & ``tokens''                    & and POS-tagged ``tokens'' & assigned to the                 \\%\hline
  &                  & & original tokens                  \\\hline
sa & ça & ça/{\em PRO} & sa/{\em PRO} \\
fé & fait & fait/{\em V} & fé/{\em V} \\
o & au & au/{\em P+D} & o/{\em P+D} \\
moin & moins & moins/{\em ADV} & moin/{\em ADV} \\
6 & 6 & 6/{\em DET} & 6/{\em DET} \\
mois & mois & mois/{\em NC} & mois/{\em NC} \\
qe & que & \color{red}{que/{\em PROREL}} & \color{red}{qe/{\em PROREL}} \\
les & les & les/{\em DET} & les/{\em DET} \\
preliminaires & préliminaires & preliminaires/{\em NC} & preliminaires/{\em NC} \\
sont & sont & sont/{\em V} & sont/{\em V} \\
" & "& "/{\em PONCT} & "/{\em PONCT} \\
sauté & sautés & sauté/{\em VPP} & sauté/{\em VPP} \\
" & "& "/{\em PONCT} & "/{\em PONCT} \\
c a dire & c'est-à-dire & c'est-à-dire/{\em CC} & c/{\em Y} a/{\em Y} dire/{\em CC} \\
qil & qu' il & qu'/{\em CS} il/{\em CLS} & qil/{\em CS+CLS} \\
yen & y en & y/{\em CLO} en/{\em CLO} & yen/{\em CLO+CLO} \\
a & a & a/{\em V} & a/{\em V} \\
presk & presque & presque/{\em ADV} & presk/{\em ADV} \\
pa & pas & pas/{\em ADV} & pa/{\em ADV} \\\hline
\end{tabular}
\end{frame}


\begin{frame}{\melt: Étiquetage via normalisation, sur un exemple}
\scriptsize
\begin{tabular}{llll}\hline
Mots & Gold corrected             & Automatically corrected      & POS tags automatically \\
d'origine   & ``tokens''                    & and POS-tagged ``tokens'' & assigned to the                 \\%\hline
  &                  & & original tokens                  \\\hline
sa & ça & ça/{\em PRO} & sa/{\em PRO} \\
fé & fait & fait/{\em V} & fé/{\em V} \\
o & au & au/{\em P+D} & o/{\em P+D} \\
moin & moins & moins/{\em ADV} & moin/{\em ADV} \\
6 & 6 & 6/{\em DET} & 6/{\em DET} \\
mois & mois & mois/{\em NC} & mois/{\em NC} \\
qe & que & que/{\em PROREL} & qe/{\em PROREL} \\
les & les & les/{\em DET} & les/{\em DET} \\
preliminaires & préliminaires & preliminaires/{\em NC} & preliminaires/{\em NC} \\
sont & sont & sont/{\em V} & sont/{\em V} \\
" & "& "/{\em PONCT} & "/{\em PONCT} \\
sauté & sautés & sauté/{\em VPP} & sauté/{\em VPP} \\
" & "& "/{\em PONCT} & "/{\em PONCT} \\
c a dire & c'est-à-dire & \color{red}{c'est-à-dire/{\em CC}} & \color{red}{c/{\em Y} a/{\em Y} dire/{\em CC}} \\
qil & qu' il & qu'/{\em CS} il/{\em CLS} & qil/{\em CS+CLS} \\
yen & y en & y/{\em CLO} en/{\em CLO} & yen/{\em CLO+CLO} \\
a & a & a/{\em V} & a/{\em V} \\
presk & presque & presque/{\em ADV} & presk/{\em ADV} \\
pa & pas & pas/{\em ADV} & pa/{\em ADV} \\\hline
\end{tabular}
\end{frame}


\begin{frame}{\melt: Étiquetage via normalisation, sur un exemple}
\scriptsize
\begin{tabular}{llll}\hline
Mots & Gold corrected             & Automatically corrected      & POS tags automatically \\
d'origine   & ``tokens''                    & and POS-tagged ``tokens'' & assigned to the                 \\%\hline
  &                  & & original tokens                  \\\hline
sa & ça & ça/{\em PRO} & sa/{\em PRO} \\
fé & fait & fait/{\em V} & fé/{\em V} \\
o & au & au/{\em P+D} & o/{\em P+D} \\
moin & moins & moins/{\em ADV} & moin/{\em ADV} \\
6 & 6 & 6/{\em DET} & 6/{\em DET} \\
mois & mois & mois/{\em NC} & mois/{\em NC} \\
qe & que & que/{\em PROREL} & qe/{\em PROREL} \\
les & les & les/{\em DET} & les/{\em DET} \\
preliminaires & préliminaires & preliminaires/{\em NC} & preliminaires/{\em NC} \\
sont & sont & sont/{\em V} & sont/{\em V} \\
" & "& "/{\em PONCT} & "/{\em PONCT} \\
sauté & sautés & sauté/{\em VPP} & sauté/{\em VPP} \\
" & "& "/{\em PONCT} & "/{\em PONCT} \\
c a dire & c'est-à-dire & c'est-à-dire/{\em CC} & c/{\em Y} a/{\em Y} dire/{\em CC} \\
qil & qu' il & \color{red}{qu'/{\em CS} il/{\em CLS}} & \color{red}{qil/{\em CS+CLS}} \\
yen & y en & \color{red}{y/{\em CLO} en/{\em CLO}} & \color{red}{yen/{\em CLO+CLO}} \\
a & a & a/{\em V} & a/{\em V} \\
presk & presque & presque/{\em ADV} & presk/{\em ADV} \\
pa & pas & pas/{\em ADV} & pa/{\em ADV} \\\hline
\end{tabular}
\end{frame}

\begin{frame}{Les étiquettes multiples non standard les plus
    fréquentes}
\begin{itemize}
\item Données issues du French Social Media Bank
\end{itemize}
\begin{tabular}{lllll}\hline
Étiquettes multiples & Occ. & Exemple attesté & Équivalent standard \\\hline
{\em CLS+V} & 54 & {\em c} & {\em c' est} \\
{\em ADV+CLO} & 12 & {\em ni} & {\em n' y} \\
{\em CS+CLS} & 12 & {\em qil} & {\em qu' il} \\
{\em CLS+CLO} & 11 & {\em jen} & {\em j' en} \\
{\em CLO+V} & 9 & {\em ma}& {\em m' a} \\
{\em DET+NC} & 9 & {\em lamour} & {\em l' amour} \\
{\em ADV+V} & 7 & {\em non} & {\em n' ont}\\\hline
\end{tabular}
\end{frame}

\begin{frame}{Règles de normalisation}
\begin{itemize}
\item Règles simples à construire: extraction automatique de candidats
  séquences, renseignement manuel de l'équivalent souhaité
\item La correspondance entre mots d'origine et mots normalisés doit
  être préservée
\item Les règles sont appliquées par ordre décroissant de nombre de
  mots impliqués
\item Ces règles n'ont pas vocation à \og traduire\fg le texte brut en
  texte standard, mais à le transformer temporairement pour que
  l'étiqueteur puisse fonctionner correctement
\item Exemples (parmi les 1804 règles disponibles):
\end{itemize}
\end{frame}

\begin{frame}{Règles de normalisation: exemples}
\begin{center}
\begin{tabular}{ll}\hline
ke & que\\
C & C'\_est\\
jte & je\_te\\
c pa & c'\_est pas\\
je c & je sais\\
jc pa & je\_sais pas\\
ya pa & il\_y\_a pas\\
g pa & j'\_ai pas\\
sa va & ça va\\
c\_t & c'\_était\\
koi d bo & quoi de beau\\
sava & ça\_va \\\hline
dois ([\^{~} ]\{2,\})é	dois & \$1er\\
({\textbackslash}d+) moi & \$1 mois\\\hline
\end{tabular}
\end{center}
\end{frame}

\section{Utiliser \protect\melt}

\begin{frame}{}
~\\[1cm]
\begin{center}
Cf.~manuel utilisateur \& démo
\end{center}
\end{frame}


\end{document}



