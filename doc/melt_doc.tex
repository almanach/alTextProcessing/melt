\documentclass[10pt,a4paper]{article}

\usepackage{times}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{url}
\usepackage{xspace}
\usepackage{graphicx}
\usepackage[frenchb]{babel}

\def\lefff{\textrm{Le}\textit{f{}f{}f}\xspace}
\def\leffe{\textrm{Le}\textit{f{}f}$\!$e\xspace}
\def\perlex{PerLex\xspace}
\def\sxpipe{\textsc{Sx}Pipe\xspace}
\def\alexina{{\sf Alexina}\xspace}
\def\melt{\textrm{MElt}\xspace}
\def\mmme{\textsc{mmme}\xspace}
\def\pergram{PerGram\xspace}
\newcommand{\postag}[1]{\texttt{#1}}
\def\ftb{\textsc{ftb}\xspace}
\def\train{\textsc{ftb-train}\xspace}
\def\dev{\textsc{ftb-dev}\xspace}
\def\test{\textsc{ftb-test}\xspace}
\def\sxspell{\textsc{Sx}Spell\xspace}
\def\texttodag{\textsc{text2dag}\xspace}
\def\syntax{\textsc{Syntax}\xspace}
\def\treetagger{TreeTagger\xspace}
\def\treetaggerlefff{TreeTagger$_{\rm\scriptstyle Le{\it fff}}$\xspace}
\def\berkeley{\textsc{f-bky}\xspace}
\def\unigramtagger{\textsc{unigram}\xspace}
\def\leffftagger{\textsc{unigram}$_{\rm\scriptstyle Le{\it fff}}$\xspace}

\title{MElt\\Manuel de l'utilisateur}
\author{Benoît Sagot}
\date{Version 2.0}

\begin{document}

\maketitle

\melt est un système d'étiquetage séquentiel de textes. Plus précisément, l'archive \melt contient:
\begin{itemize}
\item Un ensemble de modèles d'étiquetage pour diverses langues, c'est-à-dire les données nécessaires à \melt pour
  étiqueter des corpus;
\item Le système \melt proprement dit, qui permet l'utilisation de ces modèles pour réaliser effectivement la tâche
  d'étiquetage, mais également pour construire de nouveaux modèles à partir de données annotées (corpus d'entraînement
  et, si disponible, lexique);
\item Un lemmatiseur, qui peut être appelé sur option comme post-traitement après l'étiquetage;
\item Un ensemble de scripts de pré-traitement qui permettent, à la demande de l'utilisateur, de tokeniser un texte, de
  le segmenter en énoncés et d'y reconnaître certaines entités (URL, adresses e-mail, etc) avant de l'étiqueter;
\item Des outils, qui peuvent également être appelés sur simple option, qui permettent de traiter des données bruitées
  (textes issus du web, par exemple) en les normalisant à la volée, en étiquetant alors le texte normalisé, puis en
  redistribuant les étiquettes sur les mots (tokens) d'origine.
\end{itemize}

Ce document décrit comment installer et utiliser \melt. Il donne aussi quelques indications sur les enjeux scientifiques
sous-jacents à son développement.

\section{Installation}

L'utilisation de base de \melt nécessite un système de type UNIX (par exemple, Linux, MacOS~X ou encore Cygwin sous
Windows) ainsi que les logiciels {\tt sh}, Perl et Python. \melt peut être téléchargé sous forme d'une archive .tgz sur
la page de \melt: \url{https://www.rocq.inria.fr/alpage-wiki/tiki-index.php?page=MElt}. L'installation se fait alors,
comme pour tout logiciel reposant sur une architecture {\tt autotools} (architecture standard pour le déploiement de
logiciels sous UNIX), de la façon suivante:
\begin{itemize}
\item Dans un terminal (ou invite de commande), décompresser l'archive ({\tt tar xvzf
    [nom\_de\_l'archive\_téléchargée].tgz}), puis aller dans le dossier ainsi créé ({\tt cd
    [nom\_de\_l'archive\_téléchargée]})
\item Préparer la compilation en l'adaptant à son ordinateur et à son système d'exploitation: \texttt{./configure}
\item Compiler: \texttt{make}
\item Installer le résultat de la compilation dans un endroit accessible au système: \texttt{sudo make install}
\end{itemize}
Cette installation, standard, nécessite d'avoir les droits permettant d'exécuter la commande {\tt sudo}. Une alternative
est de remplacer \texttt{./configure} par \texttt{./configure --prefix=/chemin/vers/un/dossier/d'installation} (dossier
où l'on doit avoir les droits d'écriture), de compiler avec {\tt make} puis d'installer simplement par {\tt make
  install}.

Pour tester l'installation, il suffit de lancer la commande suivante\footnote{Attention aux majuscules, l'exécutable
  s'appelle \melt, avec {\tt ME} en majuscules suivi de {\tt lt} en minuscules.}:
\begin{center}
\mbox{\tt echo "Ceci est un test" $|$ MElt}
\end{center}
Si tout s'est correctement installé, une réponse telle que celle ci-dessous doit être renvoyée:
\begin{center}
{\tt Ceci/PRO est/V un/DET test/NC}.
\end{center}
Si ce n'est pas le cas (\og command not found\fg, ou quelque chose de ce type), vérifier que la variable d'environnement
{\tt \$PATH} contient bien le dossier d'installation de l'exécutable {\tt MElt} (pour une installation par défaut, il
s'agit le plus souvent de {\tt /usr/local/bin}). Une autre possibilité est de remplacer les appels à {\tt MElt} par un
appel explicite avec le chemin complet\footnote{Par exemple {\tt echo "Ceci est un test" $|$ /usr/local/bin/MElt}}.

Si l'on souhaite pouvoir entraîner de nouveaux modèles, il faut également installer le logiciel {\tt megam}
(\url{http://www.umiacs.umd.edu/~hal/megam/}), disponible soit sous forme binaire (exécutable UNIX, fichier .exe pour
Windows) soit sous forme de fichiers sources, aux adresses suivantes:
\begin{itemize}
\item Exécutable Linux: \url{http://hal3.name/megam/megam_i686.opt.gz}
\item Exécutable Mac OS X (processeurs Intel 64 bits, c'est-à-dire toute machine raisonnablement récente):
  \url{http://alpage.inria.fr/~sagot/megam.opt}
\item Code source \url{http://hal3.name/megam/megam_src.tgz}. Attention: la compilation de {\tt megam} à partir des
  sources nécessite l'installation préalable du compilateur {\tt ocaml} pour le langage {\tt caml}
  (\url{http://caml.inria.fr}).
\end{itemize}

 
\section{Utilisation}

\subsection{Étiquetage}

\melt lit du texte, soit brut soit tokenisé et segmenté, sur l'entrée standard. Il l'étiquette puis écrit le résultat au
format Brown sur sa sortie standard.

Autrement dit, après installation, la commande 
\begin{center}
\mbox{\tt echo "Ceci est un test" $|$ MElt}
\end{center}
doit renvoyer quelque chose comme
\begin{center}
{\tt Ceci/PRO est/V un/DET test/NC}.
\end{center}
On notera qu'il y a un temps de lancement pour \melt: étiqueter 10 phrases va à peine plus vite qu'en étiqueter une
seule.

Par défaut, \melt utilise le modèle standard du français. Il suppose que le texte donné en entrée est déjà segmenté en
phrases (une phrase par ligne) et tokenisé (l'espace étant le séparateur de tokens). Il considère de plus qu'il doit
identifier les URL, les adresses e-mail et les smileys dans le texte avant de l'étiqueter, ces entités étant
particulièrement problématiques à traiter sans reconnaissance explicite. Enfin, les éléments XML, à condition qu'ils
constituent une ligne à eux seuls, sont laissés tels quels (qu'il s'agisse de balises seules, de séquences de balises ou
de balises encadrant du contenu, lui-même laissé inchangé). Ainsi,
\begin{center}
\mbox{\tt echo "Aller à http://www.inria.fr" $|$ MElt}
\end{center}
renverra quelque chose comme
\begin{center}
{\tt Aller/VINF à/P \{http://www.inria.fr\}\_URL/NPP}.
\end{center}
Ainsi, l'URL elle-même a été mise entre accolades, suivies d'un identifiant d'entité, {\tt \_URL}. C'est cet identifiant
que l'étiqueteur a traité comme un mot et a étiqueté, ici comme un nom propre.

Toutes ces décisions par défaut peuvent être modifiées en donnant des options à \melt. Pour un aperçu complet des
options disponibles dans une version donnée de \melt, il est toujours possible de faire apparaître l'aide en ligne par
la commande {\tt MElt -h}. Les options principales peuvent être classées comme suit:
\begin{itemize}
\item Sélection du modèle:
\begin{itemize}
\item[{\tt -m}] Chaque modèle d'étiquetage est un dossier comportant plusieurs fichiers. Par l'option {\tt -m
    [/chemin/vers/un/modèle]}, on spécifie à \melt quel modèle utiliser. Les options {\tt -l} et {\tt -m} sont
  incompatibles, puisqu'elles constituent deux manières concurrentes de spécifier un modèle.
\item[{\tt -l}] L'option {\tt -l [langue]}, spécifie à \melt d'utiliser le modèle par défaut pour une langue
  donnée. Dans le cas d'une installation standard, {\tt -l [langue]} est équivalent à {\tt -l
    /usr/loca/share/melt/[langue]}.
\item Exemple: pour étiqueter une phrase en anglais, on peut par exemple utiliser la commande suivante:\\
  {\tt echo "This is a test" | MElt -l en}\\
  qui renvoie en principe\\
  {\tt This/DT is/VBZ a/DT test/NN}.
\end{itemize}
\item Tokenisation, segmentation et détection de certaines entités (comme indiqué ci-dessus, \melt considère par défaut
  que le texte est tokenisé, mais identifie malgré tout les URL, adresses e-mails et smileys):
\begin{itemize}
\item[{\tt -t}] Spécifie à \melt qu'il doit segmenter en phrases et tokeniser le texte avant de l'étiqueter, et ce au
  moyen de modules extraits de la chaîne \sxpipe et intégrés à \melt. Cette option n'est pas compatible avec l'option
  {\tt -c} décrite plus bas, car elle l'implique automatiquement. Les éléments XML, à condition qu'ils constituent une
  ligne à eux seuls, sont laissés tels quels (qu'il s'agisse de balises seules, de séquences de balises ou de balises
  encadrant du contenu, lui-même laissé inchangé). Incompatible avec {\tt -T}.
\item[{\tt -T}] Comme {\tt -t}, mais le tokeniseur/segmenteur utilisé est un tokeniseur qui tente d'appliquer les
  conventions de tokensiation et segmentation utilisées par l'analyseur syntaxique Bonsai. Incompatible avec {\tt -t}.\\
  Note: pour l'instant, les éléments XML ne sont pas gérés correctement par ce tokeniseur.
\item[{\tt -r}] Spécifie à \melt qu'il ne doit pas chercher à détecter les URL, adresses e-mail et smileys avant
  d'appliquer éventuellement le tokeniseur à la Bonsai (option {\tt -T}, soit avant d'étiqueter directement (si ni {\tt
    -t} ni {\tt -T} ne sont donnés). Si le tokeniseur/segmenteur issu de \sxpipe est utilisé, cette option n'a aucun
  effet puisque ce dernier détecte nécessairement les URL, adresses e-mail et smileys.
\item[{\tt -C}] Ne pas étiqueter, sortir le résultat des prétraitements et/ou de la normalisation. Exemple:\\
  {\tt echo "Aller à http://www.inria.fr" $|$ MElt}\\
doit renvoyer:\\
{\tt Aller à \{http://www.inria.fr\}\_URL}
\end{itemize}
\item Lemmatisation:
\begin{itemize}
\item[{\tt -L}] Utilise le lemmatiseur en post-traitement. Exemple:
  {\tt echo "Ceci est un test" $|$ MElt -L}\\
  doit renvoyer\\
  {\tt Ceci/PRO/ceci est/V/être un/DET/un test/NC/test}\\
  Les lemmes pour les mots inconnus du lexique sont précédés d'un symbole *\\
  Note: ce lemmatiseur n'est pas disponible dans tous les modèles distribués avec \melt. En effet, les données
  nécessaires au lemmatiseur pour fonctionner ne sont construites correctement lors de la construction d'un modèle que
  si le jeu d'étiquette du lexique externe est identique au jeu d'étiquettes du corpus d'entraînement (cette contrainte
  n'esst pas nécessaire pour la seule construction du modèle d'étiquetage proprement dit).
\end{itemize}
\item Traitement de données bruitées (disponible uniquement pour français et l'anglais):
\begin{itemize}
\item[{\tt -n}] Normaliser le texte avant de l'étiqueter, et restaurer après étiquetage les tokens d'origine. Les
  étiquettes sont redistribuées sur les tokens d'origine; l'étiquette \postag{x} est attribuée aux amalgames
  non-standard (type {\em chépa}), et tous les composants de composés non-standard sont étiquetés \postag{y} (par
  exemple, les deux premiers tokens de {\em c a dire}). Exemple complet:\\
  {\tt echo "c a dire ke c un truc de ouf" | MElt -n}\\
  doit renvoyer:\\
  {\tt c/Y a/Y dire/Y ke/CS c/X un/DET truc/NC de/P ouf/ADJ}
\item[{\tt -N}] Normaliser le texte avant de l'étiqueter, et restaurer après étiquetage les tokens d'origine. Les
  étiquettes sont redistribuées sur les tokens d'origine; les amalgames non-standard (type {\em chépa}) sont étiquetés
  avec la liste des étiquettes des mots amalgamés séparés par le symbole \postag{+} (ici, \postag{cls+v+advneg}), et les
  composants de composés non-standard sont étiquetés \postag{y} ou \postag{gw} (par exemple, les deux premiers tokens de
  {\em c a dire}), sauf le dernier composant qui reçoit l'étiquette du composé complet. Exemple complet:\\
  {\tt echo "c a dire ke c un truc de ouf" | MElt -n}\\
  doit renvoyer:\\
  {\tt c/Y a/Y dire/CC ke/CS c/CLS+V un/DET truc/NC de/P ouf/ADJ}
\item[{\tt -C}] (rappel) Ne pas étiqueter, sortir le résultat des prétraitements et/ou de la normalisation. Exemple:\\
  {\tt echo "c un truc de ouf" | MElt -n -C}\\
  doit renvoyer:\\
  {\tt \{c\} c' \{\} est un truc de \{ouf\} fou}\\
  On notera que la correspondance entre tokens d'origine et tokens normalisés est donnée via les contenus entre
  accolades: un token modifié est placé entre accolades avant le ou les tokens modifés qui lui correspondent (s'il y en
  a plusieurs, les tokens modifiés suivants sont précédés d'une paire d'accolades sans contenu).
\end{itemize}
\begin{itemize}
\item Options diverses:
\item[{\tt -c}] Gère les <<~commentaires~>> à la \sxpipe. Autrement dit, toute séquence entre accolades est
  ignorée. Implique {\tt -r}, incompatible avec {\tt -T} ou {\tt -t}.
\item[{\tt -d}] Toute phrase intégralement en majuscules et minusculisée avant étiquetage.
\item[{\tt -P}] Ajouter derrière chaque étiquette la probabilité que lui a assignée le modèle d'étiquetage.
\item[{\tt -e}] Ne pas exécuter \melt, sortir la commande complète telle qu'elle aurait été exécutée sans cette option.
\item[{\tt -h}] Renvoie un message d'aide décrivant toutes les options disponibles.
\item[{\tt -v}] Renvoie la version de MElt.
\end{itemize}
\end{itemize}

\subsection{Entraînement de nouveaux modèles}

L'entraînement de nouveaux modèles nécessite d'avoir préalablement installé {\tt megam} (voir plus haut). Pour entraîner
un nouveau modèle \melt, il faut disposer des données suivantes (des exemples sont donnés ci-dessous):
\begin{itemize}
\item un {\bf corpus d'entraînement} étiqueté au format Brown, c'est-à-dire qui respecte les conventions suivantes:
\begin{itemize}
\item une phrase par ligne;
\item le caractère d'espace est le séparateur de mots (tokens);
\item chaque mot est immédiatement suivi du symbole <<~/~>> puis d'une étiquette, qui ne doit pas comporter de symbole
  <<~/~>>.
\end{itemize}
\item un {\bf lexique externe}, qui peut être un fichier vide (pour le cas où l'on n'a pas ou ne souhaite pas utiliser
  de lexique externe), mais qui est typiquement un lexique au format texte à 3 ou 4 colonnes (la quatrième étant
  facultative) séparées par des tabulations. Chaque ligne est une entrée lexicale, répartie sur les 3 ou 4 colonnes
  comme suit:
\begin{itemize}
\item[colonne 1] Le mot (token). Il ne doit comporter aucun espace (les composants de mots composés doivent être séparés
  par des blancs soulignés)
\item[colonne 2] Une catégorie (note: pour que l'entraînement du modèle construise également un lemmatiseur, cette
  catégorie doit être cohérente avec les catégories utilisées dans le corpus; pour le seul entraînement du modèle
  d'étiquetage, il n'est pas nécessaire de respecter cette contrainte, puisque le fait que le mot ait cette catégorie
  dans le lexique externe est utilisé comme trait et non comme contrainte (par exemple, à aucun moment de l'étiquetage
  les catégories issues du lexiques ne sont comparées à celles issues du corpus; en revanche, une telle comparaison est
  nécessaire pour le lemmatiseur).
\item[colonne 3] Un lemme. Si l'on ne souhaite pas ou ne peut pas entraîner de lemmatiseur en même temps qu'un
  étiqueteur, on peut systématiquement remplir cette colonne par un faux lemme (l'usage est de mettre {\tt *} à toutes
  les entrées).
\item[colonne 4 (facultative et expérimentale)] Soit 0 soit 1, selon que l'entrée lexicale est morphologiquement
  régulière. Des travaux en cours visent à construire automatiquement une telle information et à en étudier l'utilité
  pour un étiqueteur, notamment pour améliorer l'étiquetage des mots inconnus. Il est fortement déconseillé d'utiliser
  des lexiques à 4 colonnes à ce stade du développement de \melt.
\end{itemize}
\end{itemize}
~\\[-1em]

Exemple de corpus:\\
\begin{tabular}{|l|}\hline
{\tt Ceci/PRO est/V un/DET petit/ADJ corpus/NC ./PONCT}\\
{\tt Il/CLS ne/ADV comprend/V que/ADV deux/DET phrases/NC ./PONCT}\\\hline
\end{tabular}\\[0.5em]

Exemple de lexique:\\
\begin{tabular}{|lll|}\hline
{\tt pondérerions} & {\tt V} & {\tt pondérer}\\
{\tt domptions} & {\tt VS} & {\tt dompter}\\
{\tt domptions} & {\tt V} & {\tt dompter}\\
{\tt cahotements} & {\tt NC} & {\tt cahotement}\\
{\tt translatés} & {\tt VPP} & {\tt translater}\\
{\tt schématique} & {\tt ADJ} & {\tt schématique}\\\hline
\end{tabular}\\[0.5em]

\section{Principes de fonctionnement}

De nombreux systèmes pour l'étiquetage automatique en parties du discours ont été développés pour un large éventail de
langues. Parmi les systèmes les plus performants, on trouve ceux qui s'appuient sur des techniques d'apprentissage
automatique\footnote{Se reporter à~\cite{manning_schutze99} pour un panorama complet.}. Pour certaines langues comme
l'anglais ou d'autres langues très étudiées, ces systèmes ont atteint des niveaux de performance proches des niveaux
humains. Il est intéressant de constater que la majorité de ces systèmes n'ont pas recours à une source externe
d'informations lexicales, et se contentent d'un lexique extrait <<~à la volée~>> à partir du corpus d'apprentissage
(cf.~cependant \cite{hajic00}).

L'un des objectifs scientifiques ayant conduit au développement de \melt est précisément l'étude de la faisabilité et de
l'impact de l'utilisation de telles ressources lexicales externes. Nous avons pu démontrer
\cite{denis09,denis10,denis12} qu'il y a là matière à améliorations dans diverses directions:
\begin{itemize}
\item les étiqueteurs ainsi construits permettent en un meilleur traitement des mots <<~inconnus~>>, c'est-à-dire des
  mots absents du corpus d'apprentissage, dès lors qu'ils sont présents dans le lexique externe;
\item le temps de développement des ressources (corpus d'entraînement, lexique externe) nécessaire pour atteindre un
  niveau de performances fixé à l'avance est moins élevé si l'on développe en parallèle un corpus annoté et un lexique
  externe; autrement dit, si l'on ne dispose pas à l'avance d'un lexique externe, ne développer qu'un corpus annoté ne
  constitue pas la solution optimale en temps pour avoir construite en un temps donné le meilleur étiqueteur.
\end{itemize}
Nous avons également montré la pertinence de notre approche sur plusieurs langues. Si l'on entraîne le système \melt sur
le Corpus Arboré de Paris~7 \cite{ftb} et en utilisant le lexique \lefff \cite{sagot10lefff} comme lexique externe,
l'étiqueteur obtenu est état-de-l'art pour le français. À ce jour avons entraîné avec succès des modèles d'étiquetage
(c'est-à-dire des <<~instances~>> de \melt) pour l'anglais, l'espagnol, l'italien, l'allemand, le néerlandais, le
portugais, le polonais et le persan. La plupart de ces modèles sont distribués avec le système \melt proprement dit.

Pour présenter les principes sous-jacents au système d'étiquetage \melt, nous présenterons tout d'abord le modèle dit
<<~de base~>>, qui n'exploite pas de lexique externe. Puis nous montrerons comment nous intégrons les informations
provenant d'un lexique externe.

De manière générale, \melt repose sur des modèles discriminants linéaires et séquentiels. L'idée sous-jacente à de tels
modèles est que les décisions (ici, les choix d'étiquettes) se font de gauche à droite, en s'appuyant sur un certain
nombre de traits ({\em features}). Ces traits peuvent être de diverses natures: le ou les mots qui précèdent (2 au plus,
sinon les statistiques que l'on fait sur le corpus d'entraînement ne sont plus significatives), le ou les mots qui
suivent (même remarque), les préfixes et suffixes de ces mots (au sens informatique), mais également les étiquettes déjà
attribuées aux mots qui précèdent (les mots suivants n'ayant pas encore reçu d'étiquette). Dans le cas de l'utilisation
d'un lexique externe, des traits issus de ce lexique pourront être utilisés également. Ces traits sont choisis comme
étant tous booléens: chaque trait est ou bien présent ou bien absent (par exemple, il n'y a pas de trait <<~étiquette du
mot précédent~>>, qui pourrait valoir \postag{v} ou \postag{det}, mais plusieurs traits de la forme <<~l'étiquette du
mot précédent est \postag{v}~>>, qui peut donc valoir Vrai ou Faux). L'idée générale est alors la suivante: pour chaque
trait, l'algorithme d'apprentissage calcule pour chaque étiquette un poids, qui indique combien ce trait, s'il est Vrai,
renforce l'hypothèse selon laquelle cette étiquette est la bonne. Par exemple, le trait <<~l'étiquette du mot précédent
est \postag{det}~>> est susceptible d'avoir un poids positif relativement élevé en faveur des étiquettes \postag{nc} et
\postag{adj}, mais un poids très négatif pour l'étiquette \postag{v}: en effet, il est vraisemblable qu'un mot précédé
d'un déterminant soit un nom ou un adjectif, et très peu vraisemblable qu'il soit un verbe.

Plusieurs types de modèles permettent de calculer de tels poids à partir de données d'entraînement (corpus
d'apprentissage annoté manuellement et, le cas échéant, lexique externe).  Les premières versions de \melt ne
permettaient que l'entraînement de modèles markoviens à maximum d'entropie (Maximum Entropy Markov Models,
MEMM). Désormais, lors de l'entraînement d'un nouveau modèle, l'utilisateur peut choisir d'utiliser un algorithme
d'apprentissage de type MEMM ou un perceptron multiclasse, ce dernier étant bien plus rapide à entraîner mais conduisant
en général à des modèles très légèrement moins précis que les MEMM.



\subsection{Le modèle de base}

Étant donné un jeu d'étiquettes $T$ et une chaîne de mots (tokens)
$w_1^n$, on définit la tâche d'étiquetage comme le processus
consistant à assigner à $w_1^n$ la séquence d'étiquettes $\hat{t}^n_1
\in T{^n}$ de vraisemblance maximale.  Suivant \cite{ratnaparkhi96},
on peut alors approcher la probabilité conditionnelle $P(t^n_1|w_1^n)$
de sorte que:
\begin{equation}
  \hat{t}^n_1 = \arg\max_{t_1^n \in T{^n}} P(t^n_1|w_1^n) \approx \arg\max_{t_1^n \in T{^n}} \prod_{i=1}^n P(t_i|h_i),
\end{equation}
où $t_i$ est l'étiquette du mot $w_i$ et $h_i$ est le {\em contexte} de $(w_i,t_i)$, qui comprend la
séquence des étiquettes déjà assignées $t_1^{i-1}$ et la séquence des mots $w_1^n$.

Dans un modèle à maximisation d'entropie, on estime les paramètres d'un modèle exponentiel qui a la
forme suivante: 
\begin{equation}
  P(t_i|h_i) = \frac{1}{Z(h)}\cdot\exp\left(\sum\limits_{i=j}^m \lambda_j f_j(h_i,t_i)\right)
\end{equation}
Les $f_1^m$ sont des traits, fonctions définies sur l'ensemble des
étiquettes $t_i$ et des historiques $h_i$ (avec $f(h_i,t_i) \in
\{0,1\}$), les $\lambda_1^m$ sont les paramètres associés aux $f_1^m$,
et $Z(h)$ est un facteur de normalisation sur les différentes
étiquettes. Dans ce type de modèle, le choix des paramètres est
assujetti à des contraintes garantissant que l'espérance de chaque
trait soit égale à son espérance empirique telle que mesurée sur le
corpus d'apprentissage \cite{berger_et_al:96}. Le système \melt estime
ces paramètres en utilisant l'algorithme dit {\em
  Limited Memory Variable Metric Algorithm} \cite{malouf:02}
implémenté au sein du système Megam\footnote{Librement disponible à
  l'adresse \url{http://www.cs.utah.edu/~hal/megam/}.}
\cite{daume04cg-bfgs}.

Les classes de traits que nous avons utilisées pour la conception du
modèle de base, c'est-à-dire du
modèle n'utilisant pas de lexique externe, est un sur-ensemble des
traits utilisé par \cite{ratnaparkhi96} et \cite{toutanova_manning00}
pour l'anglais (qui étaient largement indépendants de la langue). Ces
traits peuvent être regroupés en deux sous-ensembles. Le premier
rassemble des traits dits {\em internes} qui essaient de capturer les
caractéristiques du mot à étiqueter. Il s'agit notamment du mot $w_i$
lui-même, de ses préfixes et suffixes de longueur $1$ à $4$, ainsi que
de traits booléens qui testent si $w_i$ contient ou non certains
caractères particuliers comme les chiffres, le tiret ou les
majuscules. Le deuxième ensemble de traits, dits {\em externes},
modélise le contexte du mot à étiqueter. Il s'agit tout d'abord des
mots qui sont dans les contextes gauche et droit de $w_i$ (à une
distance d'au plus $2$). Ensuite, nous intégrons comme traits
l'étiquette $t_{i-1}$ assignée au mot précédent, ainsi que la
concaténation des étiquettes $t_{i-1}$ et $t_{i-2}$ pour les deux mots
précédents $w_i$. La liste détaillée des classes de traits utilisées
dans le système de base est indiquée à la table~\ref{tab:baselinefeatures}.

\begin{table}[ht]
\center
\small
\begin{tabular}{lr}
  \hline
  \multicolumn{2}{l}{\textbf{Traits internes}}\\
  \hline
  $w_i = X$ & \& $t_i=T$\\
  Préfixe de $w_i = P, |P| < 5 $ & \& $t_i=T$\\
  Suffixe de $w_i = S, |S| < 5 $ & \& $t_i=T$\\
  $w_i$ contient un nombre & \& $t_i=T$\\
  $w_i$ contient un tiret & \& $t_i=T$\\
  $w_i$ contient une majuscule & \& $t_i=T$\\
  $w_i$ contient uniquement des majuscules & \& $t_i=T$\\
  $w_i$ contient une majuscule et n'est pas le premier mot d'une phrase & \& $t_i=T$\\
  \hline
  \multicolumn{2}{l}{\textbf{Traits externes}}\\
  \hline
  $t_{i-1} = X$ & \& $t_i=T$\\
  $t_{i-2}t_{i-1} = XY$ & \& $t_i=T$\\
  $w_{i+j} = X, j \in \{-2,-1,1,2\} $ & \& $t_i=T$\\
  \hline
\end{tabular}
\caption{Traits de base utilisés par le système de base}
\label{tab:baselinefeatures}
\end{table}

Une différence importante avec le jeu de traits de
\cite{ratnaparkhi96} vient du fait que nous n'avons pas restreint
l'application des traits de type préfixes et suffixes aux mots qui
sont rares dans le corpus d'apprentissage. Dans notre modèle, ces
traits sont toujours construits, même pour les mots fréquents. En
effet, nous avons constaté lors du développement\footnote{Plus précisément, lors du développement du \melt du français entraîné sur le Corpus Arboré de Paris~7 et sur le lexique \lefff.} que la prise en
compte systématique de ces traits conduit à de meilleurs résultats,
notamment sur les mots inconnus. De plus, ces traits sont probablement
plus discriminants sur des langues à morphologie plus riche que l'anglais, comme le français ou l'italien, que sur l'anglais, langue traitée par \cite{ratnaparkhi96}. Une autre différence entre
notre modèle de base et les travaux antérieurs concerne le
lissage. \cite{ratnaparkhi96} et \cite{toutanova_manning00} seuillent
leurs traits à un nombre d'occurrence de $10$ pour éviter les données
statistiquement non significatives. Nous n'avons pas seuillé nos
traits mais avons utilisé à la place une régularisation gaussienne sur
les poids\footnote{Plus précisément, nous utilisons un
  distribution gaussienne avec une précision (c'est-à-dire une
  variance inverse) de $1$, ce qui est la valeur par défaut dans
  Megam; d'autres valeurs ont été testées pendant la phase de
  développement, mais n'ont pas amélioré les résultats.}, ce qui
est une technique de lissage plus motivée
statistiquement\footnote{Informellement, l'effet de ce type de
  régluarisation est de pénaliser les poids artificiellement élevés en
  forçant l'ensemble des poids à respecter une certaine distribution
  {\em a priori}, ici une distribution gaussienne centrée sur
  zéro.}.

%\pcomment{Add other features based on new experiments. If time, test and include Collins pattern
%  features.}
\subsection{Intégration des informations lexicales dans l'étiqueteur}

L'avantage du modèle de base est de permettre un
ajout aisé de traits supplémentaires, y compris de traits dont les
valeurs sont calculées à partir d'une ressource externe au corpus
d'apprentissage.

Pour chaque mot $w_i$, nous générons une nouvelle série de traits
internes basés sur la présence (ou non) de $w_i$ dans le lexique externe et, le
cas échéant, les étiquettes associées à $w_i$ par ce même lexique. Si $w_i$
est associé à une étiquette unique $t_i$, nous générons un trait qui
encode l'association non ambiguë entre $w_i$ et $t_{i,0}$. Lorsque
$w_i$ est associé à plusieurs étiquettes $t_{i,0},\dots,t_{i,m}$ par
le lexique externe, nous générons un trait interne pour chacune de ses
étiquettes possibles $t_{i,j}$, ainsi qu'un trait interne qui
représente la disjonction de $m$ étiquettes. Enfin, si $w_i$ n'est pas
recensé dans le lexique externe, nous créons un trait spécifique qui encode le
statut d'inconnu du lexique\footnote{Pour les mots qui apparaissent en
  position initiale dans la phrase, nous vérifions au prélalable que
  la version décapitalisée du mot n'est pas présente non plus dans le
  lexique externe.}.

De même, nous utilisons le lexique externe pour construire de nouveaux traits
externes: nous construisons l'équivalent des traits internes pour les
mots des contextes gauche et droit à une distance de moins de $2$ du
mot courant. Nous générons également des traits bigrammes
correspondant à la concaténation des étiquettes du lexique externe pour les $2$
mots à gauche, les $2$ mots à droite, et les deux mots qui entourent
$w_i$. Lorsque ces mots sont ambigus pour le lexique externe, seule leur
disjonction contribue au bigramme, et si l'un ce ces mots est inconnu,
la valeur \textit{unk} tient lieu d'étiquette\footnote{Différentes
  valeurs de <<~fenêtre~>> ont été essayées lors de la phase de
  développement: $1$, $2$ et $3$. Bien que le passage de $1$ à $2$
  mène à une amélioration significative, le passage de $2$ à $3$ mène
  à une légère dégradation.}.


La liste détaillée des classes de traits utilisées par le modèle complet, en plus de ceux de la table~\ref{tab:baselinefeatures}, est indiquée à la table~\ref{tab:leffffeatures}. Ce jeu de traits, présenté dans \cite{denis10}, étend légèrement celui présenté par \cite{denis09}.

\begin{table}[h]
\center
\small
\begin{tabular}{lr}
\hline
\multicolumn{2}{l}{\textbf{Traits lexicaux internes}}\\
\hline

$t_i =_{uni} X$, if $\textsf{lex\_ext}(w_i) = \{X\}$ & \& $t_i=T$\\
$t_i = X$, $\forall X \in \textsf{lex\_ext}(w_i)$ if
$|\textsf{lex\_ext}(w_i)| > 1$ & \& $t_i=T$\\
$t_i = \bigvee \textsf{lex\_ext}(w_i)$ if $|\textsf{lex\_ext}(w_i)| > 1$ & \& $t_i=T$\\
$t_i = unk$, if $\textsf{lex\_ext}(w_i) = \emptyset$ & \& $t_i=T$\\
\hline
\multicolumn{2}{l}{\textbf{Traits lexicaux externes}}\\
\hline
$t_{i+j} = \bigvee \textsf{lex\_ext}(w_{i+1}), j \in \{-2,-1,1,2\} $ & \& $t_i=T$\\
$t_{i+j}t_{i+k} = \bigvee \textsf{lex\_ext}(w_{i+j})\bigvee
\textsf{lex\_ext}(w_{i+k}), (j,k) \in \{(-2,-1),(+1,+2),(-1,+1)\}$ & \& $t_i=T$\\
\hline
\end{tabular}
\caption{Traits lexicaux ajoutés au modèle de base dans le modèle complet}
\label{tab:leffffeatures}
\end{table}


Ces différents traits permettent d'avoir une information, ne serait-ce
qu'ambiguë, sur les étiquettes dans le contexte droit du mot, ce que
ne permettent pas les traits de base. Ceux-ci
n'incluent que les étiquettes sur le contexte gauche, les seules à
pouvoir être intégrées dans un décodage gauche-droite. Par ailleurs,
cette manière d'intégrer les informations issues du lexique au modèle
sous forme de traits supplémentaires a l'avantage de ne pas ajouter de
contraintes {\em fortes}, et d'être ainsi robuste à d'éventuelles
erreurs ou incomplétudes du lexique.

Une autre façon, plus directe, d'exploiter une ressource lexicale
exogène consiste en effet à utiliser les informations lexicales comme
{\em filtre}. A savoir, on contraint l'étiqueteur à choisir pour un
mot $w$ une étiquette correspondant soit à une occurrence de $w$ dans
le corpus, soit à une entrée du lexique pour $w$. C'est l'approche
employée par exemple par \cite{hajic00} pour des langues à morphologie
très riche, et notamment pour le tchèque. Dans \cite{denis09}, nous
avons montré sur notre étiqueteur \melt du français que cette stratégie ne permet d'améliorer que
marginalement les performances du modèle de base, et restent largement
en-deçà de celles du modèle complet tel que présenté ci-dessus.



\subsection{Décodage}

La procédure de décodage (c'est-à-dire l'étiquetage proprement dit une
fois le modèle construit) repose sur un algorithme de type {\em beam
  search} pour trouver la séquence d'étiquettes la plus probable pour
une phrase donnée. Autrement dit, chaque phrase est décodée de gauche
à droite, et l'on conserve pour chaque mot $w_i$ les $n$ séquences
d'étiquettes candidates les plus probables du début de la phrase
jusqu'à la position $i$. Pour nos expériences, nous avons utilisé un
{\em beam} de taille $3$\footnote{Nous avons essayé d'autres valeurs
  ($5$, $10$, $20$) pendant le développement du \melt du français, mais ces valeurs n'ont
  pas conduit à des variations significatives de performance.}. De
plus, la procédure de test utilise un {\em dictionnaire d'étiquettes})
qui liste pour chaque mot les étiquettes qui lui sont associées dans
le corpus d'apprentissage. Ceci réduit considérablement l'ensemble des
étiquettes parmi lesquelles l'étiqueteur peut choisir pour étiqueter
un mot donné, ce qui conduit, comme le montrent nos expériences, à de
meilleures performances tant en termes de précision que d'efficacité
en temps.

\subsection{Normalisation et étiquetage de textes bruités}

{\em à rédiger}



\bibliographystyle{apalike}
\bibliography{melt_doc}

\end{document}