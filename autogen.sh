#!/bin/sh

echo aclocal\
&& aclocal \
&& echo automake\
&& automake --gnu --add-missing \
&& echo autoconf\
&& autoconf
