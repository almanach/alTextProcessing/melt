#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$remove_non_standard_amalgams = 1;

print STDERR "  MElt TAGGING REALIGNER: Realigning POS-tags to original tokens...\n";

while (<>) {
  chomp;
  s/^ +//;
  s/ +$//;
  $out = "";
  s/  +/ /g;

  # réalignement sur les tokens d'origine (premier pas)
  while (s/{([^{}]+) ([^{} ]+)} ([^ \/]+)\/[^ ]+/{$1} ${3}_1\/Y {$2} ${3}_n\/Y/g) {}
  
  # corrections/normalisations manuelles
  while (s/{([^{}]+) ([^{} ]+)} ([^ \/]+)\/[^ ]+/{$1} ${3}_1\/Y {$2} ${3}_n\/Y/g) {}
  s/({ *[^{} ]+ *})\s*_SMILEY\/[^ ]+/$1 _SMILEY\/I/g;
  s/({ *[^{} ]+ [^{}]+}\s*)_SMILEY\/[^ ]+/$1 _SMILEY\/X/g;
  s/^([0-9\.]+)\/[^ ]+$/\1\/META/;
  s/^([0-9\.]+)\/[^ ]+ \.\/[^ ]+$/\1\/META \.\/META/;
  s/_URL\/[^ ]+/_URL\/NPP/g;
  s/_EMAIL\/[^ ]+/_EMAIL\/NPP/g;
  s/(^| )(l+o+l+|a+r+g+h+|a+h+a+|♥)\/[^ ]+/$1$2\/I/gi;
  s/(^| )([•·\*o]|\.+)\/[^ ]+/$1$2\/PONCT/g;
  s/(^| )(Like|Share)\/[^ ]+/$1$2\/ET/g;
  s/(^|$)([^ ]+)\/[^ ]+ (at)\/[^ ]+ (\d+)\/[^ ]+ (:)\/[^ ]+ (\d+(?:[ap]m)?)\/[^ ]+/$1$2\/ADV $3\/P $4\/DET $5\/PONCT $6\/DET/g;
  s/(^|$)(\d+)\/[^ ]+ (people)\/[^ ]+ (like)\/[^ ]+ (this)\/[^ ]+/$1$2\/DET $3\/NC $4\/V $5\/PRO/g;
  s/(^|$)(\d+)\/[^ ]+ (hours|minutes|seconds)\/[^ ]+ (ago)\/[^ ]+/$1$2\/DET $3\/NC $4\/ADV/g;
  s/(^|$)(love)\/[^ ]+ (u|you)\/[^ ]+/$1$2\/V $3\/PRO/g;
  s/}_/} _/g;

  # réalignement sur les tokens d'origine
  while ($_ ne "") {
    if (s/^{([^ {}]+)} ([^ {}]+(?: \{\} *[^ {}]+)+)( |$)//) {
      $t = $1;
      $f = $2;
      $f =~ s/^[^ ]*\///;
      $f =~ s/ {} [^ ]*\//+/g;
      $t =~ s/^(.*)◀.*/\1/;
      if ($remove_non_standard_amalgams && $f =~ /\+/ && $f ne "P+D" && $f ne "P+PRO") {
	$f = "X";
      }
      $out .= " $t/$f";
    } elsif (s/^{([^ {}]+(?: [^{}]+)+)} ([^ {}]+)( |$)//) {
      $t = $1;
      $f = $2;
      $t =~ s/^(.*)◀.*/\1/;
      $t =~ s/ /\/Y /g;
      $out .= " $t/Y";
    } elsif (s/^{([^ {}]+)} ([^ {}]+)( |$)//) {
      $t = $1;
      $f = $2;
      $t =~ s/^(.*)◀.*/\1/;
      $f =~ s/^.*\///;
      $out .= " $t/$f";
    } elsif (s/^([^{} ]+)( |$)//) {
      $out .= " $1";
    } else {
      die $_;
    }
    s/^ *//;
  }
  print $out."\n";
}
print STDERR "  MElt TAGGING REALIGNER: Realigning POS-tags to original tokens: done\n";
