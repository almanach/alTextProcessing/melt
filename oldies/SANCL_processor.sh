#!/usr/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

SPP_LAST_STEP=w
LANGUAGE=fr
TOKBOOL=nt

while getopts :l:Thtcmbw o
do case "$o" in
	h)  echo "Only perform FTB/Bonsai tokenization, deterministic correction, MElt POS-tagging and realignment of MElt POS-tags to the original tokens" \
	    && echo "Usage: sh supercrap_preprocessor.sh < input > output" \
            && echo "-g	Only apply SxPipe URL, e-mail and smiley grammars" \
            && echo "-t	Only apply SxPipe URL, e-mail and smiley grammars and FTB/Bonsai tokenization" \
            && echo "-c	Only apply SxPipe URL, e-mail and smiley grammars, FTB/Bonsai tokenization and deterministic correction" \
            && echo "-m	Only apply SxPipe URL, e-mail and smiley grammars, FTB/Bonsai tokenization, deterministic correction and MElt POS-tagging" \
            && echo "-b	Full processing, output in Brown format" \
            && echo "-w	Full processing, output in WordFreak format (default)" \
	    && echo "-T Skip FTB/Bonsai tokenization" \
            && echo "-h	Output this help message" \
            && exit 1;;
	T)  TOKBOOL=yes;;
	g)  SPP_LAST_STEP=g;;
	t)  SPP_LAST_STEP=t;;
	c)  SPP_LAST_STEP=c;;
	m)  SPP_LAST_STEP=m;;
	b)  SPP_LAST_STEP=b;;
	w)  SPP_LAST_STEP=w;;
	l)  LANGUAGE="$OPTARG";;
    esac
done

case $TOKBOOL in
    nt)
	TOKENIZER="cat"
	SEGMENTER="cat"
	;;
    *)
	TOKENIZER="perl $DIR/do_tok.pl"
	SEGMENTER="perl $DIR/do_seg.pl"
	;;
esac
CORRECTOR="perl $DIR/apply_correcs.pl -d $DIR/../data/$LANGUAGE"
TAGGER="MElt -c -m /Users/sagot/Documents/MElt/en_ontonotes_ms.perceptron"
POST_TAGGER="perl $DIR/SANCL_postprocess.pl"
FORMAT_CONVERTER="perl $DIR/brown2wordfreak.pl"

case $SPP_LAST_STEP in
    g)
        $DIR/clean_noisy_characters.sh | $DIR/escape_curly_brackets.pl | perl /usr/local/share/sxpipe/txt2tok/gl_url.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_email.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_smiley.pl | perl -pe "s/_(REGLUE|UNSPLIT)_//g" | $DIR/unescape_curly_brackets.pl
        ;;
    t)
        $DIR/clean_noisy_characters.sh | $DIR/escape_curly_brackets.pl | perl /usr/local/share/sxpipe/txt2tok/gl_url.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_email.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_smiley.pl | perl -pe "s/_(REGLUE|UNSPLIT)_//g" | $TOKENIZER | $SEGMENTER | $DIR/unescape_curly_brackets.pl
        ;;
    c)
        $DIR/clean_noisy_characters.sh | $DIR/escape_curly_brackets.pl | perl /usr/local/share/sxpipe/txt2tok/gl_url.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_email.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_smiley.pl | perl -pe "s/_(REGLUE|UNSPLIT)_//g" | $TOKENIZER | $SEGMENTER | $CORRECTOR | $DIR/unescape_curly_brackets.pl
        ;;
    m)
        $DIR/clean_noisy_characters.sh | $DIR/escape_curly_brackets.pl | perl /usr/local/share/sxpipe/txt2tok/gl_url.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_email.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_smiley.pl | perl -pe "s/_(REGLUE|UNSPLIT)_//g" | $TOKENIZER | $SEGMENTER | $CORRECTOR | $TAGGER | $DIR/unescape_curly_brackets.pl
        ;;
    b)
        $DIR/clean_noisy_characters.sh | $DIR/escape_curly_brackets.pl | perl /usr/local/share/sxpipe/txt2tok/gl_url.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_email.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_smiley.pl | perl -pe "s/_(REGLUE|UNSPLIT)_//g" | $TOKENIZER | $SEGMENTER | $CORRECTOR | $TAGGER | $POST_TAGGER | $DIR/unescape_curly_brackets.pl
        ;;
    w)
        $DIR/clean_noisy_characters.sh | $DIR/escape_curly_brackets.pl | perl /usr/local/share/sxpipe/txt2tok/gl_url.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_email.pl -no_sw | perl /usr/local/share/sxpipe/txt2tok/gl_smiley.pl | perl -pe "s/_(REGLUE|UNSPLIT)_//g" | $TOKENIZER | $SEGMENTER | $CORRECTOR | tee corrected | $TAGGER | tee tagged | $POST_TAGGER | tee posttagged | $DIR/unescape_curly_brackets.pl | $FORMAT_CONVERTER
        ;;
    *)
        echo "Internal error"
        ;;
esac
