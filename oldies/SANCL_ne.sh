#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

$DIR/clean_noisy_characters.sh | $DIR/escape_curly_brackets.pl | perl $DIR/../../sxpipe-light/gl_email.pl -l en -no_sw | perl $DIR/../../sxpipe-light/gl_url.pl -l en -no_sw | $DIR/../../sxpipe-light/gl_date.pl -l en -no_sw | $DIR/../../sxpipe-light/gl_heure.pl -l en -no_sw | perl $DIR/../../sxpipe-light/gl_smiley.pl | perl -pe "s/} *([^ ]+) _(?:REGLUE|UNSPLIT)_([^ \\n{]+)/\$2} \$1/g"| perl -pe "s/_(?:REGLUE|UNSPLIT)_//g" | perl -pe "s/{[^ }\\n]+} *(_[A-Z]+)(?:[^ A-Z\\n][^ \\n]*)?/lc(\$1)/ge; s/{(.*?)} *_[^ \\n]+/\$1/g" | $DIR/unescape_curly_brackets.pl



