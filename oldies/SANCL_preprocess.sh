#!/bin/sh

LANGUAGE=fr
TOKBOOL=yes

while getopts :l:Thtcmbw o
do case "$o" in
	h)  echo "Usage: sh supercrap_preprocessor.sh < input > output" \
            && echo "-l language	Specify the language (fr, en)" \
	    && echo "-T Skip FTB/Bonsai tokenization" \
            && echo "-h	Output this help message" \
            && exit 1;;
	T)  TOKBOOL=nt;;
	l)  LANGUAGE="$OPTARG";;
    esac
done

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CORRECTOR="perl $DIR/apply_correcs.pl -d $DIR/../data/${LANGUAGE} -l ${LANGUAGE}"
case $TOKBOOL in
    nt)
	TOKENIZER="cat"
	GL_EMAIL="perl $DIR/../../sxpipe-light/gl_email.pl -no_sw"
	GL_URL="perl $DIR/../../sxpipe-light/gl_url.pl -no_sw"
	GL_SMILEY="perl $DIR/../../sxpipe-light/gl_smiley.pl -no_sw"
	;;
    *)
	TOKENIZER="perl $DIR/do_tok.pl"
	GL_EMAIL="perl $DIR/../../sxpipe-light/gl_email.pl"
	GL_URL="perl $DIR/../../sxpipe-light/gl_url.pl"
	GL_SMILEY="perl $DIR/../../sxpipe-light/gl_smiley.pl"
	;;
esac

$DIR/clean_noisy_characters.sh | $DIR/escape_curly_brackets.pl | ${GL_EMAIL} | ${GL_URL} | ${GL_SMILEY} | perl -pe "while (s/({[^{}\\n]*){([^{}\\n]*)} *[^ {}\\n]+/\$1\$2/g) {}" | perl -pe "s/} *([^ ]+) _(?:REGLUE|UNSPLIT)_([^ \\n{]+)/\$2} \$1/g" | perl -pe "s/_(?:REGLUE|UNSPLIT)_//g" | perl -pe "s/{([^}]+ [^0-9 }]+ [^}\\n]+)} *_(?:DATE[^ {\\n]*|HEURE|TIME)/\$1/g; s/{([^0-9 }]+ [^}\\n]+)} *_(?:DATE[^ \\n]*|HEURE|TIME)/\$1/g; s/{([^}]+ [^0-9 }\\n]+)} *_(?:DATE[^ \\n]*|HEURE|TIME)/\$1/g;" | $TOKENIZER | $CORRECTOR | perl -pe "while (s/({[^{}\\n]*){[^{}\\n]*} *([^ {}\\n]+)/\$1\$2/g) {}"
