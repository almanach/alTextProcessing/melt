#!/usr/bin/perl

while (<>) {
  chomp;
  s/(I|he|she|you|it|we|they|there)â € ™ /$1 '/g;
  s/� � � � � � �/ \`\` /g;
  s/� � � � � � �/ '' /g;
  s/â € ™/ ' /g;
  s/(I|he|she|you)� � � � � � /$1 '/g;
  s/� � � � � �/ ' /g;
  s/� � � � � �/ \`\` /g;
  s/� � � � � �/ '' /g;
  s/� � � � � �/ ... /g;
  s/� � � �/ — /g;
  s/� � � � � �/ ™ /g;
  s/� � � � � �/ ��� /g;
  s/(I|he|she|you|it|we|they|there)Â ´ /$1 '/g;
  s/Â ´/ ' /g;
  s/� �/ï/g;
  s/(I|he|she|you|it|we|they|there)� � /$1 '/g;
  s/� �/ ' /g;
  s/� � */ ''/g;
  s/� � / \`\`/g;
  s/� � / \`/g;
  s/� � / £ /g;
  s/� � / — /g;
  s/  +/ /g;
  s/^ +//g;
  s/ +$//g;
  s/ ' s / 's /g;
  s/n ' t / n't /g;
  print "$_\n";
}
